﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.ReportingServices;
using Microsoft.Reporting.WebForms;
using System.Web.Security;
using System.IO;

namespace DSNY.Mvc
{
    public partial class Reports : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            // make sure user is authenticated and in the correct group
            if (!Request.IsAuthenticated)
                Response.Redirect("/Home/Index?ReturnUrl=/Reports/reports.aspx");
            else if (!Roles.IsUserInRole("System Administrator"))
                Response.Redirect("/");

            // if this is the first page hit
            if (!IsPostBack)
            {
                // get reports from web.config
                string[] reports = ConfigurationManager.AppSettings["Reports"].ToString().ToLower().Split(';');

                // check if we have a report from the querystring and that report matches a report defined from the web.config
                if (!string.IsNullOrEmpty(Request.QueryString["report"]) && reports.Contains(Request.QueryString["report"].ToLower()))
                {
                    // set report to visible
                    ReportViewer.Visible = true;
                    
                    // get report, report server and the report path
                    string requestedReport = Request.QueryString["report"].ToString();
                    string reportServer = ConfigurationManager.AppSettings["ReportServer"].ToString();
                    string reportPath = ConfigurationManager.AppSettings["ReportPath"].ToString();

                    // get credentials for report server
                    string user = ConfigurationManager.AppSettings["ReportServerUser"].ToString();
                    string password = ConfigurationManager.AppSettings["ReportServerPass"].ToString();
                    string domain = ConfigurationManager.AppSettings["ReportServerDomain"].ToString();
                    ReportServerNetworkCredentials reportCredentials = new ReportServerNetworkCredentials(user, password, domain);

                    // load up report viewer with necessary data
                    ReportViewer.ServerReport.ReportServerCredentials = reportCredentials;
                    ReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ReportViewer.ServerReport.ReportServerUrl = new Uri(reportServer);
                    ReportViewer.ServerReport.ReportPath = reportPath + "/" + requestedReport;
                    ReportViewer.ShowParameterPrompts = true;

                    // refresh server report
                    ReportViewer.ServerReport.Refresh();
                }
                else
                {
                    // show error message and hide report viewer
                    lblReportNotFound.Visible = true;
                    ReportViewer.Visible = false;
                }
            }
        }
    }
}