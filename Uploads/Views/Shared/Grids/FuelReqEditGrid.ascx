﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Data.Fuel_Form_Details>>" %>

    <% List<DSNY.Data.Vendor> vendors = ViewData["Vendors"] as List<DSNY.Data.Vendor>;
       List<DSNY.Data.Product_User> productUser = ViewData["AvailableProducts"] as List<DSNY.Data.Product_User>;
       List<DSNY.Data.Equipment_User> userProductEquip = ViewData["AvailableEquipment"] as List<DSNY.Data.Equipment_User>;
       
       if (Model.Count() > 0) { %>
        <table id="fuelReqTable" style="width: 1125px;">
            <thead>
            <tr>
                <th style="width: 250px;">Product</th>
                <th>Capacity</th>
                <th style="width: 95px;">Qty On Hand</th>
                <th style="width: 97px;">Spare Drums</th>
                <th style="width: 100px; text-align: center;">Vendor</th>
                <th style="width: 100px; text-align: center;">Invoice #</th>
                <th style="width: 115px; text-align: center;">Date</th>
                <th>Quantity</th>
            </tr>
            </thead>
            <tbody>
        <%  int rowCount = 0;
            int deliveryCount = 1;
            List<DSNY.Data.Product_User> userProducts = new List<DSNY.Data.Product_User>();
            List<DSNY.Data.Fuel_Form_Details_Delivery> fuelDeliveries = null;
            DSNY.Data.Product_User foundProduct = null;
            DSNY.Data.Equipment_User relatedEquip = null;

            if (ViewData["UserProducts"] != null && ((List<DSNY.Data.Product_User>)ViewData["UserProducts"]).Count > 0)
                userProducts = (List<DSNY.Data.Product_User>)ViewData["UserProducts"];

            foreach (var item in Model)
            {
                foundProduct = null;
                fuelDeliveries = null;
                relatedEquip = userProductEquip.SingleOrDefault(pe => pe.Equipment_User_ID == item.Equipment_User_ID && pe.Equipment.has_Capacity); %>
              
                <tr productId="<%: item.Product_ID %>" class="<%: rowCount % 2 == 0 ? "alt" : string.Empty %>">
                    <td>
                        <input type="hidden" id="prodDetailsId-<%: item.Product_ID %>" name="prodDetailsId-<%: item.Product_ID %>" value="<%: item.Fuel_Form_Details_Id %>"  />
                        <%: item.Product.Product_Name%>
                        <% if (relatedEquip != null && relatedEquip.Equipment_Description != string.Empty) {%>
                            - <%: relatedEquip.Equipment_Description%>
                            <input type="hidden" id="equipUser-<%: item.Product_ID %>.<%: rowCount %>" name="equipUser-<%: item.Product_ID %>.<%: rowCount %>" value="<%: relatedEquip.Equipment_User_ID %>" />
                        <% } %>
                        <% if (!string.IsNullOrEmpty(item.Product.Measurement_Type)) %>
                            (<%: item.Product.Measurement_Type%>)
                    </td>
                    <td style="text-align: center;">
                    <% if (relatedEquip != null && relatedEquip.Capacity > 0)
                        {%>
                        <%: relatedEquip.Capacity%>
                    <% }
                        else
                        {
                            foundProduct = userProducts.SingleOrDefault(up => up.Product_ID == item.Product_ID);

                            if (foundProduct != null && foundProduct.Capacity > 0)
                            { %>
                            <%: foundProduct.Capacity%>
                        <% }
                            else
                            { %>
                            <%: item.Product.Default_Capacity%>
                    <%     }
                        } %>
                    </td>
                    <td style="text-align: center;">
                        <input type="text" id="prodqtyOnHand-<%: item.Product_ID %>.1" name="prodqtyOnHand-<%: item.Product_ID %>.1" value="<%: item.On_Hand %>" style="width: 50px; text-align: right;" />
                    </td>
                    
                    <td style="text-align: center;">
                        <% if (item.Product.is_Drums)
                           {%>
                            <input type="text" id="proddrum-<%: item.Product_ID %>.1" name="proddrum-<%: item.Product_ID %>.1" value="<%: item.Spare_Drums %>" style="width: 50px; text-align: right;" />
                        <% } %>
                    </td>
                    <%
                        fuelDeliveries = item.Fuel_Form_Details_Delivery.Where(dd => dd.Fuel_Form_Details_Id == item.Fuel_Form_Details_Id).OrderBy(dd => dd.Order_No).ToList();
                        deliveryCount = 1;

                        if (fuelDeliveries.Count == 0)
                        { %>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            </tr>
                     <% }
                        else
                        {
                            foreach (var delivery in fuelDeliveries)
                            {
                                if (deliveryCount == 1)
                                { %>
                                <td style="text-align: center;">
                                    <select id="prodvendor-<%: item.Product_ID %>.<%: deliveryCount %>" name="prodvendor-<%: item.Product_ID %>.<%: deliveryCount %>">
                                        <option value="">SELECT VENDOR</option>
                                    <%                                   
                                    if (vendors != null)
                                    {
                                        foreach (DSNY.Data.Vendor vendor in vendors)
                                        {
                                            if (vendor.Vendor_ID == delivery.Vendor_ID)
                                            { %>
		                                           <option value="<%: vendor.Vendor_ID %>" selected><%: vendor.Vendor_Name%></option>
                                               <% }
                                            else
                                            { %>
                                                   <option value="<%: vendor.Vendor_ID %>"><%: vendor.Vendor_Name%></option>
                                               <%} %>
	                                    <%  }
                                    } %>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" id="prodinvoice-<%: item.Product_ID %>.<%: deliveryCount %>" name="prodinvoice-<%: item.Product_ID %>.<%: deliveryCount %>" 
                                        value="<%: delivery.Invoice_Number %>" style="width: 95px; text-align: right;" />
                                </td>

                                <td>
                                    <input type="text" id="proddate-<%: item.Product_ID %>.<%: deliveryCount %>" name="proddate-<%: item.Product_ID %>.<%: deliveryCount %>" style="width: 70px;" size="10" readonly />
                                    <script type="text/javascript">
                                    <% if (item.Fuel_Form_Details_Delivery.Count > 0 && item.Fuel_Form_Details_Delivery.First().Recieve_Date != null) { %>
                                           new tcal({ 'formname': 'fuelForm', 'controlname': 'proddate-<%: item.Product_ID %>.<%: deliveryCount %>', 'id': 'proddatecal-<%: item.Product_ID %>.<%: deliveryCount %>', 
                                                'selected': '<%: delivery.Recieve_Date.Value.ToString("MM/dd/yyyy")%>'});
                                    <% } else { %>
                                           new tcal({ 'formname': 'fuelForm', 'controlname': 'proddate-<%: item.Product_ID %>.<%: deliveryCount %>', 'id': 'proddatecal-<%: item.Product_ID %>.<%: deliveryCount %>'});
                                    <% } %>
		                            </script>
                                </td>
                                <td>
                                    <input type="text" id="prodqty-<%: item.Product_ID %>.<%: deliveryCount %>" name="prodqty-<%: item.Product_ID %>.<%: deliveryCount %>" value="<%: delivery.Quantity %>" style="width: 50px; text-align: right;" />
                                </td>
                            </tr>
                         <% }
                                else
                                { %>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                <td style="text-align: center;">
                                    <select id="prodvendor-<%: item.Product_ID %>.<%: deliveryCount %>" name="prodvendor-<%: item.Product_ID %>.<%: deliveryCount %>">
                                        <option value="">SELECT VENDOR</option>
                                    <% 
                                    if (vendors != null)
                                    {
                                        foreach (DSNY.Data.Vendor vendor in vendors)
                                        {
                                            if (vendor.Vendor_ID == delivery.Vendor_ID)
                                            { %>
		                                           <option value="<%: vendor.Vendor_ID %>" selected><%: vendor.Vendor_Name%></option>
                                               <% }
                                            else
                                            { %>
                                                   <option value="<%: vendor.Vendor_ID %>"><%: vendor.Vendor_Name%></option>
                                               <%} %>
	                                    <%  }
                                    } %>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" id="prodinvoice-<%: item.Product_ID %>.<%: deliveryCount %>" name="prodinvoice-<%: item.Product_ID %>.<%: deliveryCount %>"
                                         value="<%: delivery.Invoice_Number %>"  style="width: 95px; text-align: right;" />
                                </td>

                                <td>
                                    <input type="text" id="proddate-<%: item.Product_ID %>.<%: deliveryCount %>" name="proddate-<%: item.Product_ID %>.<%: deliveryCount %>" style="width: 70px;" size="10" readonly />
                                    <script type="text/javascript">
                                    <% if (item.Fuel_Form_Details_Delivery.Count > 0 && item.Fuel_Form_Details_Delivery.First().Recieve_Date != null) { %>
                                           new tcal({ 'formname': 'fuelForm', 'controlname': 'proddate-<%: item.Product_ID %>.<%: deliveryCount %>', 'id': 'proddatecal-<%: item.Product_ID %>.<%: deliveryCount %>', 
                                            'selected': '<%: delivery.Recieve_Date.Value.ToString("MM/dd/yyyy")%>'});
                                    <% } else { %>
                                           new tcal({ 'formname': 'fuelForm', 'controlname': 'proddate-<%: item.Product_ID %>.<%: deliveryCount %>', 'id': 'proddatecal-<%: item.Product_ID %>.<%: deliveryCount %>'});
                                    <% } %>
		                            </script>
                                </td>
                                <td>
                                    <input type="text" id="prodqty-<%: item.Product_ID %>.<%: deliveryCount %>" name="prodqty-<%: item.Product_ID %>.<%: deliveryCount %>" value="<%: delivery.Quantity %>" style="width: 50px; text-align: right;" />
                                </td>
                            </tr>
                         <% }

                            deliveryCount++;
                        }
                    }%>
            <%
                rowCount++;
            } %>
            </tbody>
        </table>
    <% } else { %>
        <h3>No products have been assigned to this site assigned.</h3>
    <% } %>