﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master"  Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.LogOnModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Login
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/DSNYFunctions.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftAjax.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftMvcValidation.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) { %>
        <div id="loginContainer">
            <%: Html.ValidationMessage("LoginError", new { @id = "loginError" }) %>
            <div id="loginHeader">
                Please sign in with your user name and password&nbsp;
                [<a style="color: #FFFFFF;" href="#" onclick="helpPopup('Login')">?</a>]</div>
            <div id="loginBox"">
                <div class="display-label">
                    <%: Html.ValidationMessageFor(m => m.UserName) %>
                    <%: Html.LabelFor(m => m.UserName) %>:
                </div>
                <div class="display-field">
                    <%: Html.TextBoxFor(m => m.UserName) %>
                </div>
                
                <div class="clear"></div>

                <div class="display-label">
                    <%: Html.ValidationMessageFor(m => m.Password) %>
                    <%: Html.LabelFor(m => m.Password) %>:
                </div>
                <div class="display-field">
                    <%: Html.PasswordFor(m => m.Password)%>
                </div>
                
                <div class="clear"></div>

                <p class="loginButton">
                    <input type="submit" id="submitBtn" name="submitBtn" value="Log On" />
                </p>
            </div>
        </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            // focus on the user name field on load
            $("#UserName").focus();
        });
    </script>
</asp:Content>