﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.FuelFormViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	View Fuel Form
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/calendar_us.js" type="text/javascript"></script>
    <script src="/Scripts/Tabs.js" type="text/javascript"></script>
    <script src="/Scripts/DSNYFunctions.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="/Content/calendar.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Fuel Requirements and Equipment Form</h2>
    <p style="width: 1180px;" class="dottedLine"></p>

    <%  if (Model.fuelForm.Fuel_Form_ID != -1)
        {
            using (Html.BeginForm("Create", "FuelForm", FormMethod.Post, new { id = "fuelForm" }))
            { %>

            <div style="float: left; width: 1175px;">
                <div class="iconLeft">
                    <a href="" onclick="history.go(-1); return false;" class="iconLink">
                        <img border="0" src="/Content/Images/Icons/cancel.png" alt="Close" /><br />Close
                    </a>
                </div>
            </div>

            <div class="clear"></div>

            <div style="float: left; margin-bottom: 10px; width: 1175px;">
                <div class="display-label" style="margin-top: .9em; width: 100px; font-weight: bold;"><label for="locationId">Location Id</label>: </div>
                <div class="display-field" style="margin: 0.9em 0; width: 125px;">
                    <%: Model.submittedBy.userName%>
                </div>

                <div class="display-label" style="margin-top: .9em; font-weight: bold;"><label for="Supervisor_Full_Name">Supervisor</label>: </div>
                <div class="display-field" style="margin: 0.9em 0; width: 160px;">
                    <%: Model.fuelForm.Supervisor_Full_Name%>
                </div>

                <div class="display-label" style="margin-top: .9em; font-weight: bold; width: 150px;"><label for="Supervisor_Badge_Number">Badge Number</label>: </div>
                <div class="display-field" style="margin: 0.9em 0; width: 185px;">
                    <%: Model.fuelForm.Supervisor_Badge_Number%>
                </div>

                <div style="margin-top: .9em;"><%: Model.fuelForm.Submission_Date.Value.ToString("M/d/yyyy h:mm tt")%></div>
            </div>

            <div class="clear"></div>

            <ul id="tabnav">
                <li class="selectedTab"><a href="#">Fuel Requirements</a></li>
                <% if (ViewData["EquipFailure"] != null && ((List<DSNY.Data.Fuel_Form_Equipment_Failure>)ViewData["EquipFailure"]).Exists(ef => ef.is_Equipment_Failure))
                   { %>
                    <li><a href="#" onclick="f_tcalHideAll();">* Equipment Failure *</a></li>
                <% }
                   else
                   { %>
                    <li><a href="#" onclick="f_tcalHideAll();">Equipment Failure</a></li>
                <% } %>
                <li><a href="#">General Remarks</a></li>
            </ul>

            <div id="tabsContainer" style="width: 1175px;">
                <div class="tabContent" style="width: 1175px;">
                    <% Html.RenderPartial("Grids/FuelReqViewGrid", Model.fuelFormDetails); %>
                </div>

                <div class="tabContent" style="width: 1175px;">
                    <% Html.RenderPartial("Grids/EquipFailViewGrid", Model.fuelFormEquipFailure); %>
                </div>

                <div class="tabContent" style="width: 1175px;">
                    <% if (!string.IsNullOrEmpty(Model.fuelForm.Remarks))
                       { %>
                        <pre style="font-family: Verdana,Helvetica,Sans-Serif; font-size: 1.2em; color: #000000;"><%: Model.fuelForm.Remarks%></pre>
                    <% } %>
                </div>
            </div>
        <%  }
        } else { %>
            <div><%: Model.missingFuelFormMessage %></div>
            <div style="width: 500px; text-align: center; padding-top: 10px;">
                <a href="" onclick="history.go(-1); return false;" style="font-size: .85em; text-decoration: none; color: #696969;">
                    <img style="border: 0px;" src="/Content/Images/Icons/back.png" alt="Go Back" /><br />Go Back
                </a>
            </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 1200px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tabsContainer").tabs({ tab: 1 });
            preload(["/Content/Images/Icons/delete.png"]);

        });
    </script>
</asp:Content>