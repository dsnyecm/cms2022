﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<DSNY.Core.Interfaces.IRole>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Roles
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script src="/Scripts/DSNYFunctions.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Roles</h2>
    <p style="width: 300px;" class="dottedLine"></p>

    <div style="width: 300px;">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Admin", action = "Index"}) %>" class="iconLink">
                <img border="0" src="/Content/Images/Icons/back.png" alt="Admin List" /><br />Admin<br />Menu
            </a>
        </div>
    </div>

    <div class="clear" style="padding-bottom: 15px;"></div>

    <div id="roleGrid" style="width: 300px;">
        <% if (Html.ValidationMessage("Error") != null) { %>
            <%: Html.ValidationMessage("Error", new { id = "errormessage" })%><br /><br />
        <% } %>

        <% Html.RenderPartial("Grids/RoleGrid"); %>
    </div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 340px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            // set up our table sorter
            $("#roleTable").tablesorter({ widgets: ["zebra"],
                widgetZebra: { css: ["alt", ""] }
            });

            // AJAX event which deletes the selected item
            $(".deleteButton").live('click', function () {
                var data = { name: $(this).attr("role") };

                if (confirm('Press \'OK\' to delete the ' + data.name + ' role.')) {

                    $.get("/Roles/Delete/", data, function (data) {
                        if (data.status == "success") {
                            $('#row-' + data.name).fadeOut('slow');
                            reStripe("roleTable");
                        }
                        else {
                            $("#errorMessage").html("Users are still in the " + data.name + " role.  Remove all users before deleting the role.<br /><br />");
                        }
                    }, 'json');
                }
            });
        });
    </script>
</asp:Content>