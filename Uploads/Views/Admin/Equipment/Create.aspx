﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.Data.Equipment>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create Equipment
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="/Scripts/MicrosoftAjax.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftMvcValidation.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Create Equipment Item</h2>
    <p class="dottedLine"></p>

    <div class="adminIconContainer">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Equipment", action = "List"}) %>" class="iconLink">
                <img border="0" src="/Content/Images/Icons/Cancel.png" alt="Cancel" /><br />Cancel
            </a>
        </div>
        <div class="iconLeft">
            <a onclick="document.forms[0].submit();" href="#" class="iconLink">
                <img border="0" src="/Content/Images/Icons/create.png" alt="Create"/><br />Create
            </a>
        </div>
    </div>

    <div class="clear"></div>

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend>Equipment Information</legend>
            
            <div class="editor-label"><%: Html.LabelFor(model => model.Equipment_Name) %>:</div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Equipment_Name) %>
                <%: Html.ValidationMessageFor(model => model.Equipment_Name) %>
            </div>
            
            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.is_Active) %>:</div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.is_Active, new { @checked = "true"}) %>
                <%: Html.ValidationMessageFor(model => model.is_Active) %>
            </div>
        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>