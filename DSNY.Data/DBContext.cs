﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DSNY.Data;

namespace DSNY.Data
{
    public class DSNYContext
    {
        private DSNYEntities dataContext;

        public DSNYEntities DataContext
        {
            get
            {
                if (dataContext == null)
                {
                    dataContext = new DSNYEntities();
                }

                dataContext.Messages.EnablePlanCaching = true;
                dataContext.Sent_Message.EnablePlanCaching = true;
                dataContext.Products.EnablePlanCaching = true;
                dataContext.Fuel_Form.EnablePlanCaching = true;
                dataContext.Fuel_Form_Details.EnablePlanCaching = true;
                dataContext.Fuel_Form_Details_Delivery.EnablePlanCaching = true;
                dataContext.Fuel_Form_Equipment_Failure.EnablePlanCaching = true;
                dataContext.Product_User.EnablePlanCaching = true;
                dataContext.Vendors.EnablePlanCaching = true;

                return dataContext;
            }
        }

        public void DisposeDataContext()
        {
            if (dataContext != null) {
                dataContext.Dispose();
            }
        }
    }
}