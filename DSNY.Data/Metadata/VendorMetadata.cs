﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace DSNY.Data
{
    /// <summary>
    /// Metadata class which defines how the Entities Framework displays/validates the Vendor table data
    /// </summary>
    [MetadataType(typeof(VendorMetaData))]
    public partial class Vendor
    {
        // Validation rules for the Product class
        [Bind(Exclude = "Vendor_ID")]
        public class VendorMetaData
        {
            [ScaffoldColumn(false)]
            public object Vendor_ID { get; set; }

            [Required(ErrorMessage = "A vendor name is required.")]
            [DisplayName("Vendor Name")]
            public object Vendor_Name { get; set; }

            [DisplayName("Account Number")]
            public object Account_Number { get; set; }

            [DisplayName("Phone Number")]
            public object Phone_Number { get; set; }

            [DisplayName("Active")]
            public object is_Active { get; set; }
        }
    }
}
