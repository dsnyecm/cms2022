GO

ALTER Table Product
ADD is_Reading_Required BIT NOT NULL DEFAULT 0;
GO

ALTER Table Fuel_Form_Details
ADD Tank_Delivered INT NULL,
Tank_Dispensed INT NULL
GO

-- Random generation for fuel form
--UPDATE [Fuel_Form_Details]
--SET [Tank_Delivered] = ABS(Checksum(NewID()) % 500) + 1,
--[Tank_Dispensed] = ABS(Checksum(NewID()) % 500) + 1n

CREATE TABLE DSNYExceptions
	(
	ExceptionId int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	ExceptionCategoryId int NOT NULL,
	Exception nchar(3000) NOT NULL,
	ExceptionTableName nchar(255) NULL,
	ExceptionTableKey nchar(255) NULL,
	ExceptionDateTime date NOT NULL,
	ExceptionUser uniqueidentifier NOT NULL
	)
GO

CREATE TABLE DSNYExceptionCategories
	(
	ExceptionCategoryId int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	ExceptionCategory nchar(255) NOT NULL,
	ExceptionCode nchar(255) NOT NULL,
	IsCollected bit NOT NULL,
	IsReport bit NOT NULL,
	IsCritical bit NOT NULL,
	Priority int NOT NULL
	)
GO

CREATE TABLE DailyReports
	(
	DailyReportId int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	DailyReportDate datetime NOT NULL,
	DailyReportWeatherId int NOT NULL,
	DailyReportFunctionId int NOT NULL,
	DailyReportTempLowId int NOT NULL,
	DailyReportTempHighId int NOT NULL
	)
GO

CREATE TABLE DailyReportsWeather
	(
	DailyReportWeatherId int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	DailyReportWeather nchar(255) NOT NULL
	)
GO

CREATE TABLE DailyReportsFunctions
	(
	DailyReportFunctionId int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	DailyReportFunction nchar(255) NOT NULL
	)
GO

CREATE TABLE DailyReportsTemperature
	(
	DailyReportTemperatureId int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	DailyReportTemperature nchar(255) NOT NULL
	)
GO
 
 
ALTER TABLE [dbo].[DSNYExceptions]  WITH CHECK ADD  CONSTRAINT [FK_DSNYExceptions_DSNYExceptionCategories] FOREIGN KEY([ExceptionCategoryId])
REFERENCES [dbo].[DSNYExceptionCategories] ([ExceptionCategoryId])
GO

ALTER TABLE [dbo].[DSNYExceptions] CHECK CONSTRAINT [FK_DSNYExceptions_DSNYExceptionCategories]
GO

ALTER TABLE [dbo].[DailyReports]  WITH CHECK ADD  CONSTRAINT [FK_DailyReports_DailyReportsWeather] FOREIGN KEY([DailyReportWeatherId])
REFERENCES [dbo].[DailyReportsWeather] ([DailyReportWeatherId])
GO

ALTER TABLE [dbo].[DailyReports] CHECK CONSTRAINT [FK_DailyReports_DailyReportsWeather]
GO

ALTER TABLE [dbo].[DailyReports]  WITH CHECK ADD  CONSTRAINT [FK_DailyReports_DailyReportsFunctions] FOREIGN KEY([DailyReportFunctionId])
REFERENCES [dbo].[DailyReportsFunctions] ([DailyReportFunctionId])
GO

ALTER TABLE [dbo].[DailyReports] CHECK CONSTRAINT [FK_DailyReports_DailyReportsFunctions]
GO

ALTER TABLE [dbo].[DailyReports]  WITH CHECK ADD  CONSTRAINT [FK_DailyReports_DailyReportTempLow] FOREIGN KEY([DailyReportTempLowId])
REFERENCES [dbo].[DailyReportsTemperature] ([DailyReportTemperatureId])
GO

ALTER TABLE [dbo].[DailyReports] CHECK CONSTRAINT [FK_DailyReports_DailyReportTempLow]
GO

ALTER TABLE [dbo].[DailyReports]  WITH CHECK ADD  CONSTRAINT [FK_DailyReports_DailyReportTempHigh] FOREIGN KEY([DailyReportTempHighId])
REFERENCES [dbo].[DailyReportsTemperature] ([DailyReportTemperatureId])
GO

ALTER TABLE [dbo].[DailyReports] CHECK CONSTRAINT [FK_DailyReports_DailyReportTempHigh]
GO

INSERT INTO [dbo].[DailyReportsWeather] ([DailyReportWeather]) VALUES('Clear')
INSERT INTO [dbo].[DailyReportsWeather] ([DailyReportWeather]) VALUES('Rain')
INSERT INTO [dbo].[DailyReportsWeather] ([DailyReportWeather]) VALUES('Snow')

INSERT INTO [dbo].[DailyReportsFunctions] ([DailyReportFunction]) VALUES('Regular Operations')
INSERT INTO [dbo].[DailyReportsFunctions] ([DailyReportFunction]) VALUES('Exception Operations')
INSERT INTO [dbo].[DailyReportsFunctions] ([DailyReportFunction]) VALUES('No Operations')

INSERT INTO [dbo].[DailyReportsTemperature] ([DailyReportTemperature]) VALUES('-20 - 0')
INSERT INTO [dbo].[DailyReportsTemperature] ([DailyReportTemperature]) VALUES('1 - 20')
INSERT INTO [dbo].[DailyReportsTemperature] ([DailyReportTemperature]) VALUES('41 - 60')
INSERT INTO [dbo].[DailyReportsTemperature] ([DailyReportTemperature]) VALUES('61 - 80')
INSERT INTO [dbo].[DailyReportsTemperature] ([DailyReportTemperature]) VALUES('81 - 100')
INSERT INTO [dbo].[DailyReportsTemperature] ([DailyReportTemperature]) VALUES('100+')

INSERT INTO DSNYExceptionCategories VALUES('Purchase Order Override', 'POO', 0, 1, 0, 1)

GO



-- FUEL FORM HISTORY
CREATE TABLE Fuel_Form_History
(
	Fuel_Form_History_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Fuel_Form_ID int NOT NULL,
	Submission_Date datetime NULL,
	UserId uniqueidentifier NULL,
	Supervisor_Full_Name varchar(40) NULL,
	Supervisor_Badge_Number varchar(20) NULL,
	Remarks varchar(1000) NULL,
	Modified_By uniqueidentifier NOT NULL,
	Modified_Date datetime NOT NULL
)
GO

ALTER TABLE [dbo].[Fuel_Form_History]  WITH CHECK ADD  CONSTRAINT [FK_Fuel_Form_History_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Fuel_Form_History] CHECK CONSTRAINT FK_Fuel_Form_History_aspnet_Users
GO

CREATE TABLE Fuel_Form_Details_History
(
	Fuel_Form_Details_History_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Fuel_Form_ID int NOT NULL,
	Product_ID int NOT NULL,
	On_Hand int NOT NULL,
	Spare_Drums int NULL,
	Fuel_Form_Details_Id int NOT NULL,
	Equipment_User_ID int NULL,
	Tank_Delivered int NULL,
	Tank_Dispensed int NULL,
	Modified_By uniqueidentifier NULL,
	Modified_Date datetime NULL
)

GO

ALTER TABLE [dbo].[Fuel_Form_Details_History]  WITH CHECK ADD  CONSTRAINT [FK_Fuel_Form_Details_History_aspnet_Users] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Product] ([Product_ID])
GO

ALTER TABLE [dbo].[Fuel_Form_Details_History] CHECK CONSTRAINT [FK_Fuel_Form_Details_History_aspnet_Users]
GO

CREATE TABLE Fuel_Form_Details_Delivery_History
(
	Fuel_Form_Details_Delivery_History_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Fuel_Form_Details_Id int NOT NULL,
	Order_No int NOT NULL,
	Invoice_Number varchar(20) NULL,
	Recieve_Date datetime NULL,
	Quantity int NULL,
	Vendor_ID int NULL,
	Modified_By uniqueidentifier NULL,
	Modified_Date datetime NULL
)

GO

ALTER TABLE [dbo].[Fuel_Form_Details_Delivery_History]  WITH CHECK ADD  CONSTRAINT [FK_Fuel_Form_Details_Delivery_History_Vendor] FOREIGN KEY([Vendor_ID])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

ALTER TABLE [dbo].[Fuel_Form_Details_Delivery_History] CHECK CONSTRAINT [FK_Fuel_Form_Details_Delivery_History_Vendor]
GO


CREATE TABLE Fuel_Form_Equipment_Failure_History
(
	Fuel_Form_Equipment_Failure_History_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Equipment_Failure_ID int NOT NULL,
	is_Equipment_Failure bit NOT NULL,
	Failure_Date datetime NULL,
	Fix_Date datetime NULL,
	Remarks varchar(350) NULL,
	Equipment_User_ID int NULL,
	Fuel_Form_ID int NOT NULL,
	Modified_By uniqueidentifier NULL,
	Modified_Date datetime NULL
)

GO

CREATE TABLE Purchase_Orders
(
	Purchase_Order_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	UserId uniqueidentifier NOT NULL,
	Product_ID int NOT NULL,
	Vendor_ID int NOT NULL,
	Order_Amount int NOT NULL,
	Order_Date datetime NULL,
	Is_Ordered bit NULL,
	Not_Ordered_Reason varchar(350) NULL
)

GO

ALTER TABLE [dbo].[Purchase_Orders]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Orders_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Purchase_Orders] CHECK CONSTRAINT [FK_Purchase_Orders_User]
GO

ALTER TABLE [dbo].[Purchase_Orders]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Orders_Product] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Product] ([Product_ID])
GO

ALTER TABLE [dbo].[Purchase_Orders] CHECK CONSTRAINT [FK_Purchase_Orders_Product]
GO

ALTER TABLE [dbo].[Purchase_Orders]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Orders_Vendor] FOREIGN KEY([Vendor_ID])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

ALTER TABLE [dbo].[Purchase_Orders] CHECK CONSTRAINT [FK_Purchase_Orders_Vendor]
GO

-- Add columns to product table
ALTER TABLE [dbo].[Product]
ADD Vendor_ID int NOT NULL DEFAULT 3,
Sub_Type VARCHAR(50),
Order_Delivery_Type VARCHAR(1000),
Order_Delivery_Distribution VARCHAR(1000)

ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Vendor] FOREIGN KEY([Vendor_ID])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Vendor]
GO

-- Add columns to vendor table
ALTER TABLE [dbo].[Vendor]
ADD Account_Number VARCHAR(100),
Phone_Number VARCHAR(100)

GO

/****** Object:  UserDefinedFunction [dbo].[GetProperty]    Script Date: 10/25/2015 4:57:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetProperty] (
	@UserId UNIQUEIDENTIFIER,
	@PropertyName VARCHAR(50)
)
RETURNS VARCHAR(500)
AS
BEGIN

DECLARE @RetVal VARCHAR(500)
DECLARE @PropPos INT, @Names VARCHAR(500), @Values VARCHAR(500), @start TINYINT, @length TINYINT

-- Note how we prefix the first property name with a colon. Now we know that EVERY property name will be bookended with colons:
SELECT @Names = ':' + CAST(PropertyNames AS VARCHAR(500)), @Values = PropertyValuesString FROM aspnet_Profile WHERE UserId = @UserId

SELECT @PropPos = PATINDEX('%:'+ @PropertyName +':S%', @Names)

IF @PropPos = 0
	BEGIN
		SET @RetVal = NULL
	END
ELSE
	BEGIN
		SET @PropPos = @PropPos + LEN(@PropertyName) + 4
		SELECT @Names = SUBSTRING(@Names, @PropPos, LEN(@Names))
		SELECT @start = CAST(SUBSTRING(@Names, 1, CHARINDEX(':', @Names) - 1) AS tinyint) + 1
		SELECT @length = CAST(SUBSTRING(@Names, CHARINDEX(':', @Names) + 1, ((CHARINDEX(':', @Names, CHARINDEX(':', @Names) + 1)) - (CHARINDEX(':', @Names) + 1))) AS tinyint)
		SELECT @RetVal = SUBSTRING(@Values, @start, @length)
	END

	RETURN @RetVal
END

GO

/****** Object:  StoredProcedure [dbo].[GetPurchaseOrderItems]    Script Date: 9/27/2015 8:40:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPurchaseOrderItems] 
	-- Add the parameters for the stored procedure here
	@daysToCompare int = 90
AS
BEGIN

	if @daysToCompare < 0
		set @daysToCompare = @daysToCompare * -1

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT UserId, UserName, (SELECT dbo.GetProperty(UserId, 'Address')) as Address, 
		(SELECT dbo.GetProperty(UserId, 'Borough')) as Borough, (SELECT dbo.GetProperty(UserId, 'PODisplayName')) as PODisplayName,
		Product_ID, Product_Name, Sub_Type, Vendor_ID, Vendor_Name, Account_Number, Phone_Number,
		SUM(ROUND((ISNULL(Capacity, Default_Capacity) * (CASE is_Drums WHEN 1 THEN 1 ELSE .9 END)),0)) as Capacity, 
		Measurement_Type, SUM(Avg_Dispensed) Avg_Dispensed, SUM(Start_On_Hand) Start_On_Hand, SUM(End_On_Hand) End_On_Hand
		FROM (SELECT DISTINCT u.UserId, u.UserName, p.Product_ID, p.Product_Name, p.Sub_Type, p.is_drums, 
			v.Vendor_ID, v.Vendor_Name, v.Account_Number, v.Phone_Number,
			eu.Capacity, pu.Capacity as default_capacity, p.Measurement_Type, 
			(SELECT TOP (@daysToCompare) AVG(ffd.Tank_Dispensed)
				FROM Fuel_Form ff
				INNER JOIN Fuel_Form_Details ffd on ff.Fuel_Form_ID = ffd.Fuel_Form_ID
				WHERE Submission_Date <= dateadd(hh, (SELECT CASE DATEPART(dw,getdate()) WHEN 2 THEN -84 WHEN 1 THEN -60 ELSE -36 end), GETDATE()) AND
				ff.UserId = pu.UserId AND
				ffd.Product_ID = p.Product_ID) as Avg_Dispensed,
			(SELECT TOP 1 ffd.On_Hand
				FROM Fuel_Form ff
				INNER JOIN Fuel_Form_Details ffd on ff.Fuel_Form_ID = ffd.Fuel_Form_ID
				WHERE Submission_Date <= dateadd(hh, (SELECT CASE DATEPART(dw,getdate()) WHEN 2 THEN -84 WHEN 1 THEN -60 ELSE -36 end), GETDATE()) AND
				ff.UserId = pu.UserId AND
				ffd.Product_ID = p.Product_ID
				ORDER BY Submission_Date) as Start_On_Hand,
			(SELECT TOP 1 ffd.On_Hand
				FROM Fuel_Form ff
				INNER JOIN Fuel_Form_Details ffd on ff.Fuel_Form_ID = ffd.Fuel_Form_ID
				WHERE Submission_Date <= dateadd(hh, (SELECT CASE DATEPART(dw,getdate()) WHEN 2 THEN -84 WHEN 1 THEN -60 ELSE -36 end), GETDATE()) AND
				ff.UserId = pu.UserId AND
				ffd.Product_ID = p.Product_ID
				ORDER BY Submission_Date DESC) as End_On_Hand
			FROM Product_User pu 
				INNER JOIN aspnet_Users u ON u.UserId = pu.UserId
				INNER JOIN Product p ON p.Product_ID = pu.Product_ID
				INNER JOIN Vendor v ON p.Vendor_ID = v.Vendor_ID
				INNER JOIN Equipment_User eu on pu.UserId = eu.UserId and pu.Product_ID = eu.Product_ID
			WHERE p.is_Active = 1) as P
		WHERE (End_On_Hand - (Avg_dispensed + Start_On_Hand)) < (.6 * Capacity) and Product_ID <> 51
		group by UserId, UserName, Product_ID, Product_Name, Vendor_ID, Measurement_Type, Sub_Type, Vendor_Name, Account_Number, Phone_Number
		ORDER BY UserName
END

GO