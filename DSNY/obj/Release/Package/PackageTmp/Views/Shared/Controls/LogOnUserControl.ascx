﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% if (Request.IsAuthenticated) { %>
        Welcome <b><%: Page.User.Identity.Name %></b>
        [ 
        <% if (Roles.IsUserInRole("System Administrator") || Roles.IsUserInRole("Reports")) { %>
            <%: Html.ActionLink("Site Administration", "Index", "Admin", null, new { @class = "menu" })%>  | 
            <a class="menu" href="<%: ConfigurationManager.AppSettings["RPM Link"].ToString() %>">RPM Console</a>  |
        <% }

            if (Roles.IsUserInRole("Administrator")) { %>
            <%: Html.ActionLink("Administrator Dashboard", "Admin", "Dashboard", null, new { @class = "menu" })%>  | 
        <% }
       
            if (Roles.IsUserInRole("Field") || Roles.IsUserInRole("Garage")) { %>
            <%: Html.ActionLink("Field Dashboard", "Field", "Dashboard", null, new { @class = "menu" })%>  | 
        <% } %>
        <%: Html.ActionLink("Log Off", "LogOff", "Home", null, new { @class = "menu" })%> ]
<% } %> 