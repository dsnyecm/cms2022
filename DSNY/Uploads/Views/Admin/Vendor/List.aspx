﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<DSNY.Data.Vendor>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Vendors
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script src="/Scripts/DSNYFunctions.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Vendors&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('ManageVendor')">?</a>]</h2>
    <p style="width: 415px;" class="dottedLine"></p>

    <div id="vendorGrid" style="width: 415px;">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Admin", action = "Index"}) %>" class="iconLink">
                <img border="0" src="/Content/Images/Icons/back.png" alt="Admin List" /><br />Admin<br />Menu
            </a>
        </div>
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Vendor", action = "Create"}) %>" class="iconLink">
                <img border="0" src="/Content/Images/Icons/create.png" alt="Create User" /><br />Create Vendor
            </a>
        </div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <% Html.RenderPartial("Grids/VendorGrid"); %>
    </div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 450px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            // set up our table sorter
            $("#vendorTable").tablesorter({ widgets: ["zebra"],
                widgetZebra: { css: ["alt", ""] }
            });

            // AJAX event which deletes the selected item
            $(".deleteButton").live('click', function () {
                var data = { id: $(this).attr("id"), name: $(this).attr("name") };

                if (confirm('Press \'OK\' to delete ' + data.name + '.')) {

                    $.get("/Vendor/Delete/", data, function (data) {
                        if (data.status == "success") {
                            $('#row-' + data.id).fadeOut('slow');
                            reStripe("vendorTable");
                        }
                        else {
                            $("#errorMessage").html("There was an error deleting " + data.name + ".<br /><br />");
                        }
                    }, 'json');
                }
            });
        });
    </script>
</asp:Content>