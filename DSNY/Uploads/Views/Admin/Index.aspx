﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Site Administration
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Site Administration</h2>
    <p class="dottedLine" style="width: 510px;"></p>

    <div>
        <% if (Roles.IsUserInRole("System Administrator")) { %>
                <div style="width: 250px; float: left; margin-left: 10px;">
                    <h3 style="margin: 0px;">User Administration</h3>
                    <ul>
                        <li><%: Html.ActionLink("Manage Users", "List", "Users") %></li>
                        <li><%: Html.ActionLink("Manage Message Distribution", "MessageDist", "Users")%></li>
                        <li><%: Html.ActionLink("Manage E-mail Distribution", "EmailDist", "Users")%></li>
                        <li><%: Html.ActionLink("View Roles", "List", "Roles") %></li>
                    </ul>
                </div>
                <div style="width: 250px; float: left;">
                    <h3 style="margin: 0px;">Product / Equipment Administration</h3>
                    <ul>
                        <li><%: Html.ActionLink("Manage Products", "List", "Products") %></li>
                        <li><%: Html.ActionLink("Manage Equipment", "List", "Equipment") %></li>
                        <li><%: Html.ActionLink("Manage Vendors", "List", "Vendor") %></li>
                    </ul>
                </div>
                <div class="clear"></div>
                <div style="width: 250px; float: left; margin-left: 10px; padding-top: 10px;">
                    <h3 style="margin: 0px;">Printer Status</h3>
                    <ul>
                        <li><a href="<%= ConfigurationManager.AppSettings["PrinterStatusLink"] %>" target="_blank">View Printer Status</a></li>
                    </ul>
                </div>
        <% }

        if (Roles.IsUserInRole("Reports")) { %>
            <div style="width: 250px; float: left; margin-left: 10px; padding-top: 10px;">
                <h3 style="margin: 0px; padding-bottom: 10px;">Reports</h3>
                <select id="reportSelect" onchange="if (this.selectedIndex > 0) { window.location='/Reports/reports.aspx?report=' + this.options[this.selectedIndex].value; }">
                    <option value="">Select Report</option>
                    <% foreach (string report in ConfigurationManager.AppSettings["Reports"].ToString().Split(';'))
                        { %>
                        <option value="<%: report %>"><%: report.Replace('_', ' ')%></option>       
                    <% } %>
                </select>
            </div>
        <% } %>
    </div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>