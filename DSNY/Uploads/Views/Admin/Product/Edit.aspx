﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.Data.Product>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit <%: Model.Product_Name %>
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="/Scripts/MicrosoftAjax.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftMvcValidation.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Edit <%: Model.Product_Name%></h2>
    <p class="dottedLine"></p>

    <div class="adminIconContainer">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Products", action = "List"}) %>" class="iconLink">
                <img border="0" src="/Content/Images/Icons/Cancel.png" alt="Cancel" /><br />Cancel
            </a>
        </div>
        <div class="iconLeft">
            <a onclick="document.forms[0].submit();" href="#" class="iconLink">
                <img border="0" src="/Content/Images/Icons/create.png" alt="Create"/><br />Save Product
            </a>
        </div>
    </div>

    <div class="clear"></div>

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Product Information</legend>

            <div class="editor-label"><%: Html.LabelFor(model => model.Product_Name)%>:</div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Product_Name)%>
                <%: Html.ValidationMessageFor(model => model.Product_Name)%>
            </div>

            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.Default_Capacity)%>:</div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Default_Capacity, new { style = "width: 50px;" })%>
                <%: Html.ValidationMessageFor(model => model.Default_Capacity)%>
            </div>
            
            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.Measurement_Type)%>:</div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Measurement_Type, new { style = "width: 75px;" })%>
                <%: Html.ValidationMessageFor(model => model.Measurement_Type)%>
            </div>

            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.is_Drums)%>:</div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.is_Drums)%>
                <%: Html.ValidationMessageFor(model => model.is_Drums)%>
            </div>

            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.Order_Num)%>:</div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Order_Num, new { style = "width: 30px;" })%>
                <%: Html.ValidationMessageFor(model => model.Order_Num)%>
            </div>

            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.is_Active)%>:</div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.is_Active)%>
                <%: Html.ValidationMessageFor(model => model.is_Active)%>
            </div>

            <div class="clear"></div>

        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>