﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DSNY.Core.Interfaces.IRole>" %>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.roleName) %>:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.roleName) %>
                &nbsp;&nbsp;<%: Html.ValidationMessageFor(model => model.roleName) %>
            </div>
            
            <div class="clear"></div>