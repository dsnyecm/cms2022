﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

     <% List<DSNY.Data.Equipment_User> equipment = ViewData["AvailableEquipment"] as List<DSNY.Data.Equipment_User>;
        List<DSNY.Data.Fuel_Form_Equipment_Failure> previousFailures = new List<DSNY.Data.Fuel_Form_Equipment_Failure>();
        DSNY.Data.Fuel_Form_Equipment_Failure previousFailure = null;

        if (ViewData["EquipFailure"] != null)
            previousFailures = (List<DSNY.Data.Fuel_Form_Equipment_Failure>)ViewData["EquipFailure"];

        if (equipment.Count > 0) { %>
        <table id="equipmentTable">
            <thead>
            <tr>
                <th style="width: 300px;">Equipment</th>
                <th style="width: 56px;">Is Down</th>
                <th style="width: 100px;">Failure Date</th>
                <th style="width: 100px;">Fix Date</th>
                <th style="width: 511px;">Remarks</th>
            </tr>
            </thead>
            <tbody>
        <%  int rowCount = 0;
            foreach (var item in equipment)
            {
                previousFailure = previousFailures.SingleOrDefault(pf => pf.Equipment_User_ID == item.Equipment_User_ID && pf.is_Equipment_Failure == true && pf.Fix_Date == null);
                
                if (rowCount % 2 == 0) {%>
                    <tr id="fail-<%: item.Equipment_ID %>.1">
                <% } else { %>
                    <tr id="fail-<%: item.Equipment_ID %>.1" class="alt">
                <% } %>
                    <td><%: item.Equipment.Equipment_Name%> - <%: item.Equipment_Description %></td>

                    <% if (previousFailure != null) { %>
                        <td style="text-align: center;">
                            <input type="checkbox" name="equipDown-<%: item.Equipment_User_ID %>" id="equipDown-<%: item.Equipment_User_ID %>"
                                <%: previousFailure.is_Equipment_Failure ? "checked" : string.Empty %> />
                        </td>
                        <td>
                            <input name="equipFail-<%: item.Equipment_User_ID %>" id="equipFail-<%: item.Equipment_User_ID %>" style="width: 70px;" readonly
                                value="<%: previousFailure.Failure_Date != null ? previousFailure.Failure_Date.Value.ToString("MM/dd/yyyy") : string.Empty %>" />
                            <script type="text/javascript">
                                new tcal({ 'formname': 'fuelForm', 'controlname': 'equipFail-<%: item.Equipment_User_ID %>' });
		                    </script> 
                        </td>
                        <td>
                            <input name="equipFix-<%: item.Equipment_User_ID %>" id="equipFix-<%: item.Equipment_User_ID %>" style="width: 70px;" readonly 
                                value="<%: previousFailure.Fix_Date != null ? previousFailure.Fix_Date.Value.ToString("MM/dd/yyyy") : string.Empty %>" />
                            <script type="text/javascript">
                                new tcal({ 'formname': 'fuelForm', 'controlname': 'equipFix-<%: item.Equipment_User_ID %>' });
		                    </script> 
                        </td>
                        <td>
                            <input type="text" id="equipRemark-<%: item.Equipment_User_ID %>" name="equipRemark-<%: item.Equipment_User_ID %>" style="width: 511px;" 
                                value="<%: previousFailure.Remarks %>" />
                        </td>
                    <% } else { %>
                        <td style="text-align: center;">
                            <input type="checkbox" name="equipDown-<%: item.Equipment_User_ID %>" id="equipDown-<%: item.Equipment_User_ID %>" />
                        </td>
                        <td>
                            <input name="equipFail-<%: item.Equipment_User_ID %>" id="equipFail-<%: item.Equipment_User_ID %>" style="width: 70px;" readonly />
                            <script type="text/javascript">
                                new tcal({ 'formname': 'fuelForm', 'controlname': 'equipFail-<%: item.Equipment_User_ID %>' });
		                    </script> 
                        </td>
                        <td>
                            <input name="equipFix-<%: item.Equipment_User_ID %>" id="equipFix-<%: item.Equipment_User_ID %>" style="width: 70px;" readonly />
                            <script type="text/javascript">
                                new tcal({ 'formname': 'fuelForm', 'controlname': 'equipFix-<%: item.Equipment_User_ID %>' });
		                    </script> 
                        </td>
                        <td>
                            <input type="text" id="equipRemark-<%: item.Equipment_User_ID %>" name="equipRemark-<%: item.Equipment_User_ID %>" style="width: 511px;" />
                        </td>
                    <% } %>
                </tr>
            <% rowCount++;
            } %>
            </tbody>
        </table>
    <% } else { %>
        <h3>No equipment has been assigned to this site.</h3>
    <% } %>