﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

     <% List<DSNY.Data.Equipment_User> equipment = ViewData["AvailableEquipment"] as List<DSNY.Data.Equipment_User>;
        List<DSNY.Data.Fuel_Form_Equipment_Failure> loadedFailures = new List<DSNY.Data.Fuel_Form_Equipment_Failure>();
        DSNY.Data.Fuel_Form_Equipment_Failure loadedFailure = null;

        if (ViewData["EquipFailure"] != null)
            loadedFailures = (List<DSNY.Data.Fuel_Form_Equipment_Failure>)ViewData["EquipFailure"];         

        if (equipment.Count > 0) { %>
        <table id="equipmentTable">
            <thead>
            <tr>
                <th style="width: 300px;">Equipment</th>
                <th style="width: 56px;">Is Down</th>
                <th style="width: 100px;">Failure Date</th>
                <th style="width: 100px;">Fix Date</th>
                <th style="width: 511px;">Remarks</th>
            </tr>
            </thead>
            <tbody>
        <%  int rowCount = 0;
            foreach (var item in equipment)
            {
                loadedFailure = loadedFailures.SingleOrDefault(lf => lf.Equipment_User_ID == item.Equipment_User_ID);
                
                if (rowCount % 2 == 0) {%>
                    <tr id="fail-<%: item.Equipment_ID %>.1">
                <% } else { %>
                    <tr id="fail-<%: item.Equipment_ID %>.1" class="alt">
                <% } %>
                    <td>
                        <%: item.Equipment.Equipment_Name%> - <%: item.Equipment_Description%>
                        <% if (loadedFailure != null) { %>
                            <input type="hidden" name="equipFailureId-<%: item.Equipment_User_ID %>" id="equipFailureId-<%: item.Equipment_User_ID %>" value="<%: loadedFailure.Equipment_Failure_ID %>" />
                        <% } %>
                    </td>

                    <td style="text-align: center;">
                        <input type="checkbox" name="equipDown-<%: item.Equipment_User_ID %>" id="equipDown-<%: item.Equipment_User_ID %>"
                        <%: (loadedFailure != null && loadedFailure.is_Equipment_Failure) ? "checked" : string.Empty %> />
                    </td>
                    <td>
                        <input name="equipFail-<%: item.Equipment_User_ID %>" id="equipFail-<%: item.Equipment_User_ID %>" style="width: 70px;" readonly />
                        <script type="text/javascript">
                        <% if (loadedFailure != null && loadedFailure.Failure_Date != null) { %>
                               new tcal({ 'formname': 'fuelForm', 'controlname': 'equipFail-<%: item.Equipment_User_ID %>', 'selected': '<%: loadedFailure.Failure_Date.Value.ToString("M/d/yyyy")%>' });
                        <% } else { %>
                               new tcal({ 'formname': 'fuelForm', 'controlname': 'equipFail-<%: item.Equipment_User_ID %>' });
                        <% } %>
		                </script> 
                    </td>
                    <td>
                        <input name="equipFix-<%: item.Equipment_User_ID %>" id="equipFix-<%: item.Equipment_User_ID %>" style="width: 70px;" readonly />
                        <script type="text/javascript">
                        <% if (loadedFailure != null && loadedFailure.Fix_Date != null) { %>
                               new tcal({ 'formname': 'fuelForm', 'controlname': 'equipFix-<%: item.Equipment_User_ID %>', 'selected': '<%: loadedFailure.Fix_Date.Value.ToString("M/d/yyyy")%>' });
                        <% } else { %>
                               new tcal({ 'formname': 'fuelForm', 'controlname': 'equipFix-<%: item.Equipment_User_ID %>' });
                        <% } %>
		                </script> 
                    </td>
                    <td>
                        <input type="text" id="equipRemark-<%: item.Equipment_User_ID %>" name="equipRemark-<%: item.Equipment_User_ID %>" style="width: 511px;" 
                        value="<%: (loadedFailure != null && loadedFailure.Remarks != null) ? loadedFailure.Remarks : string.Empty %>" />
                    </td>
                </tr>
            <% rowCount++;
            } %>
            </tbody>
        </table>
    <% } else { %>
        <h3>No equipment has been assigned to this site.</h3>
    <% } %>