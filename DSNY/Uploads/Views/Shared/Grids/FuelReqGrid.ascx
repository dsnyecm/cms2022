﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

    <% List<DSNY.Data.Vendor> vendors = ViewData["Vendors"] as List<DSNY.Data.Vendor>;
       List<DSNY.Data.Product_User> productUser = ViewData["AvailableProducts"] as List<DSNY.Data.Product_User>;
       List<DSNY.Data.Equipment_User> userProductEquip = ViewData["AvailableEquipment"] as List<DSNY.Data.Equipment_User>;

       if (productUser.Count > 0) { %>
        <table id="fuelReqTable" style="width: 1125px;">
            <thead>
            <tr>
                <th style="width: 250px;">Product</th>
                <th>Capacity</th>
                <th style="width: 95px;">Qty On Hand</th>
                <th style="width: 97px;">Spare Drums</th>
                <th style="width: 100px; text-align: center;">Vendor</th>
                <th style="width: 100px; text-align: center;">Invoice #</th>
                <th style="width: 115px; text-align: center;">Date</th>
                <th>Quantity</th>
            </tr>
            </thead>
            <tbody>
        <%  int rowCount = 0;
            List<DSNY.Data.Equipment_User> relatedEquip = null;
            
            foreach (var item in productUser)
            {
                relatedEquip = userProductEquip.Where(pe => pe.Product_ID == item.Product_ID && pe.Equipment.has_Capacity).ToList();

                   if (relatedEquip.Count > 0)
                   {
                       int euCount = 1;
                       foreach (DSNY.Data.Equipment_User eu in relatedEquip)
                       {
                           if (eu.Equipment.Equipment_ID != 10) // remove dispensors
                           {
                           %>
                            <tr productId="<%: item.Product_ID %>" class="<%: rowCount % 2 == 0 ? "alt" : string.Empty %>">
                                <td> 
                                   <%: item.Product.Product_Name%> - <%: eu.Equipment_Description%>
                                    <% if (!string.IsNullOrEmpty(item.Product.Measurement_Type)) %>
                                        (<%: item.Product.Measurement_Type%>)
                                    <input type="hidden" id="equipUser-<%: item.Product_ID %>.<%: euCount %>" name="equipUser-<%: item.Product_ID %>.<%: euCount %>" value="<%: eu.Equipment_User_ID %>" />
                                </td>
                                <td style="text-align: right;">
                                    <% if (eu.Capacity != null && eu.Capacity > 0)
                                       {%>
                                        <%: eu.Capacity%>
                                    <% }
                                       else if (item.Capacity != null && item.Capacity > 0)
                                       {%>
                                        <%: item.Capacity%>
                                    <% }
                                       else if (item.Product.Default_Capacity > 0)
                                       {%>
                                        <%: item.Product.Default_Capacity%>
                                    <% } %>
                                </td>
                                <td style="text-align: center;">
                                    <input type="text" id="prodqtyOnHand-<%: item.Product_ID %>.<%: euCount %>" name="prodqtyOnHand-<%: item.Product_ID %>.<%: euCount %>" style="width: 50px; text-align: right;" />
                                </td>
                                <td style="text-align: center;">
                                    <% if (item.Product.is_Drums)
                                       {%>
                                        <input type="text" id="proddrum-<%: item.Product_ID %>.<%: euCount %>" name="proddrum-<%: item.Product_ID %>.<%: euCount %>" style="width: 50px; text-align: right;" />
                                    <% } %>
                                </td>
                                <% if (euCount == 1)
                                   { %>
                                    <td style="text-align: center;">
                                        <select id="prodvendor-<%: item.Product_ID %>.<%: euCount %>" name="prodvendor-<%: item.Product_ID %>.1">
                                            <option value="">SELECT VENDOR</option>
                                        <% if (vendors != null)
                                           {
                                               foreach (DSNY.Data.Vendor vendor in vendors)
                                               { %>
		                                            <option value="<%: vendor.Vendor_ID %>"><%: vendor.Vendor_Name%></option>
	                                    <%      }
                                           } %>
                                        </select>
                                    </td>

                                    <td>
                                        <input type="text" id="prodinvoice-<%: item.Product_ID %>.1" name="prodinvoice-<%: item.Product_ID %>.1"  style="width: 95px; text-align: right;" />
                                    </td>
                                    <td>
                                        <input type="text" id="proddate-<%: item.Product_ID %>.1" name="proddate-<%: item.Product_ID %>.1" style="width: 70px;" size="10" readonly />
                                        <script type="text/javascript">
                                            new tcal({ 'formname': 'fuelForm', 'controlname': 'proddate-<%: item.Product_ID %>.1', 'id': 'proddatecal-<%: item.Product_ID %>.1' });
		                                </script> 
                                    </td>
                                    <td>
                                        <input type="text" id="prodqty-<%: item.Product_ID %>.1" name="prodqty-<%: item.Product_ID %>.1" style="width: 50px; text-align: right;" />
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="#" class="addRow" onclick="return false;">
                                            <img border="0" src="/Content/Images/Icons/add.png"  />
                                        </a>
                                    </td>
                                <% }
                                   else
                                   { %>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                <% } %>
                   <%       euCount++;
                            rowCount++;
                           }
                       }
                   }
                   else
                   { %>
                    <tr productId="<%: item.Product_ID %>" class="<%: rowCount % 2 == 0 ? "alt" : string.Empty %>">
                        <td>
                            <%: item.Product.Product_Name%>
                            <% if (!string.IsNullOrEmpty(item.Product.Measurement_Type)) %>
                                (<%: item.Product.Measurement_Type%>)
                        </td>
                        <td style="text-align: right;">
                            <%  if (item.Capacity != null && item.Capacity > 0)
                                {%>
                                <%: item.Capacity%>
                            <% }
                                else if (item.Product.Default_Capacity > 0)
                                {%>
                                <%: item.Product.Default_Capacity%>
                            <% } %>
                        </td>
                        <td style="text-align: center;">
                            <input type="text" id="prodqtyOnHand-<%: item.Product_ID %>.1" name="prodqtyOnHand-<%: item.Product_ID %>.1" style="width: 50px; text-align: right;" />
                        </td>
                        <td style="text-align: center;">
                            <% if (item.Product.is_Drums)
                               {%>
                                <input type="text" id="proddrum-<%: item.Product_ID %>.1" name="proddrum-<%: item.Product_ID %>.1" style="width: 50px; text-align: right;" />
                            <% } %>
                        </td>
                        <td style="text-align: center;">
                            <select id="prodvendor-<%: item.Product_ID %>.1" name="prodvendor-<%: item.Product_ID %>.1">
                                <option value="">SELECT VENDOR</option>
                            <% if (vendors != null)
                               {
                                   foreach (DSNY.Data.Vendor vendor in vendors)
                                   { %>
		                                <option value="<%: vendor.Vendor_ID %>"><%: vendor.Vendor_Name%></option>
	                        <%     }
                               } %>
                            </select>
                        </td>
                        <td>
                            <input type="text" id="prodinvoice-<%: item.Product_ID %>.1" name="prodinvoice-<%: item.Product_ID %>.1"  style="width: 95px; text-align: right;" />
                        </td>
                        <td>
                            <input type="text" id="proddate-<%: item.Product_ID %>.1" name="proddate-<%: item.Product_ID %>.1" style="width: 70px;" size="10" readonly />
                            <script type="text/javascript">
                                new tcal({ 'formname': 'fuelForm', 'controlname': 'proddate-<%: item.Product_ID %>.1', 'id': 'proddatecal-<%: item.Product_ID %>.1' });
		                    </script> 
                        </td>
                        <td>
                            <input type="text" id="prodqty-<%: item.Product_ID %>.1" name="prodqty-<%: item.Product_ID %>.1" style="width: 50px; text-align: right;" />
                        </td>
                        <td style="text-align: center;">
                            <a href="#" class="addRow" onclick="return false;">
                                <img border="0" src="/Content/Images/Icons/add.png"  />
                            </a>
                        </td>
                <%      rowCount++;
                    } %>
                </tr>
            <% } %>
            </tbody>
        </table>
    <% } else { %>
        <h3>No products have been assigned to this site.</h3>
    <% } %>