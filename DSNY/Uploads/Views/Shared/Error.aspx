﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<System.Web.Mvc.HandleErrorInfo>" %>

<asp:Content ID="errorTitle" ContentPlaceHolderID="TitleContent" runat="server">
Error
</asp:Content>

<asp:Content ID="errorContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 style="color: Red;">There was an error.</h2>
    <div style="font-size: 1.2em; color: Red;">When contacting support, please provide the following information.</div>

    <%
        Exception ex = ((HandleErrorInfo)ViewData.Model).Exception;
        
        System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
    %>

    <p><b>Error Message</b><br /><%= ex.Message %></p>
    <p><b>Stack Trace</b><br /><%= ex.StackTrace %></p>
    <p><b>Location</b><br /><%= trace.GetFrame(0).GetFileName() %> (Line <%= trace.GetFrame(0).GetFileLineNumber() %>)</p>
    
</asp:Content>
