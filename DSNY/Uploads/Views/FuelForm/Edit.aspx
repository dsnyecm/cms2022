﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.FuelFormViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Fuel Requirements and Equipment Form
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/calendar_us.js" type="text/javascript"></script>
    <script src="/Scripts/Tabs.js" type="text/javascript"></script>
    <script src="/Scripts/DSNYFunctions.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="/Content/calendar.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Fuel Requirements and Equipment Form&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('FuelRequirements')">?</a>]</h2>
    <p style="width: 1180px;" class="dottedLine"></p>

    <% using (Html.BeginForm("Edit", "FuelForm", FormMethod.Post, new { id = "fuelForm" })) {%>

        <div style="float: left; width: 1175px;">
            <div class="iconLeft">
                <a href="" onclick="history.go(-1); return false;" class="iconLink">
                    <img border="0" src="/Content/Images/Icons/cancel.png" alt="Cancel" /><br />Cancel
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" id="submitButton" class="iconLink">
                    <img border="0" src="/Content/Images/Icons/clipboard_check.png" alt="Submit" style="vertical-align: bottom;"/><br />Submit
                </a>
            </div>
        </div>

        <div class="clear"></div>

        <div style="float: left; margin-bottom: 10px; width: 1175px;">
            <div class="display-label" style="margin-top: .9em; width: 100px; font-weight: bold;"><label for="locationId">Location Id</label>: </div>
            <div class="display-field" style="margin: 0.9em 0; width: 125px;">
                <%: Model.submittedBy.userName %>
            </div>

            <div class="display-label" style="margin-top: .9em; font-weight: bold;"><label for="Supervisor_Full_Name">Supervisor</label>: </div>
            <div class="display-field" style="margin: 0.9em 0; width: 160px;">
                <%: Model.fuelForm.Supervisor_Full_Name %>
            </div>

            <div class="display-label" style="margin-top: .9em; font-weight: bold; width: 150px;"><label for="Supervisor_Badge_Number">Badge Number</label>: </div>
            <div class="display-field" style="margin: 0.9em 0; width: 185px;">
                <%: Model.fuelForm.Supervisor_Badge_Number %>
            </div>

            <div style="margin-top: .9em;"><%: DateTime.Now.ToString("M/d/yyyy h:mm tt")  %></div>
        </div>

        <div class="clear"></div>

        <ul id="tabnav">
            <li class="selectedTab"><a href="#" onclick="f_tcalHideAll();">Fuel Requirements</a></li>
            <% if (ViewData["EquipFailure"] != null && ((List<DSNY.Data.Fuel_Form_Equipment_Failure>)ViewData["EquipFailure"]).Exists(ef => ef.is_Equipment_Failure)) { %>
                <li><a href="#" onclick="f_tcalHideAll();">* Equipment Failure *</a></li>
            <% } else { %>
                <li><a href="#" onclick="f_tcalHideAll();">Equipment Failure</a></li>
            <% } %>
            <li><a href="#" onclick="setTimeout(function () { $('#Remarks').focus() }, 10); f_tcalHideAll();">General Remarks</a></li>
        </ul>

        <div id="tabsContainer" style="width: 1175px;">
            <div class="tabContent" style="width: 1175px;">
                <% Html.RenderPartial("Grids/FuelReqEditGrid", Model.fuelFormDetails); %>
            </div>

            <div class="tabContent" style="width: 1175px;">
                <% Html.RenderPartial("Grids/EquipFailEditGrid"); %>
            </div>

            <div class="tabContent" style="width: 1175px;">
                <%: Html.TextAreaFor(model => model.fuelForm.Remarks, new { @cols = "138", @rows = "20" })%>
                <%: Html.HiddenFor(model => model.fuelForm.Fuel_Form_ID) %>
            </div>
        </div>

        <div class="iconContainer" style="width: 1175px;">
            <div id="errorMessage" style="float: right; display: none; color: red; font-weight: bold; margin: 30px 20px 0px;">
                Please fill in all required fields including supervisor,<br />badge # and any partially completed rows.
            </div>
        </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 1200px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tabsContainer").tabs({ tab: 1 });
            preload(["/Content/Images/Icons/delete.png"]);

            $('.addRow').live('click', function () {
                var rowIndex = $(this).closest("tr").prevAll("tr").length + 1;
                var row = $(this).parents("table").find("tr:eq(" + rowIndex + ")");
                var productId = row.attr("productId");
                var copiedRow = row.clone();
                var numOfProductRows = getHighestProductRowNum(productId);

                copiedRow.find(".addRow > img").attr("src", "/Content/Images/Icons/delete.png").parent().attr("class", "removeRow");
                copiedRow.find("script").remove();

                copiedRow.find("img").each(function (index) {
                    if ($(this).attr('id') == ("tcalico_proddatecal-" + productId + '.1')) {
                        $(this).replaceWith('<img src="/content/images/calendar/cal.gif" id="tcalico_proddatecal-' + productId + '.' + (numOfProductRows + 1) + '" onclick="A_TCALS[\'proddatecal-'
                                + productId + '.' + (numOfProductRows + 1) + '\'].f_toggle()" class="tcalIcon" alt="Open Calendar"  />');
                    }
                });

                // clears first two cells on a copy
                copiedRow.find("td").each(function (index) {
                    if (index < 2) {
                        $(this).text("");
                    }
                });

                // removes input boxes in cells 3 & 4 on copied row or changes the id of control
                copiedRow.find(":input").each(function (index) {
                    var inputName = $(this).attr('name');

                    switch (inputName) {
                        case "prodqtyOnHand-" + productId + ".1":
                        case "proddrum-" + productId + ".1":
                            $(this).remove();
                            break;

                        default:
                            $(this).attr('id', inputName.replace(/\d+$/, (numOfProductRows + 1)));
                            $(this).attr('name', inputName.replace(/\d+$/, (numOfProductRows + 1)));
                            $(this).attr('value', '');
                    }
                });

                copiedRow.removeAttr("style");
                copiedRow.insertAfter($(this).parents("table").find("tr:eq(" + rowIndex + ")"));

                new tcal({
                    'formname': 'fuelForm',
                    'controlname': 'proddate-' + productId + '.' + (numOfProductRows + 1),
                    'id': 'proddatecal-' + productId + '.' + (numOfProductRows + 1)
                });
            });

            $('.removeRow').live('click', function () {
                var rowIndex = $(this).closest("tr").prevAll("tr").length + 1;
                var row = $(this).parents("table").find("tr:eq(" + rowIndex + ")");
                row.remove();
            });

            $("#submitButton").click(function () {
                var valid = true;

                $("#fuelReqTable tr").each(function (index) {
                    var oneComplete = false;
                    var oneBlank = false;

                    $(this).find(":input[id^='prod']").each(function () {
                        var inputName = $(this).attr('name');

                        if (inputName.indexOf("prodqtyOnHand") == -1 && inputName.indexOf("proddrum") == -1 && inputName.indexOf("prodDetailsId") == -1) {
                            if ($(this).val() == "") {
                                oneBlank = true;
                            } else {
                                oneComplete = true;
                            }
                        }
                        else if (inputName.indexOf("proddrum") > -1) {
                            if ($(this).val() == "" || !IsNumeric($(this).val())) {
                                valid = false;
                                oneBlank = true;
                                oneComplete = true;
                            }
                        }
                        else if (inputName.indexOf("prodqtyOnHand") > -1) {
                            if ($(this).val() == "" || !IsNumeric($(this).val())) {
                                valid = false;
                                oneBlank = true;
                                oneComplete = true;
                            }

                            if ($(this).parent().prev().html() != "" && $(this).val() != "") {
                                capacity = parseInt($(this).parent().prev().html());
                                qtyOnHand = parseInt($(this).val());

                                if (qtyOnHand > capacity) {
                                    valid = false;
                                    oneBlank = true;
                                    oneComplete = true;
                                }
                            }
                        }
                    });

                    if (oneComplete && oneBlank) {
                        $(this).attr("style", "background-color: #FF6666;");
                        valid = false;
                    } else {
                        $(this).removeAttr("style");
                    }

                });

                if (valid) {
                    $("#errorMessage").hide();
                    document.forms[0].submit();
                } else {
                    $("#errorMessage").show();
                }
            });
        });

        function getHighestProductRowNum(productId) {
            var highestNum = 0;

            $("#fuelReqTable").find("[productId=" + productId + "] :input").each(function (index) {
                if ($(this).attr('id').indexOf("prodqty-" + productId + ".") > -1) {
                    var parsedNum = parseInt($(this).attr('id').replace("prodqty-" + productId + ".", ""));
                    if (parsedNum > highestNum) {
                        highestNum = parsedNum;
                    }
                }

            });

            return highestNum;
        }

    </script>
</asp:Content>