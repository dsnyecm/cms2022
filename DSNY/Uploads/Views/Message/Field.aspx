﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.Data.Message>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	View Message
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>View Message</h2>
    <p class="dottedLine" style="width: 840px"></p>

    <div style="width: 840px;">
        <div style="float: right; width: 50px; text-align: center; font-size: .85em;">
            <a href="" onclick="window.name=window.name+',<%: Model.Message_Id %>'; history.go(-1); return false;" style="text-decoration: none; color: #696969;">
                <img border="0" src="/Content/Images/Icons/cancel.png" /><br />Close
            </a>
        </div>

       <div class="messageContainer" style="font-family: Monospace; font-size: 12pt;">
            <div class="message-display-label" style="margin: 0.2em 0;"><%: Html.LabelFor(model => model.Dept_Msg_No)%>:</div>
            <div class="message-display-field" style="width: 200px; margin: 0.2em 0;"><%: Model.Dept_Msg_No%></div>

            <% if (Model.aspnet_Users != null) { %>
                <div class="message-display-label" style="margin: 0.2em 0;"><%: Html.LabelFor(model => model.Sent_By_UserId)%>:</div>
                <div class="message-display-field" style="width: 200px; margin: 0.2em 0;"><%: Model.aspnet_Users.UserName.ToString()%></div>
            <% } %>

            <div class="clear"></div>

            <div class="message-display-label" style="margin: 0.2em 0;"><%: Html.LabelFor(model => model.Message_Date)%>:</div>
            
            <% if (Model.Message_Date.Value.TimeOfDay == new TimeSpan(0, 0, 0)) {%>
            <div class="message-display-field" style="width: 200px; margin: 0.2em 0;"><%: Model.Message_Date.Value.ToString("M/d/yyyy")%></div>
            <% } else { %>
            <div class="message-display-field" style="width: 200px; margin: 0.2em 0;"><%: Model.Message_Date.Value.ToString("M/d/yyyy h:mm tt")%></div>
            <% } %>

            <div class="message-display-label" style="margin: 0.2em 0;"><%: Html.LabelFor(model => model.Message_Code)%>:</div>
            <div class="message-display-field" style="width: 200px; margin: 0.2em 0;"><%: Model.Message_Code%></div>

            <div class="clear"></div>

            <div class="message-display-label" style="margin: 0.2em 0;"><%: Html.LabelFor(model => model.Message_Subject)%>:</div>
            <div class="message-display-field" style="width: 600px; margin: 0.2em 0;"><%: Model.Message_Subject%></div>

            <div class="clear"></div>

            <div class="message-display-field messageText" style="width: 740px; margin-top: 5px; line-height: 1.1em;"><pre id="Message_Text"><%: Model.Message_Text %></pre></div>
        </div>
    </div>

    <div class="clear"></div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 880px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="JavascriptSource" runat="server">

<script type="text/javascript">
    $(document).ready(function () {
        if ($.browser.msie && parseInt($.browser.version, 10) == 7) {
            $("#Message_Text").text($("#Message_Text").text().replace(new RegExp('\t', 'g'), '\t\t'));
        }
    });
</script>

</asp:Content>