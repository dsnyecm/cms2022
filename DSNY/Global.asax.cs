﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

using DSNY.Core.Interfaces;
using DSNY.Core.Models;
using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Models;
using DSNY.Data;
using DSNY.Mvc;

namespace DSNY
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        // Static variable to hold the global pageSize
        static int _pageSize = 0;

        /// <summary>
        /// Get or set the global page size variable
        /// </summary>
        public static int pageSize
        {
            get
            {
                if (_pageSize == 0)
                    int.TryParse(ConfigurationManager.AppSettings["PageSize"], out _pageSize);

                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region Role Routes

            routes.MapRoute("Role Edit", "Roles/{action}/{name}", // URL with parameters
                new { controller = "Role" } // Parameter defaults
            );

            routes.MapRoute("Roles List", "Roles/{action}", // URL with parameters
                new { controller = "Role", action = "List" } // Parameter defaults
            );

            #endregion

            #region User Routes

            routes.MapRoute("User Edit", "Users/{action}/{name}", // URL with parameters
                new { controller = "User" } // Parameter defaults
            );

            routes.MapRoute("User List", "Users/{action}", // URL with parameters
                new { controller = "User", action = "List" } // Parameter defaults
            );

            #endregion

            #region Product Routes

            routes.MapRoute("Product Edit", "Products/{action}/{id}", // URL with parameters
                new { controller = "Product" } // Parameter defaults
            );

            routes.MapRoute("Product List", "Products/{action}", // URL with parameters
                new { controller = "Product", action = "List" } // Parameter defaults
            );

            #endregion

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);

            //Configure for Dependecny Injection using Unity
            Bootstrapper.ConfigureUnityContainer();
        }

        // Force garbage collection at the end of a request
        // Seems like there is a bug in entities framework that does not release memory after a query is executed
        // This keeps memory usage reasonable
        protected void Application_EndRequest()
        {
            GC.Collect();
        }
    }
}