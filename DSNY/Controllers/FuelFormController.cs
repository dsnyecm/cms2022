﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Core.Models;
using DSNY.Core.Repository;
using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Data;
using DSNY.ViewModels;
using System.Web.Script.Serialization;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for <see cref="FuelForm"/> pages
    /// </summary>
    [HandleError]
    public class FuelFormController : Controller
    {
        #region Variables

        private IFuelFormRepository _fuelFormRepository;
        private IMessageRepository _messageRepository;        
        private IProductRepository _productRepository;
        private IEquipmentRepository _equipmentRepository;
        private IVendorRepository _vendorRepository;
        private IUserRepository _userRepository;
        private IDSNYExceptionRepository _dsnyExceptionRepository;
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FuelFormController"/> class.
        /// </summary>
        public FuelFormController() : this(null, null, null, null, null, null, null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FuelFormController"/> class.
        /// </summary>
        /// <param name="IoCFuelForm">The inversion of control fuel form.</param>
        /// <param name="IoCMessageRepository">The inversion of control message repository.</param>
        /// <param name="IoCProductRepo">The inversion of control product repo.</param>
        /// <param name="IoCEquipRepo">The inversion of control equip repo.</param>
        /// <param name="IoCVendorRepo">The inversion of control vendor repo.</param>
        /// <param name="IoCUserRepo">The inversion of control user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public FuelFormController(IFuelFormRepository IoCFuelForm, IMessageRepository IoCMessageRepository, IProductRepository IoCProductRepo,
            IEquipmentRepository IoCEquipRepo, IVendorRepository IoCVendorRepo, IUserRepository IoCUserRepo, IDSNYExceptionRepository IoCDSNYExceptionRepo, 
            ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {

            _fuelFormRepository = IoCFuelForm;
            _messageRepository = IoCMessageRepository;
            _productRepository = IoCProductRepo ;
            _equipmentRepository = IoCEquipRepo;
            _vendorRepository = IoCVendorRepo;
            _userRepository = IoCUserRepo;
            _dsnyExceptionRepository = IoCDSNYExceptionRepo;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Displays fuel form for entry
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        public ActionResult Create()
        {
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;

            // checking for remarks from previous day fuel form
            Fuel_Form previousFuelForm = _fuelFormRepository.getLastFuelForm(currentUserGuid, 1);
            Fuel_Form newFuelForm = new Fuel_Form();

            if (previousFuelForm != null)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                if (!string.IsNullOrEmpty(previousFuelForm.Remarks))
                {
                    newFuelForm.Remarks = previousFuelForm.Remarks;
                    //newFuelForm.Remarks = previousFuelForm.Remarks.Replace("\r\n", "\\r\\n");
                }

                if (previousFuelForm.Fuel_Form_Equipment_Failure != null && previousFuelForm.Fuel_Form_Equipment_Failure.Count > 0)
                    ViewData["EquipFailure"] = previousFuelForm.Fuel_Form_Equipment_Failure.ToList();

                ViewData["PreviousFuelFormDetails"] = serializer.Serialize(
                    previousFuelForm.Fuel_Form_Details.Select(f => new
                    {
                        productId = f.Product_ID,
                        spareDrums = f.Spare_Drums,
                        tankDelivered = f.Tank_Delivered,
                        tankDispensed = f.Tank_Dispensed,
                        qtyOnHand = f.On_Hand
                    }).ToList()
                );
            }

            // load up user's products, equipment, vendors
            ViewData["AvailableProducts"] = _productRepository.getProductsForUser(currentUserGuid);
            ViewData["AvailableEquipment"] = _equipmentRepository.getEquipmentForUser(currentUserGuid, false);
            ViewData["Vendors"] = _vendorRepository.getAllVendors(false);

            return View(newFuelForm);
        }

        /// <summary>
        /// Creates a fuel form from the model and form collection
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        [HttpPost]
        public ActionResult Create(Fuel_Form model, FormCollection collection)
        {
            // setup some variables
            int fuelFormId = 0, fuelFormDetails = 0;

            // message variable
            Message newMessage = null;

            // Fuel Requirements
            int productId = 0, productCount = 0, equipUserId = 0;
            int qtyOnHand = 0, spareDrums = 0, tankDelivered = 0, tankDispensed = 0, quantity = 0, vendorId = 0;
            DateTime receiveDate = DateTime.MinValue;
            string strProductId = string.Empty, strProductCount = string.Empty, invoiceNumber = string.Empty;

            // Equipment Failure
            int equipmentId = 0;
            DateTime failureDate = DateTime.MinValue, fixDate = DateTime.MinValue;
            string strEquipmentId = string.Empty;

            // initialize the fuel and details forms
            Fuel_Form newFuelForm = null;
            Fuel_Form_Details newFuelDetails = null;
            
            try
            {
                // add a new fuel form
                newFuelForm = new Fuel_Form()
                {
                    UserId = (Guid)Membership.GetUser().ProviderUserKey,
                    Supervisor_Full_Name = model.Supervisor_Full_Name,
                    Supervisor_Badge_Number = model.Supervisor_Badge_Number,
                    Remarks = model.Remarks,
                    Submission_Date = DateTime.Now
                };

                // add the form to the DB and grab its Id
                _fuelFormRepository.addFuelForm(newFuelForm);
                fuelFormId = newFuelForm.Fuel_Form_ID;

                newMessage = new Message()
                {
                    is_Fuel_Form = true,
                    Message_Date = DateTime.Now,
                    Message_Subject = "Fuel Form from " + User.Identity.Name,
                    Message_Code = "Fuel Form",
                    Sent_By_UserId = (Guid)Membership.GetUser().ProviderUserKey,
                    Fuel_Form_ID = newFuelForm.Fuel_Form_ID
                };
            }
            catch (Exception)
            {
                return RedirectToAction("Field", "Dashboard");
            }

            // now we loop through the form collection keys to grab the data about the fuel form
            foreach (string key in collection.Keys)
            {
                if (!string.IsNullOrEmpty(collection[key]))
                {
                    if (key.Contains("prodqtyOnHand-"))
                    {
                        // get quantity on hand
                        int.TryParse(collection[key].ToString(), out qtyOnHand);

                        // get product ID form key name
                        strProductId = key.Replace("prodqtyOnHand-", "");
                        strProductId = strProductId.Substring(0, strProductId.IndexOf("."));
                        int.TryParse(strProductId, out productId);

                        // get product count from key name
                        strProductCount = key.Replace("prodqtyOnHand-", "");
                        strProductCount = strProductCount.Substring(strProductCount.IndexOf(".") + 1);
                        int.TryParse(strProductCount, out productCount);

                        if (productId > 0 && qtyOnHand >= 0 && newFuelForm != null)
                        {
                            spareDrums = 0;
                            tankDelivered = 0;
                            tankDispensed = 0;

                            if (collection["proddrum-" + productId.ToString() + ".1"] != null)
                                int.TryParse(collection["proddrum-" + productId.ToString() + ".1"].ToString(), out spareDrums);

                            if (collection["proddel-" + productId.ToString() + ".1"] != null)
                                int.TryParse(collection["proddel-" + productId.ToString() + ".1"].ToString(), out tankDelivered);

                            if (collection["proddis-" + productId.ToString() + ".1"] != null)
                                int.TryParse(collection["proddis-" + productId.ToString() + ".1"].ToString(), out tankDispensed);

                            // get the equipment User ID
                            if (collection["equipUser-" + productId.ToString() + "." + productCount] != null)
                                int.TryParse(collection["equipUser-" + productId.ToString() + "." + productCount].ToString(), out equipUserId);

                            newFuelDetails = new Fuel_Form_Details()
                            {
                                Product_ID = productId,
                                Fuel_Form_ID = newFuelForm.Fuel_Form_ID,
                                On_Hand = qtyOnHand,
                                Equipment_User_ID = equipUserId > 0 ? equipUserId : (int?) null
                            };

                            if (spareDrums > 0)
                                newFuelDetails.Spare_Drums = spareDrums;

                            if (tankDelivered > 0)
                                newFuelDetails.Tank_Delivered = tankDelivered;

                            if (tankDispensed > 0)
                                newFuelDetails.Tank_Dispensed = tankDispensed;

                            // add the new details and grab its Id
                            _fuelFormRepository.addFuelFormDetails(newFuelDetails);
                            fuelFormDetails = newFuelDetails.Fuel_Form_Details_Id;
                        }
                    }
                    else if (key.Contains("prodinvoice-"))
                    {
                        // get product ID form key name
                        strProductId = key.Replace("prodinvoice-", "");
                        strProductId = strProductId.Substring(0, strProductId.IndexOf("."));
                        int.TryParse(strProductId, out productId);

                        // get product count from key name
                        strProductCount = key.Replace("prodinvoice-", "");
                        strProductCount = strProductCount.Substring(strProductCount.IndexOf(".") + 1);
                        int.TryParse(strProductCount, out productCount);

                        // reset variables
                        quantity = -1;
                        vendorId = -1;
                        receiveDate = DateTime.MinValue;

                        // gather our collection variables and put into correct data types
                        int.TryParse(collection["prodqty-" + productId + "." + productCount].ToString(), out quantity);
                        int.TryParse(collection["prodvendor-" + productId + "." + productCount].ToString(), out vendorId);
                        DateTime.TryParse(collection["proddate-" + productId + "." + productCount].ToString(), out receiveDate);
                        invoiceNumber = collection["prodinvoice-" + productId + "." + productCount] != null ?
                            collection["prodinvoice-" + productId + "." + productCount].ToString() : string.Empty;

                        if (newFuelDetails != null)
                        {
                            // create a new deliever, based off the first entry of this fuel form details
                            Fuel_Form_Details_Delivery newDelivery = new Fuel_Form_Details_Delivery()
                            {
                                Fuel_Form_Details = newFuelDetails,
                                Order_No = productCount
                            };

                            try
                            {
                                if (quantity > -1)
                                    newDelivery.Quantity = quantity;

                                if (vendorId > -1)
                                    newDelivery.Vendor_ID = vendorId;

                                if (!string.IsNullOrEmpty(invoiceNumber))
                                    newDelivery.Invoice_Number = invoiceNumber;

                                if (receiveDate > DateTime.MinValue)
                                    newDelivery.Recieve_Date = receiveDate;
                            }
                            catch (Exception ex)
                            {
                                _exceptionHandler.HandleException(ex);
                            }
                            finally
                            {
                                if (newDelivery != null)
                                    _fuelFormRepository.addFuelFormDetailsDelivery(newDelivery);
                            }
                        }
                    }
                    else if (key.Contains("equipFail-"))
                    {
                        // reset variables 
                        failureDate = DateTime.MinValue;
                        fixDate = DateTime.MinValue;

                        // get equipment ID form key name
                        strEquipmentId = key.Replace("equipFail-", "");
                        int.TryParse(strEquipmentId, out equipmentId);
                        DateTime.TryParse(collection["equipFail-" + equipmentId].ToString(), out failureDate);
                        DateTime.TryParse(collection["equipFix-" + equipmentId].ToString(), out fixDate);

                        if (equipmentId > 0 && newFuelForm != null)
                        {
                            Fuel_Form_Equipment_Failure equipFailure = new Fuel_Form_Equipment_Failure()
                            {
                                Equipment_User_ID = equipmentId,
                                is_Equipment_Failure = collection["equipDown-" + equipmentId] != null ? true : false,
                                Fuel_Form_ID = newFuelForm.Fuel_Form_ID,
                                Remarks = !(string.IsNullOrEmpty(collection["equipRemark-" + equipmentId])) ? collection["equipRemark-" + equipmentId] : null,
                            };

                            try
                            {
                                if (failureDate > DateTime.MinValue)
                                    equipFailure.Failure_Date = failureDate;

                                if (fixDate > DateTime.MinValue)
                                    equipFailure.Fix_Date = fixDate;
                            }
                            catch (Exception ex)
                            {
                                _exceptionHandler.HandleException(ex);
                            }
                            finally
                            {
                                if (equipFailure != null)
                                    _fuelFormRepository.addFuelFormEquipFailure(equipFailure);
                            }
                        }
                    }
                }

                productId = 0;
                productCount = 0;
                equipUserId = 0;
                equipmentId = 0;
            }

            if (newMessage != null)
                _messageRepository.addCommandMessage(newMessage, true);

            return RedirectToAction("Field", "Dashboard");
        }

        /// <summary>
        /// Displays fuel form for entry
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        public ActionResult Edit()
        {
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
            int fuelFormId = 0;

            // checking for remarks from previous day fuel form
            Fuel_Form previousFuelForm = _fuelFormRepository.getFuelForm(currentUserGuid, DateTime.Now);

            if (previousFuelForm != null)
                int.TryParse(previousFuelForm.Fuel_Form_ID.ToString(), out fuelFormId);

            if (fuelFormId > 0)
            {
                Fuel_Form loadedFuelForm = _fuelFormRepository.getFuelForm(fuelFormId);
                List<Fuel_Form_Details> fuelFormDetails = _fuelFormRepository.getFuelFormDetails(fuelFormId);
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                ViewData["EquipFailure"] = _fuelFormRepository.getFuelFormEquipFailure(fuelFormId);

                IUser submittedBy = _userRepository.getUser((Guid)loadedFuelForm.UserId);

                FuelFormViewModel editModel = new FuelFormViewModel()
                {
                    fuelForm = loadedFuelForm,
                    fuelFormDetails = fuelFormDetails,
                    submittedBy = submittedBy
                };

                ViewData["AvailableProducts"] = _productRepository.getProductsForUser(currentUserGuid);
                ViewData["AvailableEquipment"] = _equipmentRepository.getEquipmentForUser(currentUserGuid, false);
                ViewData["Vendors"] = _vendorRepository.getAllVendors(false);
                ViewData["UserProducts"] = _productRepository.getProductsForUser((Guid)loadedFuelForm.UserId);
                ViewData["PreviousFuelFormDetails"] = serializer.Serialize(
                    _fuelFormRepository.getLastFuelForm(currentUserGuid, 2).Fuel_Form_Details.Select(f => new
                    {
                        productId = f.Product_ID,
                        spareDrums = f.Spare_Drums,
                        tankDelivered = f.Tank_Delivered,
                        tankDispensed = f.Tank_Dispensed,
                        qtyOnHand = f.On_Hand
                    }).ToList()
                );

                return View(editModel);
            }
            else
            {
                return RedirectToAction("Create", "FuelForm");
            }
        }

        /// <summary>
        /// Creates a fuel form from the model and form collection
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        [HttpPost]
        public ActionResult Edit(FuelFormViewModel model, FormCollection collection)
        {
            // setup some variables
            int fuelFormDetails = 0, fuelFormEquipFailure = 0;
            bool isAdmin = collection.GetValue("editType") != null && collection["editType"] == "admin";
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;

            // Fuel Requirements
            int productId = 0, productCount = 0;
            int qtyOnHand = 0, spareDrums = 0, tankDelivered = 0, tankDispensed = 0, quantity = 0, vendorId = 0;
            DateTime receiveDate = DateTime.MinValue;
            string strProductId = string.Empty, strProductCount = string.Empty, invoiceNumber = string.Empty;

            // Equipment Failure
            int equipmentId = 0;
            DateTime failureDate = DateTime.MinValue, fixDate = DateTime.MinValue;
            string strEquipmentId = string.Empty;

            // initialize the fuel and details forms
            Fuel_Form updateFuelForm = null;
            Fuel_Form_Details updateFuelDetails = null;

            try
            {
                // add a new fuel form
                updateFuelForm = new Fuel_Form()
                {
                    Fuel_Form_ID = model.fuelForm.Fuel_Form_ID,
                    Remarks = model.fuelForm.Remarks,
                    Submission_Date = isAdmin ? model.fuelForm.Submission_Date : DateTime.Now
                };

                // add the form to the DB and grab its Id
                _fuelFormRepository.updateFuelForm(model.fuelForm.Fuel_Form_ID, updateFuelForm, (Guid)Membership.GetUser().ProviderUserKey);
            }
            catch (Exception)
            {
                if (isAdmin)
                    return RedirectToAction("Search", "FuelForm");
                else
                    return RedirectToAction("Field", "Dashboard");
            }

            // now we loop through the form collection keys to grab the data about the fuel form
            foreach (string key in collection.Keys)
            {
                if (!string.IsNullOrEmpty(collection[key]))
                {
                    if (key.Contains("prodqtyOnHand-"))
                    {
                        // get quantity on hand
                        int.TryParse(collection[key].ToString(), out qtyOnHand);

                        // get product ID form key name
                        strProductId = key.Replace("prodqtyOnHand-", "");
                        strProductId = strProductId.Substring(0, strProductId.IndexOf("."));
                        int.TryParse(strProductId, out productId);

                        // get product count from key name
                        strProductCount = key.Replace("prodqtyOnHand-", "");
                        strProductCount = strProductCount.Substring(strProductCount.IndexOf(".") + 1);
                        int.TryParse(strProductCount, out productCount);

                        if (collection["prodDetailsId-" + productId].ToString().Contains(','))
                        {
                            int.TryParse(collection["prodDetailsId-" + productId].ToString().Split(',')[0], out fuelFormDetails);
                            int tempFuelFormDetails = 0, prodDetailCount = 0;

                            foreach (string prodDetailsId in collection["prodDetailsId-" + productId].ToString().Split(','))
                            {
                                int.TryParse(prodDetailsId, out tempFuelFormDetails);
                                int.TryParse(collection[key].ToString().Split(',')[prodDetailCount], out qtyOnHand);

                                if (productId > 0 && qtyOnHand >= 0)
                                {
                                    spareDrums = 0;
                                    tankDelivered = 0;
                                    tankDispensed = 0;

                                    if (collection["proddrum-" + productId.ToString() + ".1"] != null)
                                        int.TryParse(collection["proddrum-" + productId.ToString() + ".1"].ToString(), out spareDrums);

                                    if (collection["proddel-" + productId.ToString() + ".1"] != null)
                                        int.TryParse(collection["proddel-" + productId.ToString() + ".1"].ToString(), out tankDelivered);

                                    if (collection["proddis-" + productId.ToString() + ".1"] != null)
                                        int.TryParse(collection["proddis-" + productId.ToString() + ".1"].ToString(), out tankDispensed);


                                    updateFuelDetails = new Fuel_Form_Details()
                                    {
                                        Fuel_Form_Details_Id = tempFuelFormDetails,
                                        Product_ID = productId,
                                        Fuel_Form_ID = model.fuelForm.Fuel_Form_ID,
                                        On_Hand = qtyOnHand,
                                    };

                                    if (spareDrums > 0)
                                        updateFuelDetails.Spare_Drums = spareDrums;

                                    if (tankDelivered > 0)
                                        updateFuelDetails.Tank_Delivered = tankDelivered;

                                    if (tankDispensed > 0)
                                        updateFuelDetails.Tank_Dispensed = tankDispensed;

                                    // add the new details and grab its Id
                                    _fuelFormRepository.updateFuelFormDetails(tempFuelFormDetails, updateFuelDetails, (Guid)Membership.GetUser().ProviderUserKey);
                                }

                                tempFuelFormDetails = 0;
                                prodDetailCount++;
                            }
                        }
                        else
                        {
                            int.TryParse(collection["prodDetailsId-" + productId].ToString(), out fuelFormDetails);

                            if (productId > 0 && qtyOnHand >= 0)
                            {
                                spareDrums = 0;
                                tankDelivered = 0;
                                tankDispensed = 0;

                                if (collection["proddrum-" + productId.ToString() + ".1"] != null)
                                    int.TryParse(collection["proddrum-" + productId.ToString() + ".1"].ToString(), out spareDrums);

                                if (collection["proddel-" + productId.ToString() + ".1"] != null)
                                    int.TryParse(collection["proddel-" + productId.ToString() + ".1"].ToString(), out tankDelivered);

                                if (collection["proddis-" + productId.ToString() + ".1"] != null)
                                    int.TryParse(collection["proddis-" + productId.ToString() + ".1"].ToString(), out tankDispensed);

                                updateFuelDetails = new Fuel_Form_Details()
                                {
                                    Fuel_Form_Details_Id = fuelFormDetails,
                                    Product_ID = productId,
                                    Fuel_Form_ID = model.fuelForm.Fuel_Form_ID,
                                    On_Hand = qtyOnHand,
                                };

                                if (spareDrums > 0)
                                    updateFuelDetails.Spare_Drums = spareDrums;

                                if (tankDelivered > 0)
                                    updateFuelDetails.Tank_Delivered = tankDelivered;

                                if (tankDispensed > 0)
                                    updateFuelDetails.Tank_Dispensed = tankDispensed;

                                // add the new details and grab its Id
                                _fuelFormRepository.updateFuelFormDetails(fuelFormDetails, updateFuelDetails, (Guid)Membership.GetUser().ProviderUserKey);
                            }

                        }
                    }
                    else if (key.Contains("prodinvoice-") && fuelFormDetails > 0)
                    {
                        // reset variables
                        quantity = -1;
                        vendorId = -1;
                        receiveDate = DateTime.MinValue;
                        
                        // get product ID form key name
                        strProductId = key.Replace("prodinvoice-", "");
                        strProductId = strProductId.Substring(0, strProductId.IndexOf("."));
                        int.TryParse(strProductId, out productId);

                        // get product count from key name
                        strProductCount = key.Replace("prodinvoice-", "");
                        strProductCount = strProductCount.Substring(strProductCount.IndexOf(".") + 1);
                        int.TryParse(strProductCount, out productCount);

                        // gather our collection variables and put into correct data types
                        int.TryParse(collection["prodqty-" + productId + "." + productCount].ToString(), out quantity);
                        int.TryParse(collection["prodvendor-" + productId + "." + productCount].ToString(), out vendorId);
                        DateTime.TryParse(collection["proddate-" + productId + "." + productCount].ToString(), out receiveDate);
                        invoiceNumber = collection["prodinvoice-" + productId + "." + productCount] != null ?
                            collection["prodinvoice-" + productId + "." + productCount].ToString() : string.Empty;

                        // create a new deliever, based off the first entry of this fuel form details
                        Fuel_Form_Details_Delivery updateDelivery = new Fuel_Form_Details_Delivery()
                        {
                            Fuel_Form_Details_Id = fuelFormDetails,
                            Order_No = productCount
                        };

                        if (quantity > -1)
                            updateDelivery.Quantity = quantity;

                        if (vendorId > -1)
                            updateDelivery.Vendor_ID = vendorId;

                        if (!string.IsNullOrEmpty(invoiceNumber))
                            updateDelivery.Invoice_Number = invoiceNumber;

                        if (receiveDate > DateTime.MinValue)
                            updateDelivery.Recieve_Date = receiveDate;

                        if (!_fuelFormRepository.updateFuelFormDetailsDelivery(fuelFormDetails, productCount, updateDelivery, (Guid)Membership.GetUser().ProviderUserKey))
                        {
                            _fuelFormRepository.addFuelFormDetailsDelivery(updateDelivery);
                        }
                    }
                    else if (key.Contains("equipFail-"))
                    {
                        // reset variables 
                        failureDate = DateTime.MinValue;
                        fixDate = DateTime.MinValue;
                        fuelFormEquipFailure = -1;

                        // get equipment ID form key name
                        strEquipmentId = key.Replace("equipFail-", "");
                        int.TryParse(strEquipmentId, out equipmentId);

                        DateTime.TryParse(collection["equipFail-" + equipmentId].ToString(), out failureDate);
                        DateTime.TryParse(collection["equipFix-" + equipmentId].ToString(), out fixDate);

                        Fuel_Form_Equipment_Failure equipFailure = new Fuel_Form_Equipment_Failure()
                        {
                            is_Equipment_Failure = collection["equipDown-" + equipmentId] != null ? true : false,
                            Remarks = !(string.IsNullOrEmpty(collection["equipRemark-" + equipmentId])) ? collection["equipRemark-" + equipmentId] : null
                        };

                        if (failureDate > DateTime.MinValue)
                            equipFailure.Failure_Date = failureDate;

                        if (fixDate > DateTime.MinValue)
                            equipFailure.Fix_Date = fixDate;

                        if (equipmentId > 0)
                        {
                            if (collection["equipFailureId-" + equipmentId] != null)
                            {
                                int.TryParse(collection["equipFailureId-" + equipmentId].ToString(), out fuelFormEquipFailure);

                                _fuelFormRepository.updateFuelFormEquipFailure(fuelFormEquipFailure, equipFailure, (Guid)Membership.GetUser().ProviderUserKey);
                            }
                            else
                            {
                                equipFailure.Equipment_User_ID = equipmentId;
                                equipFailure.Fuel_Form_ID = updateFuelForm.Fuel_Form_ID;

                                if (failureDate > DateTime.MinValue)
                                    equipFailure.Failure_Date = failureDate;

                                if (fixDate > DateTime.MinValue)
                                    equipFailure.Fix_Date = fixDate;

                                _fuelFormRepository.addFuelFormEquipFailure(equipFailure);
                            }
                        }
                    }
                }

                productId = 0;
                productCount = 0;
                equipmentId = 0;
            }

            // add exceptions
            _dsnyExceptionRepository.addExceptions(_dsnyExceptionRepository.convertExceptions(collection["exceptions"], currentUserGuid));

            //if (newMessage != null)
            //    _messageRepository.addCommandMessage(newMessage, true);

            if (isAdmin)
            {
                TempData["saved"] = true;
                return RedirectToAction("Search", "FuelForm", true);
            }
            else
                return RedirectToAction("Field", "Dashboard");
        }

        /// <summary>
        /// Displays fuel form
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        public ActionResult View(int id)
        {
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
            int fuelFormId = 0;

            Message fuelFormMsg = _messageRepository.getMessage(id, currentUserGuid);

            if (fuelFormMsg != null && fuelFormMsg.Fuel_Form_ID != null)
                int.TryParse(fuelFormMsg.Fuel_Form_ID.ToString(), out fuelFormId);

            if (fuelFormId > 0)
            {
                Fuel_Form fuelForm = _fuelFormRepository.getFuelForm(fuelFormId);
                List<Fuel_Form_Details> fuelFormDetails = _fuelFormRepository.getFuelFormDetails(fuelFormId);
                List<Fuel_Form_Equipment_Failure> fuelEquipFailure = _fuelFormRepository.getFuelFormEquipFailure(fuelFormId);
                IUser submittedBy = _userRepository.getUser((Guid)fuelForm.UserId);

                FuelFormViewModel displayModel = new FuelFormViewModel()
                {
                    fuelForm = fuelForm,
                    fuelFormDetails = fuelFormDetails,
                    fuelFormEquipFailure = fuelEquipFailure,
                    submittedBy = submittedBy
                };

                ViewData["UserProducts"] = _productRepository.getProductsForUser((Guid)fuelForm.UserId);
                ViewData["UserEquipProducts"] = _equipmentRepository.getEquipmentForUser((Guid)fuelForm.UserId, false);
                ViewData["EquipFailure"] = fuelEquipFailure;
                _messageRepository.updateMessageTimestamp(id, (Guid)Membership.GetUser().ProviderUserKey);

                return View(displayModel);
            }
            else
            {
                return RedirectToAction("Admin", "Dashboard");
            }
        }

        /// <summary>
        /// Displays search for fuel forms.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Search()
        {
            ViewData["users"] = _userRepository.getAllUsers(true);
            ViewData["saved"] = TempData["saved"];
            TempData["saved"] = false;

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            ViewData["PreviousFuelFormDetails"] = serializer.Serialize(new Fuel_Form());

            return View("~/Views/Admin/FuelForms/Search.aspx");
        }

        /// <summary>
        /// Displays search for fuel forms.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Search(FormCollection collection)
        {
            Guid userId = Guid.Empty;
            DateTime formDate = new DateTime();
            FuelFormViewModel editModel = new FuelFormViewModel();

            if (collection["userId"] != null)
                Guid.TryParse(collection["userId"], out userId);

            if (collection["formDate"] != null)
                formDate = DateTime.Parse(collection["formDate"]);

            if (userId != Guid.Empty)
            {
                Fuel_Form loadedFuelForm = _fuelFormRepository.getFuelForm(userId, formDate);
                List<Fuel_Form_Details> fuelFormDetails = new List<Fuel_Form_Details>();
                List<Fuel_Form_Equipment_Failure> fuelFormEquipFailure = new List<Fuel_Form_Equipment_Failure>();

                if (loadedFuelForm != null)
                {
                    fuelFormDetails = _fuelFormRepository.getFuelFormDetails(loadedFuelForm.Fuel_Form_ID);
                    fuelFormEquipFailure = _fuelFormRepository.getFuelFormEquipFailure(loadedFuelForm.Fuel_Form_ID);

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    IUser submittedBy = _userRepository.getUser((Guid)loadedFuelForm.UserId);

                    editModel = new FuelFormViewModel()
                    {
                        fuelForm = loadedFuelForm,
                        fuelFormDetails = fuelFormDetails,
                        submittedBy = submittedBy
                    };

                    ViewData["EquipFailure"] = fuelFormEquipFailure;
                    ViewData["AvailableProducts"] = _productRepository.getProductsForUser(userId);
                    ViewData["AvailableEquipment"] = _equipmentRepository.getEquipmentForUser(userId, false);
                    ViewData["Vendors"] = _vendorRepository.getAllVendors(false);
                    ViewData["UserProducts"] = _productRepository.getProductsForUser((Guid)loadedFuelForm.UserId);
                    ViewData["PreviousFuelFormDetails"] = serializer.Serialize(
                        _fuelFormRepository.getLastFuelForm(userId, 2).Fuel_Form_Details.Select(f => new
                        {
                            productId = f.Product_ID,
                            spareDrums = f.Spare_Drums,
                            tankDelivered = f.Tank_Delivered,
                            tankDispensed = f.Tank_Dispensed,
                            qtyOnHand = f.On_Hand
                        }).ToList()
                    );
                }
            }

            ViewData["users"] = _userRepository.getAllUsers(true);
            ViewData["userId"] = userId;
            ViewData["formDate"] = formDate;

            return View("~/Views/Admin/FuelForms/Search.aspx", editModel);
        }

        #endregion
    }
}