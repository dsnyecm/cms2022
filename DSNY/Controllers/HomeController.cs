﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Models;
using DSNY.ViewModels;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for index pages
    /// </summary>
    [HandleError]
    public class HomeController : Controller
    {
        #region Variables

        public IFormsAuthenticationService _formsService { get; set; }
        public IMembershipService _membershipService { get; set; }
        private ILogger _logger { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        public HomeController() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="IoCFormsService">The inversion of control forms service.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public HomeController(IFormsAuthenticationService IoCFormsService, IMembershipService IoCMembershipService, ILogger IoCLogger)
        {
            _formsService = IoCFormsService;
            _membershipService = IoCMembershipService ;
            _logger = IoCLogger;
        }

        #endregion

        #region Actions

        /// <summary>
        /// When hitting the home, redirect to appropriate homepage for user group
        /// If we have a user in multiple
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Roles.IsUserInRole("Administrator"))
                return RedirectToAction("Admin", "Dashboard");
            else if (Roles.IsUserInRole("Field") || Roles.IsUserInRole("Garage"))
                return RedirectToAction("Field", "Dashboard");
            else if (Roles.IsUserInRole("System Administrator"))
                return RedirectToAction("Index", "Admin");
            else
                return View();
        }

        /// <summary>
        /// If the index is posted to, try to log user on
        /// </summary>
        /// <param name="model">The log on model</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                // validate user
                if (_membershipService.ValidateUser(model.UserName, model.Password))
                {
                    // have form service create login cookie
                    _formsService.SignIn(model.UserName, false);

                    if (!String.IsNullOrEmpty(returnUrl))
                    {
                        // if we have a redirect, go that url
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        // otherwise, push back to the index to determine homepage
                        return RedirectToAction("Index");

                        //if (Roles.IsUserInRole(model.UserName, "Administrator"))
                        //    return RedirectToAction("Admin", "Dashboard");
                        //else if (Roles.IsUserInRole(model.UserName, "Field") || Roles.IsUserInRole(model.UserName, "Garage"))
                        //    return RedirectToAction("Field", "Dashboard");
                        //else if (Roles.IsUserInRole(model.UserName, "System Administrator"))
                        //    return RedirectToAction("Index", "Admin");
                        //else
                        //    return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError("LoginError", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("LoginError", "There was a problem loging you in failed.  Please contact a system administrator.");
            return View(model);
        }

        /// <summary>
        /// Logs user the off and redirects to the index.
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOff()
        {
            _formsService.SignOut();

            return RedirectToAction("Index", "Home");
        }

        #endregion
    }
}