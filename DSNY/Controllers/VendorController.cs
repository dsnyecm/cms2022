﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Common;
using DSNY.Core.Interfaces;
using DSNY.Core.Repository;
using DSNY.Data;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for vendor pages
    /// </summary>
    [HandleError]
    public class VendorController : Controller
    {
        #region Variables

        private IVendorRepository _vendorRepository;
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VendorController"/> class.
        /// </summary>
        public VendorController() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VendorController"/> class.
        /// </summary>
        /// <param name="IoCVendorRepository">The inversion of control vendor repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public VendorController(IVendorRepository IoCVendorRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _vendorRepository = IoCVendorRepository;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region List

        /// <summary>
        /// Lists vendors.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult List()
        {
            List<Vendor> model = _vendorRepository.getAllVendors(true);

            return View("~/Views/Admin/Vendor/List.aspx", model);
        }

        #endregion

        #region Create

        /// <summary>
        /// Creates form for vendor.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Create()
        {
            return View("~/Views/Admin/Vendor/Create.aspx");
        } 

        /// <summary>
        /// HTTP Post create action. Creates a vendor.
        /// </summary>
        /// <param name="model">The vendor model.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Create(Vendor model)
        {
            try
            {
                _vendorRepository.addVendor(model);

                return RedirectToAction("List");
            }
            catch
            {
                return RedirectToAction("List");
            }
        }

        #endregion

        #region Edit

        /// <summary>
        /// Edits a vendor.
        /// </summary>
        /// <param name="id">The vendor id.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Edit(int id)
        {
            Vendor model = _vendorRepository.getVendor(id);

            return View("~/Views/Admin/Vendor/Edit.aspx", model);
        }

        /// <summary>
        /// HTTP Post create action. Edits a vendor.
        /// </summary>
        /// <param name="id">The vendor id.</param>
        /// <param name="model">The vendor model.</param>
        /// <param name="collection">The form collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Edit(int id, Vendor model, FormCollection collection)
        {
            try
            {
                _vendorRepository.updateVendor(id, model);
 
                return RedirectToAction("List");
            }
            catch
            {
                return View("~/Views/Admin/Vendor/Edit.aspx", model);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// AJAX method to delete a vendor.
        /// </summary>
        /// <param name="id">The vendor id.</param>
        /// <param name="name">The vendor name.</param>
        /// <param name="collection">The form collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult Delete(int id, string name, FormCollection collection)
        {
            var returnValue = new { status = "failure", id = id, name = name };

            try
            {
                _vendorRepository.deleteVendor(id);
                returnValue = new { status = "success", id = id, name = name };
            }
            catch
            {
                // do nothing
            }

            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sort

        /// <summary>
        /// AJAX method to sort the vendor grid.
        /// </summary>
        /// <param name="field">The sort field.</param>
        /// <param name="direction">The sort direction.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult SortedGrid(string field, string direction)
        {
            return View("VendorGrid", _vendorRepository.getSortedVendors(field, direction == "ASC" ? 
                Enums.SortDirection.Ascending : Enums.SortDirection.Descending, true));
        }

        #endregion
    }
}
