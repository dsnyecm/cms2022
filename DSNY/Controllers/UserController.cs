﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.Mvc;

using DSNY.Common;
using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Common.Utilities;
using DSNY.Core.Interfaces;
using DSNY.Core.Models;
using DSNY.Core.Repository;
using DSNY.Models;
using DSNY.ViewModels;
using DSNY.Data;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for user pages
    /// </summary>
    [HandleError]
    public class UserController : Controller
    {
        #region Variables

        private IUserRepository _userRepository { get; set; }
        private IRoleRepository _roleRepository { get; set; }
        private IProductRepository _productRepository { get; set; }
        private IMessageRepository _messageRepository { get; set; }
        private IEquipmentRepository _equipmentRepository { get; set; }
        private ILogger _logger { get; set; }
        private IExceptionHandler _exceptionHandler { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserController"/> class.
        /// </summary>
        public UserController() : this(null, null, null, null, null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserController"/> class.
        /// </summary>
        /// <param name="IoCUserRepository">The inversion of control user repository.</param>
        /// <param name="IoCRoleRepository">The inversion of control role repository.</param>
        /// <param name="IoCProductRepository">The inversion of control product repository.</param>
        /// <param name="IoCEquipmentRepository">The inversion of control equipment repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public UserController(IUserRepository IoCUserRepository, IRoleRepository IoCRoleRepository, IProductRepository IoCProductRepository, IMessageRepository IoCMessageRepository,
            IEquipmentRepository IoCEquipmentRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _userRepository = IoCUserRepository;
            _roleRepository = IoCRoleRepository;
            _productRepository = IoCProductRepository;
            _messageRepository = IoCMessageRepository;
            _equipmentRepository = IoCEquipmentRepository;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region List

        /// <summary>
        /// Lists the users.  Pushes off to a specific page in View structure.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult List()
        {
            List<IUser> users = _userRepository.getAllUsers(false);

            // Creates Test Data for all Users
            //    foreach (var u in users)
            //    {
            //        Random rnd = new Random();

            //         u.poDisplayName = "District " + u.userName;
            //        u.address = rnd.Next(1, 1000).ToString() +  " Long Name Main St";

            //        if (u.userName.Contains("QN"))
            //        {
            //            u.borough = "Queens";
            //        }
            //        else if (u.userName.Contains("BK"))
            //        {
            //            u.borough = "Brooklyn";
            //        }
            //        else if (u.userName.Contains("BX"))
            //        {
            //            u.borough = "Bronx";
            //        }
            //        else if (u.userName.Contains("MN"))
            //        {
            //            u.borough = "Manhattan";
            //        }
            //        else if (u.userName.Contains("SI"))
            //        {
            //            u.borough = "Staten Island";
            //        }

            //        _userRepository.updateUser(u);
            //}

            return View("~/Views/Admin/User/List.aspx", users);
        }

    #endregion

    #region Create

    /// <summary>
    /// Create form for user.
    /// </summary>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    public ActionResult Create()
    {
        // Set our view data variable to hold all the possible roles
        ViewData["Roles"] = _roleRepository.getAllRoles();
        ViewData["Products"] = _productRepository.getAllProducts(false);
        ViewData["Equipment"] = _equipmentRepository.getAllEquipment(false);
        ViewData["Users"] = _userRepository.getAllUsers(false);
        ViewData["SelectedUsers"] = new SelectList(new List<IUser>(), "userId", "fullName");

        return View("~/Views/Admin/User/Create.aspx");
    }

    /// <summary>
    /// HTTP Post create action.  Creates a user.
    /// </summary>
    /// <param name="model">The user model.</param>
    /// <param name="collection">The form collection.</param>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    [HttpPost]
    public ActionResult Create(NewUserModel model, FormCollection collection)
    {
        // Dummy value that we'll never use but can use to check against
        MembershipCreateStatus createStatus = MembershipCreateStatus.InvalidAnswer;

        if (string.IsNullOrEmpty(model.email))
        {
            model.email = string.Empty;
        }

        // Do a double check on passwords matching, since the MVC validation doesn't work in some cases
        if (model.password == model.confirmPassword)
        {
            // Initalize the roles
            model.role = new List<IRole>();

            // Fill the model's role from the form's collection object
            if (collection["role"] != null)
            {
                string[] selectedRoles = collection["role"].Split(',');

                foreach (string role in selectedRoles)
                {
                    model.role.Add(new Role() { roleName = role });
                }
            }

            createStatus = _userRepository.addUser(model.userName, model.password, model.description, model.address, model.borough, model.poDisplayName, model.email, model.role, model.isDistribution, model.isEmail, model.isActive);

            if (createStatus == MembershipCreateStatus.Success)
            {
                // Get our new user
                IUser newUser = _userRepository.getUser(model.userName);

                // declare the user product variables
                int prodId = -1, equipId = -1, prodQty = -1, tempProdId = -1, equipCapacity = -1;
                Guid userId = Guid.Empty;

                // declare the user equipment variables
                int equipCount = 0;
                bool activeProduct = false;
                List<string> equipDescripts = new List<string>(), equipCapacities = new List<string>(), equipActive = new List<string>();
                Dictionary<int, int> productEquipCapacity = new Dictionary<int, int>();

                // Take our return collection object holding the GUID of the users, split it and loop over to add
                if (collection["usersSelected"] != null)
                {
                    string[] userGuids = collection["usersSelected"].Split(',');

                    foreach (string item in userGuids)
                    {
                        Guid.TryParse(item, out userId);

                        if (userId != Guid.Empty)
                            _userRepository.addUserToCommandChain(newUser.userId, userId);

                        userId = Guid.Empty;
                    }
                }

                foreach (string key in collection.Keys)
                {
                    if (key.Contains("product"))
                    {
                        // get product ID from the key
                        int.TryParse(key.Replace("product", string.Empty), out prodId);

                        // if we have a good product ID, get quantity and then updated product
                        if (prodId > -1)
                        {
                            int.TryParse(collection["prodQty" + prodId.ToString()], out prodQty);

                            if (prodQty > 0)
                                _productRepository.addUserToProduct(prodId, newUser.userId, prodQty);
                            else
                                _productRepository.addUserToProduct(prodId, newUser.userId, -1);
                        }

                        prodId = -1;
                    }
                    else if (key.Contains("equipDescript"))
                    {
                        // grab equip ID from key
                        int.TryParse(key.Replace("equipDescript", string.Empty), out equipId);

                        // if we have a good equip ID and a related product exists for this equip
                        if (equipId > 0 && collection["equipProd" + equipId] != null)
                        {
                            // get descriptions, capacities, active for one equip type
                            equipDescripts.AddRange(collection["equipDescript" + equipId].Split(','));
                            equipCapacities.AddRange(collection["equipCapacity" + equipId].Split(','));
                            equipActive.AddRange(collection["equipActive" + equipId].Split(','));

                            // for each product related to this equipment
                            foreach (string productId in collection["equipProd" + equipId].Split(','))
                            {
                                // get product ID and capacity
                                int.TryParse(productId, out tempProdId);
                                int.TryParse(equipCapacities[equipCount], out equipCapacity);

                                // if active
                                if (equipActive[equipCount] != null)
                                    bool.TryParse(equipActive[equipCount], out activeProduct);

                                // if we have a prod ID and a description, create user - product - equip record
                                if (tempProdId > -1 && !string.IsNullOrEmpty(equipDescripts[equipCount].ToString()))
                                {
                                    if (equipCapacity > 0)
                                    {
                                        if (productEquipCapacity.Keys.Contains(tempProdId))
                                            productEquipCapacity[tempProdId] = productEquipCapacity[tempProdId] + equipCapacity;
                                        else
                                            productEquipCapacity.Add(tempProdId, equipCapacity);
                                    }

                                    _equipmentRepository.addUserToEquipment(equipId, tempProdId, equipDescripts[equipCount], equipCapacity, activeProduct, newUser.userId);
                                }

                                // reset the local variables
                                tempProdId = -1;
                                equipCapacity = -1;
                                equipCount++;
                                activeProduct = false;
                            }

                            // clear the collections
                            equipDescripts.Clear();
                            equipCapacities.Clear();
                            equipActive.Clear();
                        }

                        // clear equip id and equip count
                        equipId = 0;
                        equipCount = 0;
                    }
                }


                // loop through capacities for equipment products and then update the product - user capacities
                foreach (int key in productEquipCapacity.Keys)
                {
                    _productRepository.updateProductUserCapacity(key, newUser.userId, productEquipCapacity[key]);
                }

                // if user is on the distribution list associate user with all past messages
                if (model.isDistribution && newUser != null)
                    _messageRepository.updateNewUserWithCurrentMessages(newUser.userId);

                return RedirectToAction("List");
            }
        }

        if (createStatus != MembershipCreateStatus.InvalidAnswer)
            ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));

        // Set our view data variables to hold all needed information
        ViewData["Roles"] = _roleRepository.getAllRoles();
        ViewData["Products"] = _productRepository.getAllProducts(false);
        ViewData["Equipment"] = _equipmentRepository.getAllEquipment(false);
        ViewData["Users"] = _userRepository.getAllUsers(false);
        ViewData["SelectedUsers"] = new SelectList(new List<IUser>(), "userId", "fullName");

        return View("~/Views/Admin/User/Create.aspx", model);
    }

    #endregion

    #region Edit

    /// <summary>
    /// Edits a user.
    /// </summary>
    /// <param name="name">The user name.</param>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    public ActionResult Edit(string name)
    {
        // Get our user and fill ViewModel
        EditUserModel editUser = new EditUserModel();
        IUser userToEdit = _userRepository.getUser(name);

        if (userToEdit != null)
        {
            editUser.userName = userToEdit.userName;
            editUser.description = userToEdit.description;
            editUser.email = userToEdit.email;
            editUser.address = userToEdit.address;
            editUser.borough = userToEdit.borough;
            editUser.poDisplayName = userToEdit.poDisplayName;
            editUser.role = userToEdit.roles.Count > 0 ? userToEdit.roles : new List<IRole>();
            editUser.isDistribution = userToEdit.isDistribution;
            editUser.isEmail = userToEdit.isEmail;
            editUser.isActive = userToEdit.isActive;
            editUser.isLocked = userToEdit.isLocked;
            editUser.userEquipment = _equipmentRepository.getEquipmentForUser(userToEdit.userId, true);
            editUser.userProducts = _productRepository.getProductsForUser(userToEdit.userId);
            editUser.userCommandChain = _userRepository.getUserCommandChain(userToEdit.userId);

            ViewData["EquipmentUser"] = editUser.userEquipment;
        }

        // Set our view data variable to hold all the possible roles
        ViewData["Roles"] = _roleRepository.getAllRoles();
        ViewData["Products"] = _productRepository.getAllProducts(false);
        ViewData["Users"] = _userRepository.getAllUsers(false);
        ViewData["Equipment"] = _equipmentRepository.getAllEquipment(false);
        ViewData["SelectedUsers"] = new SelectList(editUser.userCommandChain, "userId", "fullName");

        return View("~/Views/Admin/User/Edit.aspx", editUser);
    }

    /// <summary>
    /// HTTP Post edit action. Edit a user.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <param name="collection">The collection.</param>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    [HttpPost]
    public ActionResult Edit(EditUserModel model, FormCollection collection)
    {
        IUser updateUser = null;

        try
        {
            // Do validation checks for user information
            if (!string.IsNullOrEmpty(model.password) && model.password != model.confirmPassword)
            {
                ModelState.AddModelError("Password", "The password and confirmation password do not match.");
            }
            else if (!string.IsNullOrEmpty(model.password) && model.password.Length < Membership.Provider.MinRequiredPasswordLength)
            {
                ModelState.AddModelError("Password", "The password must be at least " + Membership.Provider.MinRequiredPasswordLength.ToString() + " characters.");
            }
            else if (!string.IsNullOrEmpty(model.email) && !EmailCheck.IsEmail(model.email))
            {
                ModelState.AddModelError("email", "Email address is invalid.");
            }
            else
            {
                // Make our EditUserModel into a IUser object to pass to our user repository
                updateUser = new User();
                updateUser.userName = model.userName;
                updateUser.description = model.description;
                updateUser.address = model.address;
                updateUser.borough = model.borough;
                updateUser.poDisplayName = model.poDisplayName;
                updateUser.email = model.email;
                updateUser.roles = new List<IRole>();
                updateUser.isDistribution = model.isDistribution;
                updateUser.isEmail = model.isEmail;
                updateUser.isActive = model.isActive;
                updateUser.isLocked = model.isLocked;

                // fill in the roles
                if (collection["role"] != null)
                {
                    string[] selectedRoles = collection["role"].Split(',');

                    foreach (string role in selectedRoles)
                    {
                        updateUser.roles.Add(new Role() { roleName = role });
                    }
                }

                // update
                _userRepository.updateUser(updateUser);

                // Change password if we need to
                if (!string.IsNullOrEmpty(model.password) && !string.IsNullOrEmpty(model.confirmPassword) &&
                        model.password == model.confirmPassword)
                {
                    _userRepository.updatePassword(updateUser, model.password);
                }

                // Re-get our user with all their updated info
                updateUser = _userRepository.getUser(model.userName);

                // Update the message/email distribution list for our user
                _userRepository.updateMessageDistForUser(updateUser.userId, updateUser.isDistribution);
                _userRepository.updateEmailDistForUser(updateUser.userId, updateUser.isEmail);

                List<Equipment_User> currentEquip = _equipmentRepository.getEquipmentForUser(updateUser.userId, true);

                // Delete all user / product relations and command chain relations, we'll add them all back
                _productRepository.deleteAllProductsForUser(updateUser.userId);
                _userRepository.deleteUserFromCommandChain(updateUser.userId);

                // Take our return collection object holding the GUID of the users, split it and loop over to add
                // repopulates command chain relationships
                if (collection["usersSelected"] != null)
                {
                    string[] userGuids = collection["usersSelected"].Split(',');
                    Guid tempGuid = Guid.Empty;

                    foreach (string item in userGuids)
                    {
                        Guid.TryParse(item, out tempGuid);

                        if (tempGuid != Guid.Empty)
                            _userRepository.addUserToCommandChain(updateUser.userId, tempGuid);

                        tempGuid = Guid.Empty;
                    }
                }

                // Fill the users product / equipment relations from form data
                int prodId = -1, equipUserId = -1, prodQty = -1, loopCounter = 0;
                int tempProdId = -1, equipTypeId = -1, equipCapacity = -1;

                bool activeProduct;
                List<int> updatedEquipUsers = new List<int>();
                Dictionary<int, int> productEquipCapacity = new Dictionary<int, int>();
                Dictionary<string, string> equipUserIds = collection.Cast<string>().Where(k => k.StartsWith("equipUserId")).ToDictionary(k => k, k => collection[k]);
                Dictionary<string, string> equipTypes = collection.Cast<string>().Where(k => k.StartsWith("equipType")).ToDictionary(k => k, k => collection[k]);
                Dictionary<string, string> equipDescripts = collection.Cast<string>().Where(k => k.StartsWith("equipDescript")).ToDictionary(k => k, k => collection[k]);
                Dictionary<string, string> equipProds = collection.Cast<string>().Where(k => k.StartsWith("equipProd")).ToDictionary(k => k, k => collection[k]);
                Dictionary<string, string> equipCapacities = collection.Cast<string>().Where(k => k.StartsWith("equipCapacity")).ToDictionary(k => k, k => collection[k]);
                Dictionary<string, string> equipActives = collection.Cast<string>().Where(k => k.StartsWith("equipActiveHidden")).ToDictionary(k => k, k => collection[k]);

                foreach (var item in equipDescripts)
                {
                    if (loopCounter + 1 <= equipDescripts.Count)
                    {
                        int.TryParse(equipUserIds.ElementAt(loopCounter).Value, out equipUserId);
                        int.TryParse(equipProds.ElementAt(loopCounter).Value, out tempProdId);
                        int.TryParse(equipTypes.ElementAt(loopCounter).Value, out equipTypeId);
                        int.TryParse(equipCapacities.ElementAt(loopCounter).Value, out equipCapacity);
                        bool.TryParse(equipActives.ElementAt(loopCounter).Value, out activeProduct);

                        if (tempProdId > 0 && !string.IsNullOrEmpty(equipDescripts.ElementAt(loopCounter).Value))
                        {
                            if (equipCapacity > 0)
                            {
                                if (productEquipCapacity.Keys.Contains(tempProdId))
                                    productEquipCapacity[tempProdId] = productEquipCapacity[tempProdId] + equipCapacity;
                                else
                                    productEquipCapacity.Add(tempProdId, equipCapacity);
                            }

                            if (equipUserId > 0)
                            {
                                updatedEquipUsers.Add(equipUserId);
                                _equipmentRepository.updateEquipmentUser(equipUserId, equipTypeId, tempProdId, equipDescripts.ElementAt(loopCounter).Value, equipCapacity, activeProduct);
                            }
                            else
                                _equipmentRepository.addUserToEquipment(equipTypeId, tempProdId, equipDescripts.ElementAt(loopCounter).Value, equipCapacity, activeProduct, updateUser.userId);
                        }

                        tempProdId = -1;
                        equipUserId = -1;
                        equipTypeId = -1;
                        equipCapacity = -1;
                        activeProduct = false;
                    }

                    loopCounter++;
                }



                // loop through all form keys
                foreach (string key in collection.Keys)
                {
                    // look for the product key in the form
                    if (key.Contains("product"))
                    {
                        // parse our the product Id
                        int.TryParse(key.Replace("product", string.Empty), out prodId);

                        // if we got something, get rest of info for those specific product Id key fields
                        if (prodId > -1)
                        {
                            int.TryParse(collection["prodQty" + prodId.ToString()], out prodQty);

                            if (prodQty > 0)
                                _productRepository.addUserToProduct(prodId, updateUser.userId, prodQty);
                            else
                                _productRepository.addUserToProduct(prodId, updateUser.userId, 0);
                        }

                        prodId = -1;
                    }
                }

                // loop through equipment user updated, look at previous equiupment user relationships and then delete any not present
                foreach (Equipment_User equipUser in currentEquip.Where(e => !updatedEquipUsers.Contains(e.Equipment_User_ID)).ToList())
                {
                    _equipmentRepository.deleteEquipmentUser(equipUser.Equipment_User_ID);
                }

                // loop through capacities for equipment products and then update the product - user capacities
                foreach (int key in productEquipCapacity.Keys)
                {
                    _productRepository.updateProductUserCapacity(key, updateUser.userId, productEquipCapacity[key]);
                }

                return RedirectToAction("List");
            }
        }
        catch (Exception ex)
        {
            _exceptionHandler.HandleException(ex);
            ModelState.AddModelError("", "Error: " + ex.Message);
        }

        // gets updated command chain for model
        model.userCommandChain = _userRepository.getUserCommandChain(_userRepository.getUser(model.userName).userId);

        // fill the view data with extra info
        ViewData["Roles"] = _roleRepository.getAllRoles();
        ViewData["Products"] = _productRepository.getAllProducts(false);
        ViewData["Equipment"] = _equipmentRepository.getAllEquipment(false);
        ViewData["Users"] = _userRepository.getAllUsers(false);
        ViewData["SelectedUsers"] = new SelectList(model.userCommandChain, "userId", "fullName");

        // if we have equipment for user, get them, otherwise create empty list
        if (updateUser != null)
            ViewData["EquipmentUser"] = _equipmentRepository.getEquipmentForUser(updateUser.userId, true);
        else
            ViewData["EquipmentUser"] = new List<Equipment_User>();

        return View("~/Views/Admin/User/Edit.aspx", model);
    }

    #endregion

    #region Delete

    /// <summary>
    /// AJAX method to delete a user.
    /// </summary>
    /// <param name="name">The user name.</param>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    [HttpGet]
    public ActionResult Delete(string name)
    {
        var returnValue = new { status = "failed", name = name };

        if (_userRepository.deleteUser(name))
            returnValue = new { status = "success", name = name };

        return Json(returnValue, JsonRequestBehavior.AllowGet);
    }

    #endregion

    #region Sort

    /// <summary>
    /// AJAX method to sort the user list.
    /// </summary>
    /// <param name="field">The field.</param>
    /// <param name="direction">The direction.</param>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    [HttpGet]
    public ActionResult SortedGrid(string field, string direction)
    {
        return View("UserGrid", _userRepository.getSortedUsers(field, direction == "ASC" ? Enums.SortDirection.Ascending :
            Enums.SortDirection.Descending));
    }

    #endregion

    #region Message Distribution

    /// <summary>
    /// Preapres lists of users available and users in the message distribution
    /// </summary>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    public ActionResult MessageDist()
    {
        List<IUser> msgDistUsers = _userRepository.getAllMessageDistribUsers();
        List<IUser> allUsers = _userRepository.getAllUsers(false);
        List<IUser> nonMsgDistUsers = new List<IUser>();

        foreach (IUser user in allUsers)
        {
            if (!msgDistUsers.Exists(eu => eu.userId == user.userId))
                nonMsgDistUsers.Add(user);
        }

        ViewData["usersAvailable"] = new SelectList(nonMsgDistUsers, "userId", "fullName");
        ViewData["usersSelected"] = new SelectList(msgDistUsers, "userId", "fullName");

        return View("~/Views/Admin/User/MessageDist.aspx");
    }

    /// <summary>
    /// HTTP Post edit action.  Sets up who recieves system messages.
    /// </summary>
    /// <param name="usersAvailable">The users available.</param>
    /// <param name="usersSelected">The users selected.</param>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    [HttpPost]
    public ActionResult MessageDist(Guid[] usersAvailable, Guid[] usersSelected)
    {
        // Take our return collection object holding the GUID of the users, split it and loop over to add
        if (usersAvailable != null)
        {
            foreach (Guid item in usersAvailable)
            {
                _userRepository.updateMessageDistForUser(item, false);
            }
        }

        if (usersSelected != null)
        {
            foreach (Guid item in usersSelected)
            {
                _userRepository.updateMessageDistForUser(item, true);
            }
        }

        return RedirectToAction("Index", "Admin");
    }

    #endregion

    #region Email Distribution

    /// <summary>
    /// Preapres lists of users available and users in the email distribution
    /// </summary>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    public ActionResult EmailDist()
    {
        List<IUser> emailDistUsers = _userRepository.getAllEmailDsitribUsers();
        List<IUser> usersWithEmailAddresses = _userRepository.getAllEmailAddressUsers();
        List<IUser> nonEmailDistUsers = new List<IUser>();

        foreach (IUser user in usersWithEmailAddresses)
        {
            if (!emailDistUsers.Exists(eu => eu.userId == user.userId))
                nonEmailDistUsers.Add(user);
        }

        ViewData["usersAvailable"] = new SelectList(nonEmailDistUsers, "userId", "fullName");
        ViewData["usersSelected"] = new SelectList(emailDistUsers, "userId", "fullName");

        return View("~/Views/Admin/User/EmailDist.aspx");
    }

    /// <summary>
    /// HTTP Post edit action.  Sets up who recieves e-mailed system messages.
    /// </summary>
    /// <param name="usersSelected">The users selected.</param>
    /// <param name="usersAvailable">The users available.</param>
    /// <returns></returns>
    [Authorize(Roles = "System Administrator")]
    [HttpPost]
    public ActionResult EmailDist(Guid[] usersSelected, Guid[] usersAvailable)
    {
        // Take our return collection object holding the GUID of the users, split it and loop over to add
        if (usersAvailable != null)
        {
            foreach (Guid item in usersAvailable)
            {
                _userRepository.updateEmailDistForUser(item, false);
            }
        }

        if (usersSelected != null)
        {
            foreach (Guid item in usersSelected)
            {
                _userRepository.updateEmailDistForUser(item, true);
            }
        }

        return RedirectToAction("Index", "Admin");
    }

    #endregion
}
}