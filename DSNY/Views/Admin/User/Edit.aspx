﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.EditUserModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit <%: Model.userName %>
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftAjax.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftMvcValidation.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Edit <%: Model.userName %>'s Information</h2>
    <p style="width: 785px;" class="dottedLine"></p>

    <div class="iconContainer">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "User", action = "List"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />Cancel
            </a>
        </div>
        <div class="iconLeft">
            <a href="#" id="submitForm" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/create.png" alt="Save" runat="server" /><br />Save User
            </a>
        </div>
    </div>

    <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <ul id="tabnav">
            <li class="selectedTab"><a href="#">User Information</a></li>
            <li><a href="#">Command Chain</a></li>
            <li><a href="#">Products</a></li>
            <li><a href="#">Equipment</a></li>
        </ul>

        <div id="tabsContainer">

            <div class="tabContent">
               <%: Html.EditorFor(m => m) %>
            </div>

            <div class="tabContent">
                <div style="padding-left: 25px;">
                <% if (ViewData["Users"] != null && ((List<DSNY.Core.Interfaces.IUser>)ViewData["Users"]).Count > 0) { %>
                    <div style="float: left;">
                        <h3>Available Users</h3>
                        <select id="usersAvailable" multiple="multiple" class="userSelectList">
                        <% List<DSNY.Core.Interfaces.IUser> tempUsers = (List<DSNY.Core.Interfaces.IUser>)ViewData["Users"];
                           List<DSNY.Core.Interfaces.IUser> users = tempUsers.OrderBy(u => u.description).ThenBy(u => u.userName).ToList();
                           foreach (DSNY.Core.Interfaces.IUser user in users) {
                                if (!Model.userCommandChain.Exists(u => u.userId == user.userId)) {
                                    if (!string.IsNullOrEmpty(user.description)) { %>
                                        <option value="<%: user.userId %>"><%: user.description%> (<%: user.userName%>)</option>
                                <% } else { %>
                                        <option value="<%: user.userId %>"><%: user.userName%></option>
                                <% }
                                }  %>
                       <% } %>
                        </select>
                    </div>
                    <div style="width: 75px; height: 175px; padding-top: 100px; padding-left: 25px; float: left;">
                        <a href="#" style="margin-bottom: 50px;">
                            <img id="addUser" border="0" src="~/Content/Images/Icons/moveRight.png" alt="Add User" runat="server" />
                        </a>
                        <div style="height: 50px;"></div>
                        <a href="#">
                            <img id="removeUser" border="0" src="~/Content/Images/Icons/moveLeft.png" alt="Add User" runat="server" />
                        </a>
                    </div>
                    <div style="float: left;">
                        <h3>Users who report to <%: Model.userName %></h3>
                        <%: Html.DropDownList("usersSelected", (SelectList)ViewData["SelectedUsers"], new { @class = "userSelectList", @multiple = "multiple" })%>
                    </div>
                <% } %>
                </div>
            </div>

            <div class="clear"></div>

            <% List<DSNY.Data.Product> products = (List<DSNY.Data.Product>)ViewData["Products"]; %>

            <div class="tabContent">
                <div >
                <% if (ViewData["Products"] != null && ((List<DSNY.Data.Product>)ViewData["Products"]).Count > 0) { %>
                    <table style="float: left; margin-right: 10px;">
                        <tr>
                            <th style="width: 175px;">Product</th>
                            <th style="width: 50px;">Capacity</th>
                        </tr>
                <%  int prodCount = 0;

                    foreach (DSNY.Data.Product prod in products)
                    {
                        if (prodCount > 0 && prodCount % 10 == 0) { %>
                        </table>
                        <table style="float: left; margin-right: 10px;">
                            <tr>
                                <th style="width: 175px;">Product</th>
                                <th style="width: 50px;">Capacity</th>
                            </tr>
                    <% }

                        if (prodCount % 2 == 0) {%>
                            <tr>
                        <% } else { %>
                            <tr class="alt">
                        <% } %>
                            <%  // If we have a postback, use that value, otherwise check if the model has a value, otherwise blank
                                if (Request["product" + prod.Product_ID] != null) { %>
                                <td>
                                    <input type="checkbox" checked="checked" id="product<%: prod.Product_ID %>" name="product<%: prod.Product_ID %>" /><%: prod.Product_Name%>
                                </td>
                                <td>
                                    <input type="text" id="prodQty<%: prod.Product_ID %>" name="prodQty<%: prod.Product_ID %>" style="width: 40px;"
                                    value="<%: Request["prodQty" + prod.Product_ID] != null ? Request["prodQty" + prod.Product_ID] : string.Empty  %>" />
                                </td>
                            <% } else if (Model.userProducts != null && Model.userProducts.Exists(up => up.Product_ID == prod.Product_ID)) { %>
                                <td>
                                    <input type="checkbox" checked="checked" id="product<%: prod.Product_ID %>" name="product<%: prod.Product_ID %>" /><%: prod.Product_Name%>
                                </td>
                                <td>
                                    <input type="text" id="prodQty<%: prod.Product_ID %>" name="prodQty<%: prod.Product_ID %>" style="width: 40px;"
                                    value="<%: Model.userProducts.SingleOrDefault(pu => pu.Product_ID == prod.Product_ID).Capacity %>" />
                                </td>
                            <% } else { %>
                                <td>
                                    <input type="checkbox" id="product<%: prod.Product_ID %>" name="product<%: prod.Product_ID %>" />&nbsp;<%: prod.Product_Name%>
                                </td>
                                <td>
                                    <input type="text" id="prodQty<%: prod.Product_ID %>" name="prodQty<%: prod.Product_ID %>" style="width: 40px;" />
                                </td>
                            <% } %>
                        </tr>
                    <% prodCount++;
                    } %>
                    </table>
                <% } else { %>
                    <h4>There are no products.</h4>
                <% } %>
                </div>
            </div>

            <div class="clear"></div>

            <div class="tabContent">
                <% if (ViewData["Equipment"] != null && ((List<DSNY.Data.Equipment>)ViewData["Equipment"]).Count > 0)
                   { %>
                    <table id="equipmentGrid" style="margin-right: 10px;">
                        <tr>
                            <th style="width: 100px;">Equipment</th>
                            <th style="width: 300px;">Description</th>
                            <th style="width: 175px;">Related Product</th>
                            <th style="width: 175px;">Capacity</th>
                            <th style="width: 40px;">Active</th>
                        </tr>
                    <% List<DSNY.Data.Equipment> equipment = (List<DSNY.Data.Equipment>)ViewData["Equipment"];
                       DSNY.Data.Equipment selectedEquipment = null;
                       List<DSNY.Data.Equipment_User> equipmentUser = (List<DSNY.Data.Equipment_User>)ViewData["EquipmentUser"];
                       int equipCount = 1; %>

                        <tr id="firstInputRow" style="display: none;" >
                            <td>
                                <input type="hidden" name="equipUserId" value="-1" />
                                <select name="equipType" class="equipType">
                                    <option value="-1">SELECT EQUIPMENT</option>
                                    <% foreach (DSNY.Data.Equipment equip in equipment)
                                        { %>
                                            <option value="<%: equip.Equipment_ID %>" class="activeCheckbox"><%: equip.Equipment_Name%></option>
                                    <% } %>
                                </select>
                            </td>
                            <td><input type="text" name="equipDescript" value="" style="width: 200px" /></td>
                            <td>
                                <select name="equipProd">
                                    <option value="-1">SELECT PRODUCT</option>
                                    <% foreach (DSNY.Data.Product prod in products)
                                        { %>
                                            <option value="<%: prod.Product_ID %>" class="activeCheckbox"><%: prod.Product_Name%></option>
                                    <% } %>
                                </select>
                            </td>
                            <td style="text-align: center;">
                                <input type="text" name="equipCapacity" value="" style="width: 50px; display: none;" />
                            </td>
                            <td style="text-align: center;">
                                <input type="checkbox" name="equipActive" class="activeCheckbox" checked="true" />
                                <input type="hidden" name="equipActiveHidden" value="true" />
                            </td>
                            <td style="text-align: center;">
                                <a href="#" class="removeRow" onclick="return false;">
                                    <img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove" runat="server" />
                                </a>
                            </td>
                        </tr>

                       <% if (equipmentUser.Count > 0)
                          {
                            foreach (DSNY.Data.Equipment_User equipUser in equipmentUser)
                            {
                                selectedEquipment = null;
                        %>
                            <tr class="<%: equipCount % 2 == 0 ? "alt" : string.Empty%>">
                                <td>
                                    <input type="hidden" name="equipUserId.<%: equipCount %>" value="<%: equipUser.Equipment_User_ID %>" />
                                    <select name="equipType.<%: equipCount %>" class="equipType">
                                        <option value="-1">SELECT EQUIPMENT</option>
                                        <% foreach (DSNY.Data.Equipment equip in equipment)
                                           {
                                               if (equip.Equipment_ID == equipUser.Equipment_ID)
                                               {
                                                   selectedEquipment = equip;%>
                                                    <option value="<%: equip.Equipment_ID %>" selected="selected"><%: equip.Equipment_Name%></option>
                                            <% }
                                               else
                                               { %>
                                                    <option value="<%: equip.Equipment_ID %>" class="activeCheckbox"><%: equip.Equipment_Name%></option>
                                        <%     } %>
                                        <% } %>
                                    </select>
                                </td>
                                <td><input type="text" name="equipDescript.<%: equipCount %>" value="<%: equipUser.Equipment_Description %>" style="width: 200px" /></td>
                                <td>
                                    <select name="equipProd.<%: equipCount %>">
                                        <option value="-1">SELECT PRODUCT</option>
                                        <% foreach (DSNY.Data.Product prod in products)
                                           {
                                               if (prod.Product_ID == equipUser.Product_ID)
                                               {%>
                                                    <option value="<%: prod.Product_ID %>" selected="selected"><%: prod.Product_Name%></option>
                                            <% }
                                               else
                                               { %>
                                                    <option value="<%: prod.Product_ID %>" class="activeCheckbox"><%: prod.Product_Name%></option>
                                        <%     } %>
                                        <% } %>
                                    </select>
                                </td>
                                <td style="text-align: center;">
                                    <input type="text" name="equipCapacity.<%: equipCount %>" value="<%: equipUser.Capacity %>" 
                                        style="width: 50px; <%: !selectedEquipment.has_Capacity ? "display: none;" : "" %>" />
                                </td>
                                <td style="text-align: center;">
                                    <input type="checkbox" name="equipActive.<%: equipCount %>" class="activeCheckbox" <%: equipUser.is_Active ? "checked" : string.Empty %> />
                                    <input type="hidden" name="equipActiveHidden.<%: equipCount %>" value="<%: equipUser.is_Active ? "true" : "false" %>" />
                                </td>
                                <td style="text-align: center;">
                                    <a href="#" class="removeRow" onclick="return false;">
                                        <img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove" runat="server" />
                                    </a>
                                </td>
                            </tr>
                    <%      equipCount++;
                          }
                       } %>
                    </table>
                    <a href="#" id="addEquipRow" style="float: right; margin: 10px 10px 0px 0px;">Add Equipment Row</a>
                    <div class="clear"></div>
                <% } %>
            </div>
        </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            // Kick off the tabs function
            $("#tabsContainer").tabs({ tab: 1 });

            // inital sort
            SortListbox("usersAvailable");
            SortListbox("usersSelected");

            // takes all selected options in 'usersAvailable' and moves to 'usersSelected'
            $("#addUser").click(function () {
                $('#usersAvailable option:selected').appendTo('#usersSelected');
                $('#usersAvailable option:selected').remove();

                SortListbox("usersAvailable");
                SortListbox("usersSelected");
            });

            // takes all selected options in 'usersSelected' and moves to 'usersAvailable'
            $("#removeUser").click(function () {
                $('#usersSelected option:selected').appendTo('#usersAvailable');
                $('#usersSelected option:selected').remove();

                SortListbox("usersAvailable");
                SortListbox("usersSelected");
            });

            // Add new row to table
            $('#addEquipRow').live('click', function () {
                var table = $('#equipmentGrid');
                var rowIndex = table.attr('rows').length - 1;
                var row = table.find("tr:eq(" + rowIndex + ")");
                var copiedRow = null;

                if (rowIndex > 1)
                    copiedRow = row.clone();
                else {
                    copiedRow = $("#firstInputRow").clone();
                    copiedRow.attr('id', '');
                    copiedRow.css('display', 'table-row');
                }

                if (copiedRow != null) {
                    var randomRowNum = Math.floor(Math.random()*1000001);
                    copiedRow.find(":input:eq(0)").attr("name", "equipUserId." + randomRowNum);
                    copiedRow.find(":input:eq(0)").val("-1");
                    copiedRow.find(":input:eq(1)").attr("name", "equipType." + randomRowNum);
                    copiedRow.find(":input:eq(1)").val("-1");
                    copiedRow.find(":input:eq(2)").attr("name", "equipDescript." + randomRowNum);
                    copiedRow.find(":input:eq(2)").val("");
                    copiedRow.find(":input:eq(3)").attr("name", "equipProd." + randomRowNum);
                    copiedRow.find(":input:eq(3)").val("-1");
                    copiedRow.find(":input:eq(4)").attr("name", "equipCapacity." + randomRowNum);
                    copiedRow.find(":input:eq(4)").val("").css('display', 'none');
                    copiedRow.find(":input:eq(5)").attr("name", "equipActive." + randomRowNum);
                    copiedRow.find(":input:eq(6)").attr("name", "equipActiveHidden." + randomRowNum);

                    copiedRow.insertAfter(table.find("tr:eq(" + rowIndex + ")"));
                }
            });

            // Remove a row
            $('.removeRow').live('click', function () {
                var rowIndex = $(this).closest("tr").prevAll("tr").length;
                var row = $(this).parents("table").find("tr:eq(" + rowIndex + ")");
                row.remove();
            });

            // shows or hides capacity for equipment
            $('.equipType').live('change', function () {
                // get value and then also get the targeted capacity input to change css display
                var value = $(this).val();
                var row = $(this).parents('tr');
                var inputToChange = $(this).parents('tr').find('td:eq(3)').find(':input');

                // loop through the equipment, if we get a match, flip display based on if there is a capacity
                <% foreach (DSNY.Data.Equipment equip in (List<DSNY.Data.Equipment>)ViewData["Equipment"]) 
                    { %>
                    if (value == <%: equip.Equipment_ID %>) {
                        $(inputToChange).css('display', '<%: equip.has_Capacity ? "inline" : "none" %>');
                        
                        if (!<%: equip.has_Capacity.ToString().ToLower() %>)
                            $(inputToChange).val('');

                        return;
                    }
                <% } %>
            });

            // before submit, we select all options in the select list, this allows us to grab all the values
            $('#submitForm').click(function () {
                $('#usersSelected option').attr('selected', 'selected');
                document.forms[0].submit();
            });

            $(".activeCheckbox").live('click', function () {
                var nextInput = $(":input:eq(" + ($(":input").index(this) + 1) + ")");

                if ($(this).is(':checked'))
                    nextInput.val("true");
                else
                    nextInput.val("false");
            });
        });
    </script>
</asp:Content>