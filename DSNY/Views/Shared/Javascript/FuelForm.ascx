﻿    $(document).ready(function () {
        var imgPath = getUrlPath();
        $("#tabsContainer").tabs({ tab: 1 });

        preload(["<%=ResolveUrl("~/Content/Images/Icons/delete.png")%>"]);

        $('.addRow').live('click', function () {
            var rowIndex = $(this).closest("tr").prevAll("tr").length + 1;
            var row = $(this).parents("table").find("tr:eq(" + rowIndex + ")");
            var productId = row.attr("productId");
            var copiedRow = row.clone();
            var numOfProductRows = getHighestProductRowNum(productId);

            copiedRow.find(".addRow > img").attr("src", "<%=ResolveUrl("~/Content/Images/Icons/delete.png")%>").parent().attr("class", "removeRow");
            copiedRow.find("script").remove();

            copiedRow.find("img").each(function (index) {
                if ($(this).attr('id') == ("tcalico_proddatecal-" + productId + '.1')) {
                    $(this).replaceWith('<img src="<%=ResolveUrl("~/Content/images/calendar/cal.gif")%>" id="tcalico_proddatecal-' + productId + '.' + (numOfProductRows + 1) + '" onclick="A_TCALS[\'proddatecal-'
                            + productId + '.' + (numOfProductRows + 1) + '\'].f_toggle()" class="tcalIcon" alt="Open Calendar"  />');
                }
            });

            // clears first two cells on a copy
            copiedRow.find("td").each(function (index) {
                if (index < 2) {
                    $(this).text("");
                }
            });

            // removes input boxes in cells 3 & 4 on copied row or changes the id of control
            copiedRow.find(":input").each(function (index) {
                var inputName = $(this).attr('name');

                switch (inputName) {
                    case "prodqtyOnHand-" + productId + ".1":
                    case "proddrum-" + productId + ".1":
                    case "proddel-" + productId + ".1":
                    case "proddis-" + productId + ".1":
                        $(this).remove();
                        break;

                    default:
                        $(this).attr('id', inputName.replace(/\d+$/, (numOfProductRows + 1)));
                        $(this).attr('name', inputName.replace(/\d+$/, (numOfProductRows + 1)));
                        $(this).attr('value', '');
                }
            });

            copiedRow.removeAttr("style");
            copiedRow.insertAfter($(this).parents("table").find("tr:eq(" + rowIndex + ")"));

            new tcal({
                'formname': 'fuelForm',
                'controlname': 'proddate-' + productId + '.' + (numOfProductRows + 1),
                'id': 'proddatecal-' + productId + '.' + (numOfProductRows + 1)
            });
        });

        $('.removeRow').live('click', function () {
            var rowIndex = $(this).closest("tr").prevAll("tr").length + 1;
            var row = $(this).parents("table").find("tr:eq(" + rowIndex + ")");
            row.remove();
        });

        var showReason = function() {
            $(".ui-button-text").each(function (index) {
                if ($(this).text() == "Yes" || $(this).text() == "No") {
                    $(this).parent().hide();
                }

                if ($(this).text() == "Ok" || $(this).text() == "Cancel") {
                    $(this).parent().show();
                }
            });

            $('.reason').show();
        }

        var buttons = {
            No: function () {
                $('.reason').hide();
                $(this).dialog("close");
            },
            Yes: function () {
                showReason();
            },
            Cancel: function () {
                $(".ui-button-text").each(function (index) {
                    if ($(this).text() == "Yes" || $(this).text() == "No") {
                        $(this).parent().show();
                    }

                    if ($(this).text() == "Ok" || $(this).text() == "Cancel") {
                        $(this).parent().hide();
                    }
                });

                $('.reason').hide();
            }
        };

        // enable/disable Ok button
        $(".reason textarea").bind('input propertychange', function() {
            if (this.value.length){
                $(".ui-button:contains('Ok')").button("enable");
            } else {
                $(".ui-button:contains('Ok')").button("disable");
            }
        });

        $("#submitButton").click(function () {
            var valid = true;
            var details = [];

            $("#fuelReqTable tr").each(function (index) {
                var oneComplete = false;
                var oneBlank = false;
                var obj = { };

                $(this).find(":input[id^='prod']").each(function () {
                    var inputName = $(this).attr('name');

                    if (inputName.indexOf("prodqtyOnHand") > -1) {
                        obj.productId = parseInt(inputName.substr(inputName.indexOf('-') + 1, inputName.indexOf('.') - inputName.indexOf('-') - 1));
                    }

                    if (inputName.indexOf("prodqtyOnHand") == -1 && inputName.indexOf("proddrum") == -1 && inputName.indexOf("proddel") == -1
                        && inputName.indexOf("proddis") == -1 && inputName.indexOf("prodDetailsId") == -1) {
                        if ($(this).val() == "") {
                            oneBlank = true;
                        } else {
                            obj.rowId = parseInt($(this).val());
                            oneComplete = true;
                        }
                    }
                    else if (inputName.indexOf("proddrum") > -1 || inputName.indexOf("proddel") > -1 || inputName.indexOf("proddis") > -1) {
                        if ($(this).val() == "" || !IsNumeric($(this).val())) {
                            valid = false;
                            oneBlank = true;
                            oneComplete = true;
                        } else {
                            var prop = "";

                            if (inputName.indexOf("proddrum") > -1) {
                                prop = "spareDrums";
                            } else if (inputName.indexOf("proddel") > -1) {
                                prop = "tankDelivered";
                            } else if (inputName.indexOf("proddis") > -1) {
                                prop = "tankDispensed";
                            }

                            obj[prop] = parseInt($(this).val());
                        }
                    }
                    else if (inputName.indexOf("prodqtyOnHand") > -1) {
                        if ($(this).val() == "" || !IsNumeric($(this).val())) {
                            valid = false;
                            oneBlank = true;
                            oneComplete = true;
                        } else {
                            obj.qtyOnHand = parseInt($(this).val());
                        }

                        if ($(this).parent().prev().html() != "" && $(this).val() != "") {
                            capacity = parseInt($(this).parent().prev().html());
                            productName = $(this).parent().prev().prev().text();
                            obj.capacity = capacity;
                            obj.productName = productName.trim().replace(/(\r\n|\n|\r)/gm,"").replace(/ /gm, "").replace("(", " (");
                            qtyOnHand = parseInt($(this).val());

                            if (qtyOnHand > capacity) {
                                valid = false;
                                oneBlank = true;
                                oneComplete = true;
                            }
                        }
                    }
                });

                if (Object.keys(obj).length > 0) {
                    details.push(obj);
                }

                if (oneComplete && oneBlank) {
                    $(this).attr("style", "background-color: #FF6666;");
                    valid = false;
                } else {
                    $(this).removeAttr("style");
                }

            });

            if (!valid) {
                $("#errorMessage").show();
            } else {
                $("#errorMessage").hide();

                var exceptions =[];

                // check for delivery issues
                _.forEach(details, function(d) {
                    if (d.productId && d.hasOwnProperty('tankDelivered')) {
                        var prev = _.find(previousFuelForm, function(ff) { return ff.productId ? ff.productId === d.productId : false; });
                        
                        if (prev) {
                            // book end exception check
                            var diff = prev.qtyOnHand + d.tankDelivered - d.tankDispensed;
                            var comp = d.qtyOnHand - diff;

                            if (comp >= 30 || comp <= -30) {
                                exceptions.push({
                                    name: 'end-book-issue',
                                    prevItem: prev,
                                    item: d
                                });
                            }
                            
                            // expected delivery exception check
                            diff = prev.qtyOnHand > (d.qtyOnHand + d.tankDelivered);

                            if (diff) {
                                exceptions.push({
                                    name: 'delivery-issue',
                                    prevItem: prev,
                                    item: d
                                });
                            }

                            // delivery exception check
                            diff = (prev.qtyOnHand + d.tankDelivered - d.tankDispensed) > d.capacity * .9;

                            if (diff) {
                                exceptions.push({
                                    name: 'expected-delivery-issue',
                                    prevItem: prev,
                                    item: d
                                });
                            }
                        }
                    }

                });

                if (exceptions.length === 0) {
                    if (confirm('Are you sure you want to submit this fuel form?')) {
                        document.forms[0].submit();
                    }
                } else {
                    var okCount = 0,
                          reasons = [];

                    buttons.Ok = function () {
                        $(".ui-button-text").each(function (index) {
                            if ($(this).text() == "Ok" || $(this).text() == "Cancel") {
                                $(this).parent().hide();
                            }

                            if ($(this).text() == "Yes" || $(this).text() == "No") {
                                $(this).parent().show();
                            }

                            if ($(this).text() == "Ok") {
                                $(this).parent().button("disable");
                            }
                        });

                        reasons.push({
                            categoryName: 'Fuel Form Override',
                            exception: $('.ui-dialog-title:visible').text() + ': ' + $('.reason:visible textarea').val()
                        });

                        $('.reason').hide();
                        $(this).dialog("close");
                        okCount++;
                            
                        if (okCount === exceptions.length) {
                            $("#exceptions").val(JSON.stringify(reasons))
                            document.forms[0].submit();
                        }
                    };

                    _.forEach(exceptions, function(ex) {
                        switch(ex.name) {
                            case 'end-book-issue':
                                $("#end-book-amount").text(ex.prevItem.qtyOnHand + ex.item.tankDelivered - ex.item.tankDispensed);
                                $("#end-book-on-hand-qty").text(ex.item.qtyOnHand);
                                break;

                            case 'delivery-issue':
                                $("#delivery-issue-amount").text(ex.item.tankDelivered);
                                $("#delivery-issue-product").text(ex.item.productName);
                                $("#delivery-issue-prod-amount").text("");
                                break;

                            case 'expected-delivery-issue':
                                $("#expected-delivery-issue-amount").text(ex.item.tankDelivered);
                                $("#expected-delivery-issue-expected-amount").text(ex.item.capacity * .9 - (ex.prevItem.qtyOnHand - ex.item.tankDispensed));
                                break;
                        }

                        $("#" + ex.name).dialog({
                            resizable: false,
                            dragable: false,
                            width: 400,
                            modal: true,
                            buttons: buttons
                        });

                        $(".ui-button-text").each(function (index) {
                            if ($(this).text() == "Ok" || $(this).text() == "Cancel") {
                                $(this).parent().hide();
                            }

                            if ($(this).text() == "Ok") {
                                $(this).parent().button("disable");
                            }
                        });
                    });
                }
            } 
        });
    });

function getHighestProductRowNum(productId) {
    var highestNum = 0;

    $("#fuelReqTable").find("[productId=" + productId + "] :input").each(function (index) {
        if ($(this).attr('id').indexOf("prodqty-" + productId + ".") > -1) {
            var parsedNum = parseInt($(this).attr('id').replace("prodqty-" + productId + ".", ""));
            if (parsedNum > highestNum) {
                highestNum = parsedNum;
            }
        }

    });

    return highestNum;
}