﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using iTextSharp.text.pdf.draw;
using System.Net.Mail;
using System.Net;
using System.Text;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the administration area
    /// </summary>
    [HandleError]
    public class PurchaseOrderController : Controller
    {
        #region Variables

        private IPurchaseOrderRepository _purchaseOrderRepository;
        private IUserRepository _userRepository;
        private IProductRepository _productRepository;
        private IDSNYExceptionRepository _dsnyExceptionRepository;
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseOrderController"/> class.
        /// </summary>
        public PurchaseOrderController() : this(null, null, null, null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseOrderController"/> class.
        /// </summary>
        /// <param name="IoCMessageRepo">The inversion of control  message repo.</param>
        /// <param name="IoCUserRepo">The inversion of control  user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public PurchaseOrderController(IPurchaseOrderRepository IoCPurchaseOrderRepo, IUserRepository IoCUserRepository, IProductRepository IoCProductRepository,
            IDSNYExceptionRepository IoCDSNYExceptionRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _purchaseOrderRepository = IoCPurchaseOrderRepo;
            _userRepository = IoCUserRepository;
            _productRepository = IoCProductRepository;
            _dsnyExceptionRepository = IoCDSNYExceptionRepo;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        private Boolean generateFakeData(Guid userId)
        {
            IUser user = _userRepository.getUser(userId);

            if (user.userName.Contains("BK"))
            {
                user.borough = "Brooklyn";
            }
            else if (user.userName.Contains("BX"))
            {
                user.borough = "Bronx";
            }
            else if (user.userName.Contains("MN"))
            {
                user.borough = "Manhattan";
            }
            else if (user.userName.Contains("QN"))
            {
                user.borough = "Queens";
            }
            else if (user.userName.Contains("SI"))
            {
                user.borough = "Staten Island";
            }

            Random r = new Random();

            user.address = r.Next(100, 999).ToString() + " Main St";
            user.poDisplayName = user.userName;

            _userRepository.updateUser(user);

            return false;
        }
        #endregion

        #region Actions

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Create()
        {
            List<GetPurchaseOrderItems_Result> gpos = _purchaseOrderRepository.getPurchaseOrder().ToList();

            // fill in the view model, holding two arrays: purchase order line and data for that purchase order line
            PurchaseOrdersViewModel pos = new PurchaseOrdersViewModel()
            {
                pos = gpos.Select(p => new PurchaseOrderViewModel()
                {
                    pos = new Purchase_Orders() {
                        UserId = p.UserId,
                        aspnet_Users = new aspnet_Users()
                        {
                            UserId = p.UserId,
                            UserName = p.UserName
                        },
                        Product_ID = p.Product_ID,
                        Product = new Product()
                        {
                            Product_ID = p.Product_ID,
                            Product_Name = p.Product_Name,
                            Measurement_Type = p.Measurement_Type,
                            Sub_Type = p.Sub_Type
                        },
                        Vendor_ID = p.Vendor_ID,
                        Vendor = new Vendor() {
                            Vendor_ID = p.Vendor_ID,
                            Vendor_Name = p.Vendor_Name,
                            Account_Number = p.Account_Number,
                            Phone_Number = p.Phone_Number
                        },
                        Order_Date = DateTime.Now,
                        Is_Ordered = false
                    },
                    userId = p.UserId,
                    address = p.Address,
                    borough = p.Borough,
                    poDisplayName = p.PODisplayName,
                    productId = p.Product_ID,
                    vendorId = p.Vendor_ID,
                    capacity = p.Capacity != null ? (decimal)p.Capacity : -1,
                    avgDispensed = p.Avg_Dispensed != null ? (int)p.Avg_Dispensed : -1,
                    startOnHand = p.Start_On_Hand != null ? (int)p.Start_On_Hand : -1,
                    endOnHand = p.End_On_Hand != null ? (int)p.End_On_Hand : -1,
                    isOrdered = true,
                    orderAmount = p.Avg_Dispensed != null ? (int)p.Avg_Dispensed : -1
                })
                .OrderBy(p => p.pos.Product.Product_Name).ThenBy(p => p.pos.aspnet_Users.UserName)
                .ToList()
            };

            // generate fake data
            //foreach (PurchaseOrderViewModel po in pos.pos)
            //{
            //    generateFakeData(po.userId);
            //}

            ViewData["users"] = _userRepository.getAllUsers(true);
            ViewData["fuelTypes"] = _productRepository.getAllProducts(false);

            return View("~/Views/Admin/PurchaseOrder/Create.aspx", pos);
        }

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Create(PurchaseOrdersViewModel model)
        {
            if (ModelState.IsValid)
            {
                Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
                int exceptionCategoryId = _dsnyExceptionRepository.getDSNYExceptionCategory("Purchase Order Override").ExceptionCategoryId;

                List<Purchase_Orders> orders = model.pos
                    .Where(p => p.isOrdered)
                    .Select(p => new Purchase_Orders() 
                    {
                        UserId  = p.userId,
                        Product_ID = p.productId,
                        Order_Amount = p.orderAmount,
                        Order_Date = DateTime.Now,
                        Vendor_ID = p.vendorId,
                        Is_Ordered = true
                    }).ToList();

                _purchaseOrderRepository.createPurchaseOrder(orders);

                List<DSNYException> exceptions = model.pos
                    .Where(p => p.exceptionReason != null && p.exceptionReason != "")
                    .Select(p => new DSNYException()
                    {
                        Exception = p.exceptionReason,
                        ExceptionCategoryId = exceptionCategoryId,
                        ExceptionUser = currentUserGuid,
                        ExceptionDateTime = DateTime.Now
                    }).ToList();

                _dsnyExceptionRepository.addExceptions(exceptions);

                List<String> pdfs = createPDF(model);

                sendEmails(pdfs);

                deletePdfs(pdfs);
                pdfs = null;

                return RedirectToAction("Index", "Admin");
            }
            else
            {
                return View("~/Views/Admin/PurchaseOrder/Create.aspx", model);
            }
        }

        #endregion

        #region Email Send

        private void sendEmails(List<String> pdfs)
        {
            try
            {
                // send out the email with one large bcc instead of sending it to specific users
                // set up the smtp mail info

                string mailServer = ConfigurationManager.AppSettings["Mail Server"];
                string mailUser = ConfigurationManager.AppSettings["Mail User"];
                string mailPassword = ConfigurationManager.AppSettings["Mail Password"];
                string mailFrom = ConfigurationManager.AppSettings["Mail From Address"];
                bool useSSL = false;
                int mailPort = 25;

                bool.TryParse(ConfigurationManager.AppSettings["Mail SSL"], out useSSL);
                int.TryParse(ConfigurationManager.AppSettings["Mail Port"], out mailPort);

                if (!string.IsNullOrEmpty(mailServer))
                {
                    SmtpClient sc = new SmtpClient(mailServer);

                    if (!string.IsNullOrEmpty(mailUser) && !string.IsNullOrEmpty(mailPassword))
                    {
                        NetworkCredential nc = new NetworkCredential(mailUser, mailPassword);
                        sc.UseDefaultCredentials = false;
                        sc.Credentials = nc;
                    }
                    else
                        sc.UseDefaultCredentials = true;

                    sc.EnableSsl = useSSL;
                    sc.Port = mailPort;

                    // set up the mail message going out
                    MailMessage mm = new MailMessage();

                    // setup message
                    mm.From = new MailAddress(mailFrom, "DSNY CMS Purchase Order System");
                    mm.Subject = DateTime.Now.ToShortDateString() + " Purchase Orders";
                    mm.IsBodyHtml = false;
                    //mm.BodyEncoding = Encoding.UTF8;
                    mm.Body += "Purcahse Orders for " + DateTime.Now.ToShortDateString();

                    foreach (String file in pdfs)
                    {
                        mm.Attachments.Add(new Attachment(file, "application/pdf"));
                    }

                    mm.To.Add("ecolman@gmail.com");

                    //mm.Bcc.Add("ecolman@gmail.com");

                    try
                    {
                        // send mail message
                        sc.Send(mm);
                    }
                    catch (SmtpFailedRecipientException ex)
                    {
                        _exceptionHandler.HandleException(ex);
                    }

                    // dispose of our objects
                    mm.Dispose();
                    sc.Dispose();
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }


        #endregion

        #region PDF

        private List<String> createPDF(PurchaseOrdersViewModel items)
        {
            List<String> returnFiles= new List<String>();

            string path = Server.MapPath("..\\PDFs");

            // prepare fonts
            iTextSharp.text.Font titleFont = FontFactory.GetFont("Arial", 28, new BaseColor(30,149, 33));
            iTextSharp.text.Font subFont = FontFactory.GetFont("Arial", 14, BaseColor.BLACK);
            iTextSharp.text.Font productFont = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font tableHeaderFont = FontFactory.GetFont("Arial", 12, BaseColor.WHITE);
            iTextSharp.text.Font tableFont = FontFactory.GetFont("Arial", 12, BaseColor.BLACK);

            // title
            var title = new Paragraph("New York City\nDepartment of Sanitation\n\n", titleFont);
            title.Alignment = Element.ALIGN_CENTER;

            // get each product Id to create a PO for and get all Products for into
            List<int> productOrders = items.pos.Where(i => i.isOrdered).Select(i => i.productId).Distinct().ToList();
            List<Product> products = _productRepository.getAllProducts(false);
            
            // loop over products to build each PO
            foreach (int pId in productOrders)
            {
                // grab specific orders and specific product
                var orders = items.pos.Where(i => i.productId == pId).ToList();
                Product prod = products.FirstOrDefault(i => i.Product_ID == pId);

                // prepare sub-header and product name
                Chunk glue = new Chunk(new VerticalPositionMark());
                Paragraph sub = new Paragraph(prod.Vendor.Vendor_Name, subFont);
                Paragraph subVendor = new Paragraph("", subFont);
                Paragraph product = new Paragraph(prod.Product_Name, productFont);
                product.Alignment = Element.ALIGN_CENTER;

                sub.Add(new Chunk(glue));
                sub.Add(DateTime.Now.ToLongDateString());

                // add vendor data if we have it
                if (prod.Vendor.Account_Number != null)
                {
                    subVendor.Add(prod.Vendor.Account_Number);
                }

                if (prod.Vendor.Phone_Number != null) 
                {
                    subVendor.Add("\n" + prod.Vendor.Phone_Number);
                }

                // create order table
                PdfPTable table = new PdfPTable(4);
                table.TotalWidth = 525f;    //fix the absolute width of the table
                table.LockedWidth = true;
                float[] widths = new float[] { 1f, 2f, 3f, 1f };    //relative col widths in proportions - 1/3, 2/3, 3/5
                table.SetWidths(widths);
                table.HorizontalAlignment = 0;
                table.SpacingBefore = 20f;
                table.SpacingAfter = 30f;

                // table header cell
                PdfPCell header1 = new PdfPCell(new Phrase("Sub Type", tableHeaderFont));
                PdfPCell header2 = new PdfPCell(new Phrase("District", tableHeaderFont));
                PdfPCell header3 = new PdfPCell(new Phrase("Address", tableHeaderFont));
                PdfPCell header4 = new PdfPCell(new Phrase("Amount", tableHeaderFont));
                header1.BackgroundColor = new BaseColor(30, 149, 33);
                header2.BackgroundColor = new BaseColor(30, 149, 33);
                header3.BackgroundColor = new BaseColor(30, 149, 33);
                header4.BackgroundColor = new BaseColor(30, 149, 33);

                header1.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                header2.HorizontalAlignment = 1;
                header3.HorizontalAlignment = 1;
                header4.HorizontalAlignment = 1;

                table.AddCell(header1);
                table.AddCell(header2);
                table.AddCell(header3);
                table.AddCell(header4);

                string bourough = null;

                // loop over orders to build order table
                foreach (var item in orders)
                {
                    // add table row per each bourough
                    if (item.borough != null && item.borough != bourough)
                    {
                        bourough = item.borough;
                        PdfPCell bouroughCell = new PdfPCell(new Phrase(bourough, tableHeaderFont));
                        bouroughCell.BackgroundColor = new BaseColor(0, 128, 0);
                        bouroughCell.Colspan = 5;
                        bouroughCell.HorizontalAlignment = 1;

                        table.AddCell(bouroughCell);
                    }

                    // add order item
                    PdfPCell cell1 = new PdfPCell(new Phrase(prod.Sub_Type, tableFont));
                    PdfPCell cell2 = new PdfPCell(new Phrase(item.poDisplayName, tableFont));
                    PdfPCell cell3 = new PdfPCell(new Phrase(item.address, tableFont));
                    PdfPCell cell4 = new PdfPCell(new Phrase(item.orderAmount.ToString(), tableFont));

                    cell1.HorizontalAlignment = 1;
                    cell2.HorizontalAlignment = 1;
                    cell3.HorizontalAlignment = 1;
                    cell4.HorizontalAlignment = 1;

                    table.AddCell(cell1);
                    table.AddCell(cell2);
                    table.AddCell(cell3);
                    table.AddCell(cell4);
                }
                
                var doc = new Document();
                string fileName = path + "\\" + prod.Product_Name.Replace(@"/", @"-") + ".pdf";
                PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
                // Create a new PdfWriter object, specifying the output stream
                //var output = new MemoryStream();
                //var writer = PdfWriter.GetInstance(doc, output);

                // build PDF from parts defined above
                doc.Open();
                doc.Add(title);
                doc.Add(sub);
                doc.Add(subVendor);
                doc.Add(new Paragraph("\n"));
                doc.Add(product);
                doc.Add(table);
                doc.Close();

                returnFiles.Add(fileName);
            }

            return returnFiles;
        }

        private bool deletePdfs(List<String> pdfs) {
            try
            {
                foreach (String file in pdfs)
                {
                    System.IO.File.Delete(file);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
    }
}