﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the administration area
    /// </summary>
    [HandleError]
    public class AdminController : Controller
    {
        #region Variables

        private IPurchaseOrderRepository _purchaseOrderRepository;
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;

        private int pageSize = MvcApplication.pageSize;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseOrderController"/> class.
        /// </summary>
        public AdminController() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseOrderController"/> class.
        /// </summary>
        /// <param name="IoCMessageRepo">The inversion of control  message repo.</param>
        /// <param name="IoCUserRepo">The inversion of control  user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public AdminController(IPurchaseOrderRepository IoCPurchaseOrderRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _purchaseOrderRepository = IoCPurchaseOrderRepo;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator, Reports")]
        public ActionResult Index()
        {
            ViewData["purchaseOrder"] = _purchaseOrderRepository.getLastPurchaseOrder();

            return View();
        }

        #endregion
    }
}