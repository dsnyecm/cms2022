﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DSNY.Core.Interfaces;
using DSNY.Core.Repository;
using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Data;
using DSNY.ViewModels;
using DSNY.Common;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for product pages
    /// </summary>
    [HandleError]
    public class ProductController : Controller
    {
        #region Variables

        private IProductRepository _productRepository;
        private IVendorRepository _vendorRepository;
        private IUserRepository _userRepository;
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductController"/> class.
        /// </summary>
        public ProductController() : this(null, null, null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductController"/> class.
        /// </summary>
        /// <param name="IoCProductRepository">The inversion of control product repository.</param>
        /// /// <param name="IoCProductRepository">The inversion of control vendor repository.</param>
        /// <param name="IoCUserRepository">The inversion of control user repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public ProductController(IProductRepository IoCProductRepository, IVendorRepository IoCVendorRepository, IUserRepository IoCUserRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _productRepository = IoCProductRepository;
            _vendorRepository = IoCVendorRepository;
            _userRepository = IoCUserRepository;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region List

        /// <summary>
        /// Lists this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult List()
        {
            List<Product> model = _productRepository.getAllProducts(true);

            Random r = new Random();

            foreach (var p in model)
            {
                p.Sub_Type = "B" + r.Next(1, 6);
                _productRepository.updateProduct(p.Product_ID, p, false);
            }

            return View("~/Views/Admin/Product/List.aspx", model);
        }

        #endregion

        #region Create

        /// <summary>
        /// Create form for product
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Create()
        {
             List<SelectListItem> OrderDeliveryTypes = new List<SelectListItem>() {
                new SelectListItem { Value = "Email", Text = "Email" },
                new SelectListItem { Value = "Fax", Text = "Fax" }
             };

            ViewData["OrderDeliveryTypes"] = OrderDeliveryTypes;
            ViewData["Order_Delivery_Type"] = null;

            // get the vendors and prepare for select list
            ViewData["Vendors"] = new SelectList(_vendorRepository.getAllVendors(false), "Vendor_ID", "Vendor_Name");
            ViewData["Vendor_ID"] = null;

            return View("~/Views/Admin/Product/Create.aspx");
        }

        /// <summary>
        /// HTTP Post create action. Creates a product.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Create(Product model, FormCollection collection)
        {
            try
            {
                _productRepository.addProduct(model);

                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex);

                return View("~/Views/Admin/Product/Create.aspx");
            }
        }

        #endregion

        #region Edit

        /// <summary>
        /// Edit form for a product.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Edit(int id)
        {
            // Populate our model
            Product model = _productRepository.getProduct(id);

            // get the vendors and prepare for select list
            ViewData["Vendors"] = new SelectList(_vendorRepository.getAllVendors(false), "Vendor_ID", "Vendor_Name");
            ViewData["Vendor_ID"] = model.Vendor.Vendor_ID;

            // order delivery type select list
            ViewData["OrderDeliveryTypes"] = new SelectList(new List<SelectListItem>() {
               new SelectListItem { Value = "Email", Text = "Email" },
               new SelectListItem { Value = "Fax", Text = "Fax", Selected = true }
            }, "Value", "Text");
            ViewData["Order_Delivery_Type"] = model.Order_Delivery_Type;

            return View("~/Views/Admin/Product/Edit.aspx", model);
        }

        /// <summary>
        /// HTTP Post create action.  Edit a product.
        /// </summary>
        /// <param name="id">The product id.</param>
        /// <param name="model">The product model.</param>
        /// <param name="collection">The form collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Edit(int id, Product model, FormCollection collection)
        {
            try
            {
                // Update product
                _productRepository.updateProduct(id, model, false);

                return RedirectToAction("List");
            }
            catch (Exception)
            {
                ModelState.AddModelError("Error", "There was an error updating the product. Please try again.");

                return View("~/Views/Admin/Product/Edit.aspx", model);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the specified product.
        /// </summary>
        /// <param name="id">The product id.</param>
        /// <param name="name">The product name.</param>
        /// <param name="collection">The form collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult Delete(int id, string name, FormCollection collection)
        {
            var returnValue = new { status = "failure", id = id, name = name };

            try
            {
                _productRepository.deleteProduct(id);
                returnValue = new { status = "success", id = id, name = name };
            }
            catch (Exception)
            {
                // do nothing, it was caught and then return a 
            }

            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sort

        /// <summary>
        /// Sortes the product grid.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult SortedGrid(string field, string direction)
        {
            return View("ProductGrid", _productRepository.getSortedProducts(field, direction == "ASC" ? 
                Enums.SortDirection.Ascending : Enums.SortDirection.Descending, true));
        }

        #endregion
    }
}