﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

using DSNY.Common.Utilities;

namespace DSNY.Common.Exception
{
    /// <summary>
    /// Microsoft Enterprise Exception handler class which implements IExceptionHandler.
    /// This uses a singleton pattern to ensure that only one exception handler is active at one time
    /// </summary>
    public class EnterpriseExceptionHandler : IExceptionHandler
    {
        private string Exception_Policy;

        /// <summary>
        /// Constructor, grabs the exception policy group name from the config file
        /// </summary>
        public EnterpriseExceptionHandler()
        {
            // Set our exception policy from core.config
            Exception_Policy = ConfigurationManager.AppSettings["Exception Policy Group"];
        }

        /// <summary>
        /// Writes an exception message to the exception log
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public bool HandleException(System.Exception ex)
        {
            return ExceptionPolicy.HandleException(ex, Exception_Policy);
        }

        /// <summary>
        /// Writes an exception message to the exception log
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="policy"></param>
        /// <returns></returns>
        public bool HandleException(System.Exception ex, string policy)
        {
            if (policy == string.Empty)
                policy = Exception_Policy;

            return ExceptionPolicy.HandleException(ex, policy);
        }
    }
}