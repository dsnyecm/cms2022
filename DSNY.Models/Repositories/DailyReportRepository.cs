﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.Core.Interfaces;
using DSNY.Common;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IRoleRepository"/> interface
    /// </summary>
    public class DailyReportRepository : IDailyReportRepository
    {
        #region Variables

        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRepository"/> class.
        /// </summary>
        public DailyReportRepository() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRepository"/> class.
        /// </summary>
        /// <param name="IoCProvider">The inversion of control role provider.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public DailyReportRepository(DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets all roles list.
        /// </summary>
        /// <returns></returns>
        public DailyReport getLastestReport()
        {
            try
            {
                return _dataProvider.DataContext.DailyReports.OrderByDescending(dr => dr.DailyReportDate).First();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);

                return new DailyReport();
            }
        }

        /// <summary>
        /// Gets all roles list.
        /// </summary>
        /// <returns></returns>
        public List<DailyReportsWeather> getAllDailyReportWeather()
        {
            try
            {
                return _dataProvider.DataContext.DailyReportsWeathers.ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);

                return new List<DailyReportsWeather>();
            }
        }

        /// <summary>
        /// Gets all roles list.
        /// </summary>
        /// <returns></returns>
        public List<DailyReportsFunction> getAllDailyReportFunctions()
        {
            try
            {
                return _dataProvider.DataContext.DailyReportsFunctions.ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);

                return new List<DailyReportsFunction>();
            }
        }

        /// <summary>
        /// Gets all roles list.
        /// </summary>
        /// <returns></returns>
        public List<DailyReportsTemperature> getAllDailyReportTemperatures()
        {
            try
            {
                return _dataProvider.DataContext.DailyReportsTemperatures.ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);

                return new List<DailyReportsTemperature>();
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds a fuel form.
        /// </summary>
        /// <param name="newFuelForm">The new fuel form.</param>
        public void addDailyReport(DailyReport newDailyReport)
        {
            _dataProvider.DataContext.DailyReports.AddObject(newDailyReport);
            _dataProvider.DataContext.SaveChanges();
        }

        #endregion
    }
}