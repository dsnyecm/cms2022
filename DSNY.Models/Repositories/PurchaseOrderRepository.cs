﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Security;

using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Core.Interfaces;
using DSNY.Core.Models;
using DSNY.Common;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IPurchaseOrderRepository"/> interface
    /// </summary>
    public class PurchaseOrderRepository : IPurchaseOrderRepository
    {
        #region Variables

        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseOrderRepository"/> class.
        /// </summary>
        public PurchaseOrderRepository() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseOrderRepository"/> class.
        /// </summary>
        /// <param name="IoCProvider">The inversion of control role provider.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public PurchaseOrderRepository(DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger ;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Compiles the latest purchase order
        /// </summary>
        /// <returns>List<GetPurchaseOrderItems_Result></returns>
        public List<GetPurchaseOrderItems_Result> getPurchaseOrder()
        {
            return _dataProvider.DataContext.GetPurchaseOrderItems(60).ToList();
        }

        /// <summary>
        /// Gets last purchase order
        /// </summary>
        /// <returns>GetPurchaseOrderItems_Result</returns>
        public Purchase_Orders getLastPurchaseOrder()
        {
            return _dataProvider.DataContext.Purchase_Orders.OrderByDescending(p => p.Order_Date).FirstOrDefault();
        }
        
        #endregion

        #region Add

        public void createPurchaseOrder(List<Purchase_Orders> pos)
        {
            try
            {
                foreach (Purchase_Orders p in pos)
                {
                    _dataProvider.DataContext.Purchase_Orders.AddObject(p);
                }

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Delete

        #endregion

    }
}