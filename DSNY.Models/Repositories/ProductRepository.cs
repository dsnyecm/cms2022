﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Core.Interfaces;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using System.Data.Objects.DataClasses;
using DSNY.Common;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IProductRepository"/> interface
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        #region Variables

        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private IUserRepository _userRepository = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductRepository"/> class.
        /// </summary>
        public ProductRepository() : this(null, null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductRepository"/> class.
        /// </summary>
        /// <param name="IoCUserRepository">The inversion of control user repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public ProductRepository(DSNYContext IoCDataProvider, IUserRepository IoCUserRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _userRepository = IoCUserRepository;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Product> getAllProducts(bool includeInactive)
        {
            if (includeInactive)
                return _dataProvider.DataContext.Products.OrderBy(p => p.Product_Name).ToList();
            else
                return _dataProvider.DataContext.Products.Where(p => p.is_Active == true).OrderBy(p => p.Product_Name).ToList();
        }

        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns></returns>
        public Product getProduct(int productId)
        {
            return _dataProvider.DataContext.Products.SingleOrDefault(p => p.Product_ID == productId);
        }

        /// <summary>
        /// Gets the sorted products.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Product> getSortedProducts(string field, Enums.SortDirection direction, bool includeInactive)
        {
            List<Product> unSortedProducts = getAllProducts(includeInactive);
            List<Product> sortedProduct;

            if (direction == Enums.SortDirection.Descending)
            {
                sortedProduct = (from Product in unSortedProducts
                                    orderby orderBy(field, Product) descending
                                    select Product).ToList();
            }
            else
            {
                sortedProduct = (from Product in unSortedProducts
                                    orderby orderBy(field, Product) ascending
                                    select Product).ToList();
            }

            _dataProvider.DataContext.Products.MergeOption = System.Data.Objects.MergeOption.NoTracking;

            return sortedProduct;
        }

        /// <summary>
        /// Gets the users of product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns></returns>
        public List<Product_User> getUsersOfProduct(int productId)
        {
            try
            {
                List<Product_User> productUsers = _dataProvider.DataContext.Product_User.Where(pu => pu.Product_ID == productId && pu.Product.is_Active == true).ToList();

                return productUsers;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);

                return new List<Product_User>();
            }
        }

        /// <summary>
        /// Gets the products for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<Product_User> getProductsForUser(Guid userId)
        {
            List<Product_User> blah = _dataProvider.DataContext.Product_User.Where(pu => pu.UserId == userId && pu.Product.is_Active == true).OrderBy(pu => pu.Product.Order_Num).ToList();

            return blah;
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds the product.
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <param name="isActive">if set to <c>true</c> [is active].</param>
        /// <param name="isDrums">if set to <c>true</c> [is drums].</param>
        /// <param name="isDrums">if set to <c>true</c> [is reading required].</param>
        /// <param name="defaultCapacity">The default capacity.</param>
        /// <param name="measurementType">Type of the measurement.</param>
        /// <returns></returns>
        public int addProduct(string productName, string subType, bool isActive, bool isDrums, bool isReadingRequired, int defaultCapacity, string measurementType)
        {
            Product newProduct = new Product()
            {
                Product_Name = productName,
                Sub_Type = subType,
                is_Active = isActive,
                is_Drums = isDrums,
                is_Reading_Required = isReadingRequired,
                Default_Capacity = defaultCapacity,
                Measurement_Type = measurementType
            };

            _dataProvider.DataContext.Products.AddObject(newProduct);
            _dataProvider.DataContext.SaveChanges();

            return newProduct.Product_ID;
        }

        /// <summary>
        /// Adds the product.
        /// </summary>
        /// <param name="newProduct">The new product.</param>
        /// <returns></returns>
        public int addProduct(Product newProduct)
        {
            _dataProvider.DataContext.Products.AddObject(newProduct);
            _dataProvider.DataContext.SaveChanges();

            return newProduct.Product_ID;
        }

        /// <summary>
        /// Adds the user to product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="capacity">The capacity.</param>
        public void addUserToProduct(int productId, Guid userId, int capacity)
        {
            // Now add the product back
            Product_User newProdUser = _dataProvider.DataContext.Product_User.SingleOrDefault(pu => pu.Product_ID == productId && pu.UserId == userId);

            if (newProdUser != null)
            {
                newProdUser.Capacity = capacity;
            }
            else
            {
                if (capacity > 0)
                    newProdUser = new Product_User() { Product_ID = productId, UserId = userId, Capacity = capacity };
                else
                    newProdUser = new Product_User() { Product_ID = productId, UserId = userId };

                _dataProvider.DataContext.Product_User.AddObject(newProdUser);
            }

            _dataProvider.DataContext.SaveChanges();
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <param name="updatedProduct">The updated product.</param>
        /// <param name="removeUsers">if set to <c>true</c> [remove users].</param>
        /// <returns></returns>
        public bool updateProduct(int productId, Product updatedProduct, bool removeUsers)
        {
            Product foundProduct = _dataProvider.DataContext.Products.Include("Product_User").SingleOrDefault(p => p.Product_ID == productId);

            if (foundProduct != null && foundProduct.Product_ID > 0)
            {
                foundProduct.Product_Name = updatedProduct.Product_Name;
                foundProduct.Sub_Type = updatedProduct.Sub_Type;
                foundProduct.is_Active = updatedProduct.is_Active;
                foundProduct.is_Drums = updatedProduct.is_Drums;
                foundProduct.is_Reading_Required = updatedProduct.is_Reading_Required;
                foundProduct.Order_Num = updatedProduct.Order_Num;
                foundProduct.Default_Capacity = updatedProduct.Default_Capacity;
                foundProduct.Measurement_Type = updatedProduct.Measurement_Type;
                foundProduct.Vendor_ID = updatedProduct.Vendor_ID;
                foundProduct.Order_Delivery_Type = updatedProduct.Order_Delivery_Type;
                foundProduct.Order_Delivery_Distribution = updatedProduct.Order_Delivery_Distribution;

                if (removeUsers)
                {
                    foreach (Product_User user in foundProduct.Product_User.ToList())
                    {
                        foundProduct.Product_User.Remove(user);
                    }
                }

                _dataProvider.DataContext.SaveChanges();

                return true;
            }
            else
                return false;
        }

        public bool updateProductUserCapacity(int productId, Guid userId, int capacity)
        {
            Product_User foundUserProduct = _dataProvider.DataContext.Product_User.SingleOrDefault(pu => pu.Product_ID == productId && pu.UserId == userId);

            if (foundUserProduct != null && foundUserProduct.Product_ID > 0)
            {
                foundUserProduct.Capacity = capacity;

                _dataProvider.DataContext.SaveChanges();

                return true;
            }
            else
                return false;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        public void deleteProduct(int productId)
        {
            try
            {
                // Get all Product_User items which belong to this product and delete those references
                IQueryable<Product_User> productUsers = _dataProvider.DataContext.Product_User.Where(pu => pu.Product_ID == productId);

                foreach (Product_User pu in productUsers)
                {
                    _dataProvider.DataContext.Product_User.DeleteObject(pu);
                }

                // Now delete the product itself and then save all changes
                Product deleteProduct = _dataProvider.DataContext.Products.Single(p => p.Product_ID == productId);

                if (deleteProduct != null)
                    _dataProvider.DataContext.Products.DeleteObject(deleteProduct);

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                if (ex is ArgumentNullException || ex is InvalidOperationException)
                {
                    _dataProvider.DataContext.SaveChanges();
                }
                else
                {
                    bool rethrow = _exceptionHandler.HandleException(ex);

                    if (rethrow)
                        throw new Exception("There was an error deleting the product.");
                }
            }
        }

        /// <summary>
        /// Deletes all products for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        public void deleteAllProductsForUser(Guid userId)
        {
            List<Product_User> productsForUser = _dataProvider.DataContext.Product_User.Where(pu => pu.UserId == userId).ToList();

            foreach (Product_User pu in productsForUser)
            {
                _dataProvider.DataContext.Product_User.DeleteObject(pu);
            }

            _dataProvider.DataContext.SaveChanges();
        }

        #endregion

        #region Sort

        // Our sorter object, holds the field key and object type
        /// <summary>
        /// Orders the by.
        /// </summary>
        /// <param name="sortKey">The sort key.</param>
        /// <param name="product">The product.</param>
        /// <returns></returns>
        private object orderBy(string sortKey, Product product)
        {
            switch (sortKey)
            {
                case "ProductName":
                    return product.Product_Name;

                case "IsActive":
                    return product.is_Active;

                case "IsDrum":
                    return product.is_Drums;

                case "IsReadingRequired":
                    return product.is_Reading_Required;

                case "MeasurementType":
                    return product.Measurement_Type;

                case "OrderNum":
                    return product.Order_Num;

                case "DefaultCapacity":
                    return product.Default_Capacity;

                default:
                    return product.Product_Name;
            }
        }

        #endregion
    }
}