﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.Core.Interfaces;
using DSNY.Common;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IFuelFormRepository"/> interface
    /// </summary>
    public class FuelFormRepository : IFuelFormRepository
    {
        #region Variables
        
        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FuelFormRepository"/> class.
        /// </summary>
        public FuelFormRepository() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FuelFormRepository"/> class.
        /// </summary>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public FuelFormRepository(DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets the fuel form.
        /// </summary>
        /// <param name="fuelformId">The fuelform id.</param>
        /// <returns></returns>
        public Fuel_Form getFuelForm(int fuelformId)
        {
            return _dataProvider.DataContext.Fuel_Form.SingleOrDefault(ff => ff.Fuel_Form_ID == fuelformId);
        }

        /// <summary>
        /// Gets the fuel form.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="submissionDate">The submission date.</param>
        /// <returns></returns>
        public Fuel_Form getFuelForm(Guid userId, DateTime submissionDate)
        {
            List<Fuel_Form> forms = _dataProvider.DataContext.Fuel_Form.Where(ff => ff.Submission_Date.Value.Year == submissionDate.Year && ff.Submission_Date.Value.Month == submissionDate.Month &&
                                        ff.Submission_Date.Value.Day == submissionDate.Day && ff.UserId == userId).ToList();

            if (forms.Count > 0)
                return forms[forms.Count - 1];
            else
                return null;
        }

        /// <summary>
        /// Gets last completed fuel form.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="submissionDate">The submission date.</param>
        /// <returns></returns>
        public Fuel_Form getLastFuelForm(Guid userId, int take = 1)
        {
            List<Fuel_Form> forms = _dataProvider.DataContext.Fuel_Form.Where(ff => ff.UserId == userId).OrderByDescending(ff => ff.Submission_Date).Take(take).ToList();

            if (forms.Count > 0)
            {
                List<Fuel_Form_Equipment_Failure> previousFuelForms = forms[forms.Count - 1].Fuel_Form_Equipment_Failure.ToList();
                return forms[forms.Count - 1];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the fuel form details.
        /// </summary>
        /// <param name="fuelFormId">The fuel form id.</param>
        /// <returns></returns>
        public List<Fuel_Form_Details> getFuelFormDetails(int fuelFormId)
        {
            return _dataProvider.DataContext.Fuel_Form_Details.Where(ffd => ffd.Fuel_Form_ID == fuelFormId).ToList();
        }

        /// <summary>
        /// Gets the fuel form details delivery.
        /// </summary>
        /// <param name="fuelformDetailsId">The fuelform details id.</param>
        /// <returns></returns>
        public List<Fuel_Form_Details_Delivery> getFuelFormDetailsDelivery(int fuelformDetailsId)
        {
            return _dataProvider.DataContext.Fuel_Form_Details_Delivery.Where(ffd => ffd.Fuel_Form_Details_Id == fuelformDetailsId).OrderByDescending(ffd => ffd.Order_No).ToList();
        }

        /// <summary>
        /// Gets the fuel form equip failure.
        /// </summary>
        /// <param name="fuelFormId">The fuel form id.</param>
        /// <returns></returns>
        public List<Fuel_Form_Equipment_Failure> getFuelFormEquipFailure(int fuelFormId)
        {
            return _dataProvider.DataContext.Fuel_Form_Equipment_Failure.Where(ffd => ffd.Fuel_Form_ID == fuelFormId)
                                                .OrderBy(ffd => ffd.Equipment_User.Equipment.Equipment_Name + ffd.Equipment_User.Equipment_Description).ToList();
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds a fuel form.
        /// </summary>
        /// <param name="newFuelForm">The new fuel form.</param>
        public void addFuelForm(Fuel_Form newFuelForm)
        {
            _dataProvider.DataContext.Fuel_Form.AddObject(newFuelForm);
            _dataProvider.DataContext.SaveChanges();
        }

        /// <summary>
        /// Adds the fuel form details.
        /// </summary>
        /// <param name="newFuelFormDetails">The new fuel form details.</param>
        public void addFuelFormDetails(Fuel_Form_Details newFuelFormDetails)
        {
            _dataProvider.DataContext.Fuel_Form_Details.AddObject(newFuelFormDetails);
            _dataProvider.DataContext.SaveChanges();
        }

        /// <summary>
        /// Adds the fuel form details delivery.
        /// </summary>
        /// <param name="newFuelFormDetailsDelivery">The new fuel form details delivery.</param>
        public void addFuelFormDetailsDelivery(Fuel_Form_Details_Delivery newFuelFormDetailsDelivery)
        {
            _dataProvider.DataContext.Fuel_Form_Details_Delivery.AddObject(newFuelFormDetailsDelivery);
            _dataProvider.DataContext.SaveChanges();
        }

        /// <summary>
        /// Adds the fuel form equip failure.
        /// </summary>
        /// <param name="newFuelFormEquipFailure">The new fuel form equip failure.</param>
        public void addFuelFormEquipFailure(Fuel_Form_Equipment_Failure newFuelFormEquipFailure)
        {
            _dataProvider.DataContext.Fuel_Form_Equipment_Failure.AddObject(newFuelFormEquipFailure);
            _dataProvider.DataContext.SaveChanges();
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates a fuel form.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public void updateFuelForm(int fuelFormId, Fuel_Form updateFuelForm, Guid userId)
        {
            try
            {
                Fuel_Form foundFuelForm = _dataProvider.DataContext.Fuel_Form.SingleOrDefault(ff => ff.Fuel_Form_ID == fuelFormId);

                if (foundFuelForm != null)
                {
                    foundFuelForm.Submission_Date = updateFuelForm.Submission_Date;
                    foundFuelForm.Remarks = updateFuelForm.Remarks;
                }

                _dataProvider.DataContext.SaveChanges();

                // archive changes
                archiveFuelForm(foundFuelForm, userId);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Updates the fuel form details.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public void updateFuelFormDetails(int fuelFormDetailsId, Fuel_Form_Details updateFuelFormDetails, Guid userId)
        {
            try
            {
                Fuel_Form_Details foundFuelFormDetails = _dataProvider.DataContext.Fuel_Form_Details.SingleOrDefault(ffd => ffd.Fuel_Form_Details_Id == fuelFormDetailsId);

                if (foundFuelFormDetails != null)
                {
                    Fuel_Form_Details_Delivery_History archivedFuelForm = new Fuel_Form_Details_Delivery_History
                    {
                        Fuel_Form_Details_Id = foundFuelFormDetails.Fuel_Form_Details_Id,
                    };

                    foundFuelFormDetails.On_Hand = updateFuelFormDetails.On_Hand;
                    foundFuelFormDetails.Spare_Drums = updateFuelFormDetails.Spare_Drums;
                    foundFuelFormDetails.Tank_Delivered = updateFuelFormDetails.Tank_Delivered;
                    foundFuelFormDetails.Tank_Dispensed = updateFuelFormDetails.Tank_Dispensed;

                    _dataProvider.DataContext.SaveChanges();

                    // archive changes
                    archiveFuelFormDetails(foundFuelFormDetails, userId);
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Updates the fuel form details delivery.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public bool updateFuelFormDetailsDelivery(int fuelFormDetailsId, int orderNo, Fuel_Form_Details_Delivery updateFuelFormDetailsDelivery, Guid userId)
        {
            try
            {
                Fuel_Form_Details_Delivery foundFuelFormDetailsDelivery = _dataProvider.DataContext.Fuel_Form_Details_Delivery.SingleOrDefault(ffdd => ffdd.Fuel_Form_Details_Id == fuelFormDetailsId &&
                                                                                                                                            ffdd.Order_No == orderNo);

                if (foundFuelFormDetailsDelivery != null)
                {
                    foundFuelFormDetailsDelivery.Invoice_Number = updateFuelFormDetailsDelivery.Invoice_Number;
                    foundFuelFormDetailsDelivery.Order_No = updateFuelFormDetailsDelivery.Order_No;
                    foundFuelFormDetailsDelivery.Quantity = updateFuelFormDetailsDelivery.Quantity;
                    foundFuelFormDetailsDelivery.Recieve_Date = updateFuelFormDetailsDelivery.Recieve_Date;
                    foundFuelFormDetailsDelivery.Vendor_ID = updateFuelFormDetailsDelivery.Vendor_ID;

                    _dataProvider.DataContext.SaveChanges();

                    // archive changes
                    archiveFuelFormDetailsDelivery(foundFuelFormDetailsDelivery, userId);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                return false;
            }
        }

        /// <summary>
        /// Updates the fuel form equip failure.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public void updateFuelFormEquipFailure(int fuelFormEquipFailureId, Fuel_Form_Equipment_Failure updateFuelFormEquipFailure, Guid userId)
        {
            try
            {
                Fuel_Form_Equipment_Failure foundFuelFormEquipFailure = _dataProvider.DataContext.Fuel_Form_Equipment_Failure.SingleOrDefault(ffef => ffef.Equipment_Failure_ID == fuelFormEquipFailureId);

                if (foundFuelFormEquipFailure != null)
                {
                    foundFuelFormEquipFailure.Failure_Date = updateFuelFormEquipFailure.Failure_Date;
                    foundFuelFormEquipFailure.Fix_Date = updateFuelFormEquipFailure.Fix_Date;
                    foundFuelFormEquipFailure.is_Equipment_Failure = updateFuelFormEquipFailure.is_Equipment_Failure;
                    foundFuelFormEquipFailure.Remarks = updateFuelFormEquipFailure.Remarks;
                }

                _dataProvider.DataContext.SaveChanges();

                // archive changes
                archiveFuelFormEquipFailure(foundFuelFormEquipFailure, userId);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Archive

        /// <summary>
        /// Updates the vendor.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public void archiveFuelForm(Fuel_Form fuelForm, Guid userId)
        {
            try
            {
                Fuel_Form_History archiveFuelForm = new Fuel_Form_History
                {
                    Fuel_Form_ID = fuelForm.Fuel_Form_ID,
                    Submission_Date = fuelForm.Submission_Date,
                    UserId = fuelForm.UserId,
                    Supervisor_Full_Name = fuelForm.Supervisor_Full_Name,
                    Supervisor_Badge_Number = fuelForm.Supervisor_Badge_Number,
                    Remarks = fuelForm.Remarks,
                    Modified_By = userId,
                    Modified_Date = DateTime.Now
                };

                _dataProvider.DataContext.Fuel_Form_History.AddObject(archiveFuelForm);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Updates the vendor.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public void archiveFuelFormDetails(Fuel_Form_Details fuelFormDetails, Guid userId)
        {
            try
            {
                Fuel_Form_Details_History archiveFuelFormDetails = new Fuel_Form_Details_History
                {
                    Fuel_Form_ID = fuelFormDetails.Fuel_Form_ID,
                    Fuel_Form_Details_Id = fuelFormDetails.Fuel_Form_Details_Id,
                    Product_ID = fuelFormDetails.Product_ID,
                    On_Hand = fuelFormDetails.On_Hand,
                    Spare_Drums = fuelFormDetails.Spare_Drums,
                    Equipment_User_ID = fuelFormDetails.Equipment_User_ID,
                    Tank_Delivered = fuelFormDetails.Tank_Delivered,
                    Tank_Dispensed = fuelFormDetails.Tank_Dispensed,
                    Modified_By = userId,
                    Modified_Date = DateTime.Now
                };

                _dataProvider.DataContext.Fuel_Form_Details_History.AddObject(archiveFuelFormDetails);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Archives a fuel form details delivery
        /// </summary>
        /// <param name="Fuel_Form_Details_Delivery">Fuel Form Details Delivery</param>
        /// <param name="Guid">Updater's Guid</param>
        public void archiveFuelFormDetailsDelivery(Fuel_Form_Details_Delivery fuelFormDetailsDelivery, Guid userId)
        {
            try
            {
                Fuel_Form_Details_Delivery_History archiveFuelFormDetails = new Fuel_Form_Details_Delivery_History
                {
                    Fuel_Form_Details_Id = fuelFormDetailsDelivery.Fuel_Form_Details_Id,
                    Order_No = fuelFormDetailsDelivery.Order_No,
                    Invoice_Number = fuelFormDetailsDelivery.Invoice_Number,
                    Recieve_Date = fuelFormDetailsDelivery.Recieve_Date,
                    Quantity = fuelFormDetailsDelivery.Quantity,
                    Vendor_ID = fuelFormDetailsDelivery.Vendor_ID,
                    Modified_By = userId,
                    Modified_Date = DateTime.Now
                };

                _dataProvider.DataContext.Fuel_Form_Details_Delivery_History.AddObject(archiveFuelFormDetails);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Updates the vendor.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public void archiveFuelFormEquipFailure(Fuel_Form_Equipment_Failure fuelFormEquipFailure, Guid userId)
        {
            try
            {
                Fuel_Form_Equipment_Failure_History archiveFuelFormEquipFailure = new Fuel_Form_Equipment_Failure_History
                {
                    Equipment_Failure_ID = fuelFormEquipFailure.Equipment_Failure_ID,
                    is_Equipment_Failure = fuelFormEquipFailure.is_Equipment_Failure,
                    Failure_Date = fuelFormEquipFailure.Failure_Date,
                    Fix_Date = fuelFormEquipFailure.Fix_Date,
                    Remarks = fuelFormEquipFailure.Remarks,
                    Equipment_User_ID = fuelFormEquipFailure.Equipment_User_ID,
                    Fuel_Form_ID = fuelFormEquipFailure.Fuel_Form_ID,
                    Modified_By = userId,
                    Modified_Date = DateTime.Now
                };

                _dataProvider.DataContext.Fuel_Form_Equipment_Failure_History.AddObject(archiveFuelFormEquipFailure);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion
    }
}