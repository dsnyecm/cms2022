﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;

using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Core.Interfaces;
using DSNY.Data;
using System.Data.Objects.DataClasses;
using DSNY.Common;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IRoleRepository"/> interface
    /// </summary>
    public class DSNYExceptionRepository : IDSNYExceptionRepository
    {
        #region Variables

        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNY.Data.DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionRepository"/> class.
        /// </summary>
        public DSNYExceptionRepository() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionRepository"/> class.
        /// </summary>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public DSNYExceptionRepository(DSNY.Data.DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger ;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets the dsny exception.
        /// </summary>
        /// <param name="DSNYExceptionId">The dsny exception id.</param>
        /// <returns></returns>
        public DSNYException getDSNYException(int DSNYExceptionId)
        {
            return _dataProvider.DataContext.DSNYExceptions.SingleOrDefault(ex => ex.ExceptionId == DSNYExceptionId );
        }

        /// <summary>
        /// Gets the dsny exception category.
        /// </summary>
        /// <param name="DSNYExceptionId">The dsny exception category id.</param>
        /// <returns></returns>
        public DSNYExceptionCategory getDSNYExceptionCategory(int DSNYExceptionCategoryId)
        {
            return _dataProvider.DataContext.DSNYExceptionCategories.SingleOrDefault(exc => exc.ExceptionCategoryId == DSNYExceptionCategoryId);
        }

        /// <summary>
        /// Gets the dsny exception category.
        /// </summary>
        /// <param name="DSNYExceptionId">The dsny exception category id.</param>
        /// <returns></returns>
        public DSNYExceptionCategory getDSNYExceptionCategory(string DSNYExceptionCategoryName)
        {
            return _dataProvider.DataContext.DSNYExceptionCategories.SingleOrDefault(exc => exc.ExceptionCategory == DSNYExceptionCategoryName);
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds a dsny exception.
        /// </summary>
        /// <param name="ex">The new DSNY exception.</param>
        /// <returns></returns>
        public int addException(DSNYException ex)
        {
            _dataProvider.DataContext.DSNYExceptions.AddObject(ex);
            _dataProvider.DataContext.SaveChanges();

            return ex.ExceptionId;
        }

        /// <summary>
        /// Adds a list of dsny exceptions.
        /// </summary>
        /// <param name="exs">The new list  ofDSNY exception.</param>
        /// <returns></returns>
        public bool addExceptions(List<DSNYException> exs)
        {
            foreach (DSNYException ex in exs)
            {
                _dataProvider.DataContext.DSNYExceptions.AddObject(ex);    
            }

            _dataProvider.DataContext.SaveChanges();

            return true;
        }

        #endregion


        #region Convert

        public List<DSNYException> convertExceptions(string exceptions, Guid userId)
        {
            var serializer = new JavaScriptSerializer();

            List<DSNYException> returnExs = new List<DSNYException>();

            List<DSNY.Core.Models.DSNYException> exs = serializer.Deserialize<List<DSNY.Core.Models.DSNYException>>(exceptions);

            if (exs != null)
            {
                foreach (DSNY.Core.Models.DSNYException ex in exs)
                {
                    DSNYException newEx = new DSNYException()
                    {
                        Exception = ex.exception,
                        ExceptionCategoryId = getDSNYExceptionCategory(ex.categoryName).ExceptionCategoryId,
                        ExceptionUser = userId,
                        ExceptionDateTime = DateTime.Now
                    };

                    returnExs.Add(newEx);
                }
            }

            return returnExs;
        }

        #endregion
    }
}