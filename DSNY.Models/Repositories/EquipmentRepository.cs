﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.Core.Interfaces;
using DSNY.Common;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IEquipmentRepository"/> interface
    /// </summary>
    public class EquipmentRepository : IEquipmentRepository
    {
        #region Variables

        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentRepository"/> class.
        /// </summary>
        public EquipmentRepository() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentRepository"/> class.
        /// </summary>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public EquipmentRepository(DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets all equipment items
        /// </summary>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Equipment> getAllEquipment(bool includeInactive)
        {
            if (includeInactive)
                return _dataProvider.DataContext.Equipments.OrderBy(e => e.Equipment_Name).ToList();
            else
                return _dataProvider.DataContext.Equipments.OrderBy(e => e.Equipment_Name).Where(e => e.is_Active == true).ToList();
        }


        /// <summary>
        /// Gets sorted equipment items
        /// </summary>
        /// <param name="field">The field to sort on.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive equipment items].</param>
        /// <returns></returns>
        public List<Equipment> getSortedEquipment(string field, Enums.SortDirection direction, bool includeInactive)
        {
            List<Equipment> unSortedEquipment = getAllEquipment(includeInactive);
            List<Equipment> sortedEquipment;

            if (direction == Enums.SortDirection.Descending)
            {
                sortedEquipment = (from Equipment in unSortedEquipment
                                    orderby orderBy(field, Equipment) descending
                                    select Equipment).ToList();
            }
            else
            {
                sortedEquipment = (from Equipment in unSortedEquipment
                                    orderby orderBy(field, Equipment) ascending
                                    select Equipment).ToList();
            }

            _dataProvider.DataContext.Equipments.MergeOption = System.Data.Objects.MergeOption.NoTracking;

            return sortedEquipment;
        }


        /// <summary>
        /// Gets a single equipment item.
        /// </summary>
        /// <param name="equipmentId">The equipment id.</param>
        /// <returns></returns>
        public Equipment getEquipment(int equipmentId)
        {
            return _dataProvider.DataContext.Equipments.SingleOrDefault(e => e.Equipment_ID == equipmentId);
        }

        /// <summary>
        /// Gets the equipment / user relationships for specific user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Equipment_User> getEquipmentForUser(Guid userId, bool includeInactive)
        {
            List<Equipment_User> equipment;

            if (includeInactive)
                equipment = _dataProvider.DataContext.Equipment_User.Where(eu => eu.UserId == userId).
                    OrderBy(e => e.Equipment.Equipment_Name).ThenBy(e => e.Equipment_Description).ToList();
            else
                equipment = _dataProvider.DataContext.Equipment_User.Where(eu => eu.UserId == userId && eu.is_Active == true).
                    OrderBy(e => e.Equipment.Equipment_Name).ThenBy(e => e.Equipment_Description).ToList();

            if (equipment != null)
                return equipment;
            else
                return new List<Equipment_User>();
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds an equipment item
        /// </summary>
        /// <param name="newEquipment">The new equipment.</param>
        public void addEquipment(Equipment newEquipment)
        {
            _dataProvider.DataContext.Equipments.AddObject(newEquipment);

            _dataProvider.DataContext.SaveChanges();
        }

        /// <summary>
        /// Relates a user to an equipment item (Equipment_User Table).
        /// </summary>
        /// <param name="equipmentId">The equipment id.</param>
        /// <param name="productId">The product id.</param>
        /// <param name="description">The description.</param>
        /// <param name="active">if set to <c>true</c> [active].</param>
        /// <param name="userId">The user id.</param>
        public void addUserToEquipment(int equipmentId, int productId, string description, int capacity, bool active, Guid userId)
        {
            _dataProvider.DataContext.Equipment_User.AddObject(new Equipment_User()
            {
                Equipment_ID = equipmentId,
                UserId = userId,
                Product_ID = productId,
                is_Active = active,
                Equipment_Description = description,
                Capacity = capacity > 0 ? capacity : (int?)null
            });

            _dataProvider.DataContext.SaveChanges();
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates an equipment item.
        /// </summary>
        /// <param name="equipmentId">The equipment id.</param>
        /// <param name="updateEquipment">The populated equipment object .</param>
        public void updateEquipment(int equipmentId, Equipment updateEquipment)
        {
            Equipment foundEquipment = _dataProvider.DataContext.Equipments.SingleOrDefault(e => e.Equipment_ID == equipmentId);

            if (foundEquipment != null && foundEquipment.Equipment_ID > 0)
            {
                foundEquipment.Equipment_Name = updateEquipment.Equipment_Name;
                foundEquipment.is_Active = updateEquipment.is_Active;
            }

            _dataProvider.DataContext.SaveChanges();
        }

        /// <summary>
        /// Updates an equipment / user relationship.
        /// </summary>
        /// <param name="equipmentUserId">The equipment user id.</param>
        /// <param name="productId">The product id.</param>
        /// <param name="description">The description.</param>
        /// <param name="active">if set to <c>true</c> [active].</param>
        public void updateEquipmentUser(int equipmentUserId, int equipmentId, int productId, string description, int capacity, bool active)
        {
            Equipment_User updateEquipUser = _dataProvider.DataContext.Equipment_User.SingleOrDefault(eu => eu.Equipment_User_ID == equipmentUserId);

            if (updateEquipUser != null)
            {
                updateEquipUser.Equipment_ID = equipmentId;
                updateEquipUser.Product_ID = productId;
                updateEquipUser.Equipment_Description = description;
                updateEquipUser.is_Active = active;
                updateEquipUser.Capacity = capacity > 0 ? capacity : (int?)null;

                _dataProvider.DataContext.SaveChanges();
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes an equipment item.
        /// </summary>
        /// <param name="equipmentId">The equipment id.</param>
        public void deleteEquipment(int equipmentId)
        {
            try
            {
                Equipment deleteEquipment = _dataProvider.DataContext.Equipments.SingleOrDefault(e => e.Equipment_ID == equipmentId);

                if (deleteEquipment != null)
                {
                    _dataProvider.DataContext.Equipments.DeleteObject(deleteEquipment);

                    _dataProvider.DataContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes an equipment / user relationship.
        /// </summary>
        /// <param name="equipmentUserId">The equipment user id.</param>
        public void deleteEquipmentUser(int equipmentUserId)
        {
            try
            {
                Equipment_User equipUser = _dataProvider.DataContext.Equipment_User.SingleOrDefault(eu => eu.Equipment_User_ID == equipmentUserId);


                if (equipUser != null)
                {
                    List<Fuel_Form_Equipment_Failure> equipFailures = _dataProvider.DataContext.Fuel_Form_Equipment_Failure.Where(ef => ef.Equipment_User_ID == equipmentUserId).ToList();

                    foreach (Fuel_Form_Equipment_Failure item in equipFailures)
                    {
                        _dataProvider.DataContext.Fuel_Form_Equipment_Failure.DeleteObject(item);
                    }

                    _dataProvider.DataContext.Equipment_User.DeleteObject(equipUser);

                    _dataProvider.DataContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes all equipment / user relationships for specific user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        public void deleteAllEquipmentForUser(Guid userId)
        {
            List<Equipment_User> equipment = _dataProvider.DataContext.Equipment_User.Where(eu => eu.UserId == userId).ToList();

            foreach (Equipment_User equip in equipment)
            {
                _dataProvider.DataContext.Equipment_User.DeleteObject(equip);
            }
        }

        #endregion

        #region Sort

        /// <summary>
        /// Our sorter object, holds the field key and object type
        /// </summary>
        /// <param name="sortKey">The sort key.</param>
        /// <param name="equipment">The equipment.</param>
        /// <returns></returns>
        private object orderBy(string sortKey, Equipment equipment)
        {
            switch (sortKey)
            {
                case "EquipmentName":
                    return equipment.Equipment_Name;

                case "IsActive":
                    return equipment.is_Active;

                default:
                    return equipment.Equipment_Name;
            }
        }

        #endregion
    }
}
