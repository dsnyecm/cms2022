﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Common;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a role repository
    /// </summary>
    public interface IRoleRepository
    {
        string[] getAllRoles();
        List<IRole> getAllRolesList();
        Dictionary<string, string[]> getAllRolesWithUsers();
        List<IRole> getSortedRoles(string field, Enums.SortDirection direction);
        List<IRole> getRolesForUser(string userName);

        bool addRole(string roleName);
        void addUsersToRoles(string[] users, string[] roles);

        bool deleteRole(string roleName);
        void deleteUsersFromRoles(string[] users, string[] roles);
    }
}
