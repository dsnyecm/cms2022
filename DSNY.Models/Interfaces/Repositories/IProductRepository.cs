﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Data;
using DSNY.Common;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a product repository
    /// </summary>
    public interface IProductRepository
    {
        List<Product> getAllProducts(bool includeInactive);
        List<Product> getSortedProducts(string field, Enums.SortDirection direction, bool includeInactive);
        Product getProduct(int productId);
        List<Product_User> getUsersOfProduct(int productId);
        List<Product_User> getProductsForUser(Guid userId);

        int addProduct(string productName, string subType, bool isActive, bool isDrums, bool isReadingRequired, int defaultCapacity, string measurementType);
        int addProduct(Product newProduct);
        void addUserToProduct(int productId, Guid userId, int capacity);

        bool updateProduct(int productId, Product updateProduct, bool removeUsers);
        bool updateProductUserCapacity(int productId, Guid userId, int capacity);
        
        void deleteProduct(int productId);
        void deleteAllProductsForUser(Guid userId);
    }
}
