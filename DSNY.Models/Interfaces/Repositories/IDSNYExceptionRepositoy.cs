﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Common;
using DSNY.Data;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a role repository
    /// </summary>
    public interface IDSNYExceptionRepository
    {
        DSNYException getDSNYException(int DSNYExceptionId);
        DSNYExceptionCategory getDSNYExceptionCategory(int DSNYExceptionCategoryId);
        DSNYExceptionCategory getDSNYExceptionCategory(string DSNYExceptionCategoryName);
        int addException(DSNYException ex);
        bool addExceptions(List<DSNYException> exs);
        List<DSNYException> convertExceptions(string exceptions, Guid userId);
    }
}
