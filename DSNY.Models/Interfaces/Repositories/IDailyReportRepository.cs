﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Common;
using DSNY.Data;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a role repository
    /// </summary>
    public interface IDailyReportRepository
    {
        DailyReport getLastestReport();
        void addDailyReport(DailyReport newDailyReport);
        List<DailyReportsWeather> getAllDailyReportWeather();
        List<DailyReportsFunction> getAllDailyReportFunctions();
        List<DailyReportsTemperature> getAllDailyReportTemperatures();
    }
}