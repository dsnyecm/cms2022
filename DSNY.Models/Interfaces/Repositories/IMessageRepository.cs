﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Data;
using DSNY.Common;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all the methods required to implement an instance of a message repository
    /// </summary>
    public interface IMessageRepository
    {
        List<Sent_Message> getMessagesForUser(Guid userId, DateTime startDate, DateTime endDate, bool fuelForms, int page, int pageSize, out int totalRecords, bool paged);
        List<Sent_Message> getSortedMessagesForUser(string field, Enums.SortDirection direction, Guid userId, DateTime startDate, DateTime endDate, string searchTerms,
            bool fuelForms, int page, int pageSize, out int totalRecords);
        List<Sent_Message> searchMessages(string searchTerms, DateTime startDate, DateTime endDate, Guid userId, bool isFuelForm, int pageNumber, int pageSize, 
            string sortField, string sortDirection, out int totalRecords);
        Message getMessage(int messageId, Guid userId);

        void addMessage(Message newMessage, bool sendToDistribution);
        void addCommandMessage(Message newMessage, bool sendToCommandChain);

        void updateMessageTimestamp(int messageId, Guid userId);
        void updateNewUserWithCurrentMessages(Guid userId);

        void Initialize();
    }
}
