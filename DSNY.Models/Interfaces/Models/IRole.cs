﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface to hold data for each role
    /// </summary>
    public interface IRole
    {
        [Required(ErrorMessage = "Please provide a role name")]
        [DisplayName("Role")]
        string roleName { get; set; }

        List<string> usersInRole { get; set; }
    }
}
