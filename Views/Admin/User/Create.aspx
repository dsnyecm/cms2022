﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.NewUserModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create New User
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftAjax.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftMvcValidation.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Create New User</h2>
    <p style="width: 785px;" class="dottedLine"></p>

    <div class="iconContainer">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "User", action = "List"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />Cancel
            </a>
        </div>
        <div class="iconLeft">
            <a href="#" id="submitForm" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/create.png" alt="Create" runat="server" /><br />Create User
            </a>
        </div>
    </div>

    <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <ul id="tabnav">
            <li class="selectedTab"><a href="#">User Information</a></li>
            <li><a href="#">Command Chain</a></li>
            <li><a href="#">Products</a></li>
            <li><a href="#">Equipment</a></li>
        </ul>

        <div id="tabsContainer">
            <div class="tabContent">
               <%: Html.EditorFor(m => m) %>
            </div>

            <div class="clear"></div>

            <div class="tabContent">
                <div style="float: left; padding-left: 25px;">
                <% if (ViewData["Users"] != null && ((List<DSNY.Core.Interfaces.IUser>)ViewData["Users"]).Count > 0) { %>
                    <div style="float: left;">
                        <h3 style="margin-top: 5px;">Available Users</h3>
                        <select id="usersAvailable" multiple="multiple" class="userSelectList">
                        <% List<DSNY.Core.Interfaces.IUser> users = (List<DSNY.Core.Interfaces.IUser>)ViewData["Users"];
                            foreach (DSNY.Core.Interfaces.IUser user in users) { 
                                if (!string.IsNullOrEmpty(user.description)) {%>
                                    <option value="<%: user.userId %>"><%: user.description%> (<%: user.userName%>)</option>
                             <% } else { %>
                                    <option value="<%: user.userId %>"><%: user.userName%></option>
                             <% }
                            } %>
                        </select>
                    </div>
                    <div style="width: 75px; height: 175px; padding-top: 100px; padding-left: 25px; float: left;">
                        <a href="#" style="margin-bottom: 50px;">
                            <img id="addUser" border="0" src="~/Content/Images/Icons/moveRight.png" alt="Add User" runat="server" />
                        </a>
                        <div style="height: 50px;"></div>
                        <a href="#">
                            <img id="removeUser" border="0" src="~/Content/Images/Icons/moveLeft.png" alt="Add User" runat="server" />
                        </a>
                    </div>
                    <div style="float: left;">
                        <h3 style="margin-top: 5px;">Users who report to new user </h3>
                        <%: Html.DropDownList("usersSelected", (SelectList)ViewData["SelectedUsers"], new { @class = "userSelectList", @multiple = "multiple" })%>
                    </div>
                <% } %>
                </div>
            </div>

            <div class="clear"></div>

            <% List<DSNY.Data.Product> products = new List<DSNY.Data.Product>(); %>

            <div class="tabContent">
                <% if (ViewData["Products"] != null && ((List<DSNY.Data.Product>)ViewData["Products"]).Count > 0) { %>
                    <table style="float: left; margin-right: 10px;">
                        <tr>
                            <th style="width: 175px;">Product</th>
                            <th style="width: 50px;">Capacity</th>
                        </tr>
                    <% products = (List<DSNY.Data.Product>)ViewData["Products"];
                        int prodCount = 1;

                        foreach (DSNY.Data.Product prod in products)
                        {
                            if (prodCount % 10 == 0) {
                                prodCount++; %>
                               
                            </table>
                            <table style="float: left; margin-right: 10px;">
                                <tr>
                                    <th style="width: 175px;">Product</th>
                                    <th style="width: 50px;">Capacity</th>
                                </tr>
                        <% } %>
                                <tr>
                                <%  // If we have a postback, use that value, otherwise blank
                                    if (Request["product" + prod.Product_ID] != null) { %>
                                    <td>
                                        <input type="checkbox" checked="checked" id="Checkbox1" name="product<%: prod.Product_ID %>" /><%: prod.Product_Name%>
                                    </td>
                                    <td>
                                        <input type="text" id="Text1" name="prodQty<%: prod.Product_ID %>" style="width: 40px;"
                                        value="<%: Request["prodQty" + prod.Product_ID] != null ? Request["prodQty" + prod.Product_ID] : string.Empty  %>" />
                                    </td>
                                <% } else { %>

                                        <td><input type="checkbox" id="product<%: prod.Product_ID %>" name="product<%: prod.Product_ID %>" /><%: prod.Product_Name%></td>
                                        <td><input type="text" id="prodQty<%: prod.Product_ID %>" name="prodQty<%: prod.Product_ID %>" style="width: 40px;" /></td>
                        <% }
                            prodCount++; %>
                                </tr>
                        <% } %>
                        </table>
                    <% } else { %>
                        <h4>There are no products.</h4>
                    <% } %>
            </div>

            <div class="clear"></div>

            <div class="tabContent">
                <% if (ViewData["Equipment"] != null && ((List<DSNY.Data.Equipment>)ViewData["Equipment"]).Count > 0) { %>
                    <table id="equipmentGrid" style="margin-right: 10px;">
                        <tr>
                            <th style="width: 219px;">Equipment</th>
                            <th style="width: 255px;">Description</th>
                            <th style="width: 175px;">Related Product</th>
                            <th style="width: 175px;">Capacity</th>
                            <th style="width: 40px;">Active</th>
                        </tr>
                    <% List<DSNY.Data.Equipment> equipment = (List<DSNY.Data.Equipment>)ViewData["Equipment"];
                       int equipCount = 0; %>
                           <tr id="firstInputRow" style="display: none;" >
                                <td>
                                    <select name="equipUser" class="equipType">
                                        <option value="-1">SELECT EQUIPMENT</option>
                                        <% foreach (DSNY.Data.Equipment equip in equipment)
                                           { %>
                                                <option value="<%: equip.Equipment_ID %>" class="activeCheckbox"><%: equip.Equipment_Name%></option>
                                        <% } %>
                                    </select>
                                </td>
                                <td><input type="text" name="equipDescript" value="" style="width: 200px" /></td>
                                <td>
                                    <select name="equipProd">
                                        <option value="-1">SELECT PRODUCT</option>
                                        <% foreach (DSNY.Data.Product prod in products)
                                           { %>
                                                <option value="<%: prod.Product_ID %>" class="activeCheckbox"><%: prod.Product_Name%></option>
                                        <% } %>
                                    </select>
                                </td>
                                <td style="text-align: center;">
                                    <input type="text" name="equipCapacity" value="" style="width: 50px; display: none;" />
                                </td>
                                <td style="text-align: center;">
                                    <input type="checkbox" name="equipActiveBox" class="activeCheckbox" />
                                    <input type="hidden" name="equipActive" />
                                </td>
                                <td style="text-align: center;">
                                    <a href="#" class="removeRow" onclick="return false;">
                                        <img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove" runat="server" />
                                    </a>
                                </td>
                            </tr>
                        <%  equipCount++; %>
                        </table>
                        <a href="#" id="addEquipRow" style="float: right; margin: 10px 10px 0px 0px;">Add Equipment Row</a>
                        <div class="clear"></div>
                <% } else { %>
                    <h4>There are no equipment items.</h4>
                <%} %>
            </div>
        </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tabsContainer").tabs({ tab: 1 });

            // inital sort
            SortListbox("usersAvailable");
            SortListbox("usersSelected");

            // takes all selected options in 'usersAvailable' and moves to 'usersSelected'
            $("#addUser").click(function () {
                $('#usersAvailable option:selected').appendTo('#usersSelected');
                $('#usersAvailable option:selected').remove();

                SortListbox("usersAvailable");
                SortListbox("usersSelected");
            });

            // takes all selected options in 'usersSelected' and moves to 'usersAvailable'
            $("#removeUser").click(function () {
                $('#usersSelected option:selected').appendTo('#usersAvailable');
                $('#usersSelected option:selected').remove();

                SortListbox("usersAvailable");
                SortListbox("usersSelected");
            });

            // Remove a row
            $('.removeRow').live('click', function () {
                var rowIndex = $(this).closest("tr").prevAll("tr").length;
                var row = $(this).parents("table").find("tr:eq(" + rowIndex + ")");
                row.remove();
            });

            // before submit, we select all options in the select list, this allows us to grab all the values
            $('#submitForm').click(function () {
                $('#usersSelected option').attr('selected', 'selected');
                $(".iconContainer").html("<div style='font-weight: bold; margin: 5px 0px 5px;'>Adding new user, do not close this window or navigate away.  Please be patient.</div>");
                document.forms[0].submit();
            });

            // Add new row to table
            $('#addEquipRow').live('click', function () {
                var table = $('#equipmentGrid');
                var rowIndex = table.attr('rows').length - 1;
                var row = table.find("tr:eq(" + rowIndex + ")");
                var copiedRow = null;

                if (rowIndex > 1)
                    copiedRow = row.clone();
                else {
                    copiedRow = $("#firstInputRow").clone();
                    copiedRow.attr('id', '');
                    copiedRow.css('display', 'table-row');
                }

                if (copiedRow != null) {
                    copiedRow.find(":input:eq(0)").val("-1");
                    copiedRow.find(":input:eq(1)").val("");
                    copiedRow.find(":input:eq(2)").val("-1");
                    copiedRow.find(":input:eq(3)").val("").css('display', 'none');
                    copiedRow.find(":input:eq(4)").attr('checked', false);

                    copiedRow.insertAfter(table.find("tr:eq(" + rowIndex + ")"));
                }
            });

            // shows or hides capacity for equipment
            $('.equipType').live('change', function () {
                // get value and then also get the targeted capacity input to change css display
                var value = $(this).val();
                var row = $(this).parents('tr');
                var inputToChange = $(this).parents('tr').find('td:eq(3)').find(':input');

                // loop through the equipment, if we get a match, flip display based on if there is a capacity
                <% foreach (DSNY.Data.Equipment equip in (List<DSNY.Data.Equipment>)ViewData["Equipment"]) 
                    { %>
                    if (value == <%: equip.Equipment_ID %>) {
                        row.find(":input:eq(0)").attr("name", "equipUser");
                        row.find(":input:eq(1)").attr("name", "equipDescript<%: equip.Equipment_ID %>");
                        row.find(":input:eq(2)").attr("name", "equipProd<%: equip.Equipment_ID %>");
                        row.find(":input:eq(3)").attr("name", "equipCapacity<%: equip.Equipment_ID %>");
                        row.find(":input:eq(4)").attr("name", "equipActiveBox<%: equip.Equipment_ID %>");
                        row.find(":input:eq(5)").attr("name", "equipActive<%: equip.Equipment_ID %>");
                        $(inputToChange).css('display', '<%: equip.has_Capacity ? "inline" : "none" %>');
                        return;
                    }
                <% } %>
            });

            $(".activeCheckbox").click(function () {
                var nextInput = $(":input:eq(" + ($(":input").index(this) + 1) + ")");

                if ($(this).is(':checked'))
                    nextInput.val("true");
                else
                    nextInput.val("false");
            });
        });
    </script>
</asp:Content>