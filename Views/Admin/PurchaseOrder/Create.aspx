﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.PurchaseOrdersViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create Purchase Order
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftAjax.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftMvcAjax.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftMvcValidation.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Create Purchase Order</h2>
    <p class="dottedLine"></p>

    <div class="adminIconContainer">
        <div class="iconLeft">
            <a id="validatePurchaseOrder" href="#" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/create.png" alt="Process Purchase Order" runat="server" /><br />
                Process Purchase Order
            </a>
        </div>

        <div class="iconLeft">
            <a id="addPurchaseOrderLine" href="#" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/add.png" alt="Add Process Purchase Line" runat="server" /><br />
                Add Line
            </a>
        </div>
    </div>

    <div class="clear"></div>
    <br />

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm("Create", "PurchaseOrder", FormMethod.Post, new { id = "purchaseOrder" }))
       {%>

    <table id="productOrderTable" class="tablesorter">
        <thead>
            <tr>
                <th style="width: 90px;">Add to Order</th>
                <th style="width: 80px;">Location</th>
                <th style="width: 170px;">Fuel Type</th>
                <th style="width: 100px;">Order Amount</th>
                <th style="width: 220px;">Do Not Add Reason</th>
            </tr>
        </thead>
        <tbody>
            <% 
           int rowCount = 0;
           int rowColorCount = 0;
           int productId = 0;
           int runningTotal = 0;
           foreach (DSNY.ViewModels.PurchaseOrderViewModel p in Model.pos) {
               if (productId != p.productId && productId != 0)
                { %>
                    <tr>
                        <td colspan="2"></td>
                        <td style="font-weight: bold; text-align: right;">Total</td>
                        <td style="text-align: center;"><%= runningTotal %></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                <%
                    runningTotal = 0;
                    rowColorCount = 0;
                    productId = p.productId;
                } %>
                <tr class="<%: rowColorCount % 2 == 0 ? "alt" : string.Empty %>" id="pos[<%= rowCount %>]_row" >
                    <td style="text-align: center;">
                        <%: Html.CheckBoxFor(po => po.pos[rowCount].isOrdered) %>
                    </td>
                    <td>
                        <%= p.pos.aspnet_Users.UserName %>
                        <%: Html.HiddenFor(po => po.pos[rowCount].userId) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.aspnet_Users.UserName) %><%: Html.HiddenFor(po => po.pos[rowCount].address) %><%: Html.HiddenFor(po => po.pos[rowCount].borough) %><%: Html.HiddenFor(po => po.pos[rowCount].poDisplayName) %>
                    </td>
                    <td>
                        <%= p.pos.Product.Product_Name%> (<%= p.pos.Product.Measurement_Type %>)
                        <%: Html.HiddenFor(po => po.pos[rowCount].productId) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.Product.Product_Name) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.Product.Measurement_Type) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.Product.Sub_Type) %>
                        <%: Html.HiddenFor(po => po.pos[rowCount].vendorId) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.Vendor.Vendor_Name) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.Vendor.Account_Number) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.Vendor.Phone_Number) %>
                    </td>
                    <td style="text-align: center;">
                        <%: Html.TextBoxFor(po => po.pos[rowCount].orderAmount, new { style="width: 65px; text-align: right;", maxlength="5", data_value=p.orderAmount}) %>
                        <%: Html.ValidationMessageFor(po => po.pos[rowCount].orderAmount) %>
                    </td>
                    <td style="text-align: center;">
                        <%: Html.TextBoxFor(po => po.pos[rowCount].exceptionReason, new { @class="is-ordered" })%>
                        <%: Html.ValidationMessageFor(po => po.pos[rowCount].exceptionReason) %>
                    </td>
                </tr>
            <% if (rowCount == Model.pos.Count() - 1) { %>
                <tr>
                    <td colspan="2"></td>
                    <td style="font-weight: bold; text-align: right;">Total</td>
                    <td style="text-align: center;"><%= runningTotal %></td>
                    <td></td>
                </tr>
            <%
            }
               if (productId == 0) {
                   productId = p.productId;
               }

               runningTotal += p.orderAmount;
               rowCount++;
               rowColorCount++;
           } %>
        </tbody>
    </table>
    <% } %>

    <div id="purchaseOrderAddView" class="modal" title="Add Purchase Order Line">
        <form id="poForm" action="/" method="get">
            <fieldset style="width: 300px;">
                <div class="editor-label" style="width: 90px;"><label for="loction">Location:</label></div>
                <div class="editor-field" style="width: 205px;">
                    <%: Html.DropDownList("poLocation", new SelectList(ViewData["users"] as List<DSNY.Core.Interfaces.IUser>, "userId", "userName"))%>
                </div>

                <div class="editor-label" style="width: 90px;"><label for="fuelType">Fuel Type:</label></div>
                <div class="editor-field" style="width: 205px;">
                    <%: Html.DropDownList("po", new SelectList(ViewData["fuelTypes"] as List<DSNY.Data.Product>, "Product_ID", "Product_Name"))%>
                </div>

                <div class="editor-label" style="width: 90px;"><label for="orderAmount">Order Amount:</label></div>
                <div class="editor-field" style="width: 205px;">
                    <input type="text" id="poAmount" name="poAmount" style="width: 50px" maxlength="5" />
                </div>

                <div class="editor-label" style="width: 90px;"><label for="exception">Reason:</label></div>
                <div class="editor-field" style="width: 205px;">
                    <input type="text" id="poException" name="poException" style="width: 193px;" />
                </div>

                <!-- Allow form submission with keyboard without duplicating the dialog button -->
                <input type="submit" tabindex="-1" style="position: absolute; top: -1000px" />
            </fieldset>
        </form>
    </div>

    <div id="purchaseOrderView" class="modal" title="Purchase Order">
        <div style="width: 250px; float: left;">
            <span style="font-weight: bold;">Available Orders</span>
            <ul id="poCategories"></ul>
        </div>

        <div id="poContainer" style="float: left; display: none; padding-left: 25px;">
            <h2 style="text-align: center;">New York City<br />Department of Sanitation</h2>
            <div >
                <div id="poVendorInfo" style="float: left"></div>
                <div id="poDate" style="float: right"></div>
                <div class="clear"></div>
            </div>

            <h3 id="poProductName"  style="text-align: center;"></h3>

            <table id="productOrderViewTable">
                <thead>
                <tr>
                    <th style="width: 75px; text-align: center;">Sub Type</th>
                    <th style="width: 90px; text-align: center;">District</th>
                    <th style="width: 200px; text-align: center;">Address</th>
                    <th style="width: 100px; text-align: center;">Amount</th>
                </tr>
                </thead>
                <tbody id="productOrderViewRows">

                </tbody>
            </table>
        </div>

        <div class="clear"></div>
    </div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="JavascriptSource" runat="server">

    <script type="text/javascript">
       
        $(document).ready(function () {
            var rowCount = <%= Model.pos.Count() - 1 %>,
                poItems = [];

            $("#purchaseOrder").validate({
                errorClass: "input-validation-error"
            });

            $('input[name*=".isOrdered"]').live('click', function () {
                var namePrefix = this.name.substr(0, this.name.indexOf('.isOrdered'));
                var orderAmount = $('input[name="' + namePrefix + '.orderAmount"]');
                var reason = $('input[name="' + namePrefix + '.exceptionReason"]');

                if (!$(this).is(':checked')) {
                    reason.rules('add', { required: true, messages: { required: '*' } });
                    $('input[name="' + namePrefix + '.orderAmount"]').attr('disabled', 'true');
                    $('tr[id="' + namePrefix + '_row"').css('background-color', '#ccc');
                } else {
                    if (orderAmount.val() == orderAmount.attr('data_value')) {
                        reason.rules('remove');
                    }

                    $('tr[id="' + namePrefix + '_row"').removeAttr('style');
                    $('input[name="' + namePrefix + '.orderAmount"]').removeAttr('disabled');
                }

                reason.valid();   // validate input
            });

            $('input[name*="orderAmount"]').live('input', function () {
                var originalValue = $(this).attr('data_value');
                var namePrefix = this.name.substr(0, this.name.indexOf('.orderAmount'));
                var isOrdered = $('input[name="' + namePrefix + '.isOrdered"]').is(':checked');
                var reason = $('input[name="' + namePrefix + '.exceptionReason"]');

                if ($(this).val() !== originalValue) {
                    reason.rules('add', { required: true, messages: { required: '*' } });
                } else if (isOrdered) {
                    reason.rules('remove');
                }

                reason.valid();   // validate input                
            });

            $('#poCategories a').live('click', function() {
                var catName = this.text,
                    items = _.filter(poItems, function(p) {
                        return p.productName === catName && p.isOrdered;
                    });

                $('#poCategories a').removeAttr("style");
                $(this).attr("style", "font-weight: bold; font-style: italic;");
                // empty po rows
                $('#productOrderViewRows').empty();

                // fill in po with data
                if (items.length > 0) {
                    $("#poVendorInfo").html('<span style="font-weight: bold; text-decoration: underline;">' + items[0].vendorName + '</span><br />' + items[0].vendorAcctNum + '<br />' + items[0].vendorPhoneNum);
                    $("#poDate").text(new Date().toDateString());
                    $("#poProductName").text(items[0].productName);

                    var currentBorough = null;

                    _.forEach(items, function(i, count) {
                        if (i.borough != '' && (count === 0 || currentBorough !== i.borough)) {
                            currentBorough = i.borough;
                            $('#productOrderViewRows').append('<tr style="color: white; text-weight: bold; background-color: green; text-align: center;"><td colspan="4">' + currentBorough + '</td></tr>');
                        }
                        console.log(i);
                        $('#productOrderViewTable').append('<tr><td style="text-align: center;">' + i.subType + '</td><td style="text-align: center;">' + i.poDisplayName + '</td><td style="text-align: center;">' + i.address + '</td><td style="text-align: center;">' + i.orderAmount + '</td></tr>');
                    });

                    reStripe("productOrderViewTable");
                    $('#poContainer').show();
                }
            });

            $('#validatePurchaseOrder').click(function () {
                if ($("#purchaseOrder").valid()) {

                    var po = $('#purchaseOrder').serializeArray(),
                        position = 0,
                        tempObj = null;

                    // parse the array for po form values
                    for (var i = 0, max = po.length; i < max; i++) {
                        if (po[i].name.indexOf('pos[' + position + ']') > -1) {
                            tempObj = {};
                            tempObj.isOrdered = (po[i++].value === 'true');
                            po[i].name.indexOf('isOrdered') > -1 ? i++ : i;
                            i++;
                            tempObj.userName = po[i++].value;
                            tempObj.address = po[i++].value;
                            tempObj.borough = po[i++].value;
                            tempObj.poDisplayName = po[i++].value;
                            i++
                            tempObj.productName = po[i++].value + ' (' + po[i++].value + ')';
                            tempObj.subType = po[i++].value;
                            i++;
                            tempObj.vendorName = po[i++].value;
                            tempObj.vendorAcctNum = po[i++].value;
                            tempObj.vendorPhoneNum = po[i++].value;
                            tempObj.orderAmount = po[i].name.indexOf('orderAmount') > -1 ? po[i++].value : -1;
                            tempObj.exceptionReason = po[i].value;

                            poItems.push(tempObj);

                            position++;
                        }
                    }

                    // sort by borough and name
                    poItems = _.sortByAll(poItems, ['borough', 'userName']);

                    // get categories
                    var categories = _.uniq(poItems, function(item) {
                        return item.productName;
                    });

                    $('#poCategories').empty();
                    $('#poContainer').hide();

                    _.forEach(categories, function(c) {
                        $('#poCategories').append('<li><a href="#">' + c.productName + '</a></li>');
                    });

                    $('#poCategories li:first a').click();

                    var dialog = $("#purchaseOrderView").dialog({
                        autoOpen: true,
                        height: $(window).height() - 100,
                        width: $(window).width() - 100,
                        modal: true,
                        buttons: {
                            "Process": function () {
                                $('#purchaseOrder').submit()
                            },
                            Cancel: function () {
                                dialog.dialog("close");
                            }
                        }
                    });
                }
            });

            $('#addPurchaseOrderLine').click(function () {
                var dialog = $("#purchaseOrderAddView").dialog({
                    autoOpen: true,
                    height: 300,
                    width: 355,
                    modal: true,
                    open: function () {
                        var form = $("#poForm"),
                            location = $('select[id="poLocation"]'),
                            fuelType = $('select[id="po"]'),
                            orderAmount = $('input[id="poAmount"]'),
                            exception = $('input[id="poException"]');

                        form.validate({
                            errorClass: "input-validation-error"
                        });

                        // disable button
                        $(".ui-dialog-buttonpane button:contains('Add')").attr("disabled", true).addClass("ui-state-disabled");

                        // reset inputs
                        location.get(0).selectedIndex = 0;
                        fuelType.get(0).selectedIndex = 0;
                        orderAmount.removeAttr('value');
                        exception.removeAttr('value');

                        // input validations
                        orderAmount.rules('add', { required: true, digits: true, messages: { required: '*', digits: '*' } });
                        exception.rules('add', { required: true, messages: { required: '*' } });

                        form.find(':input').bind('input', function () {
                            var valid = form.valid();

                            // enables/disables button based on form validity
                            if (valid) {
                                $(".ui-dialog-buttonpane button:contains('Add')").removeAttr("disabled").removeClass("ui-state-disabled");
                            } else {
                                $(".ui-dialog-buttonpane button:contains('Add')").attr("disabled", true).addClass("ui-state-disabled");
                            }
                        });

                        // run validations
                        orderAmount.valid();
                        exception.valid();
                    },
                    buttons: {
                        "Add": function () {
                            // gather form values
                            var location = $('select[id="poLocation"] option:selected'),
                                fuelType = $('select[id="po"] option:selected'),
                                orderAmount = $('input[id="poAmount"]'),
                                exception = $('input[id="poException"]');

                            // find row to copy, and an input to gather name/id info from
                            var row = $('#productOrderTable tbody tr:first'),
                                input = row.find('input[name*="orderAmount"]');

                            if (input.length > 0) {
                                // get name, id and prefixes
                                var name = input.attr('name'),
                                    id = input.attr('id'),
                                    namePrefix = name.substr(0, name.indexOf('.orderAmount')),
                                    idPrefix = id.substr(0, id.indexOf('__orderAmount'));

                                rowCount++;

                                // clone row and change text/inputs for new item
                                var newRow = row.clone(),
                                    similarRow = $('tr:contains("' + fuelType.text() + '"):last');

                                newRow.find('td:eq(1)').text(location.text());
                                newRow.find('td:eq(2)').text(fuelType.text());
                                newRow.find('#' + idPrefix + '__isOrdered').attr('name', 'pos[' + rowCount + '].isOrdered').attr('id', 'pos_' + rowCount + '__isOrdered').attr('checked', true);
                                newRow.find('input[name="' + namePrefix + '.isOrdered"]').attr('name', 'pos[' + rowCount + '].isOrdered').val(true);
                                newRow.find('#' + idPrefix + '__productId').attr('name', 'pos[' + rowCount + '].productId').attr('id', 'pos_' + rowCount + '__productId').val(fuelType.val());
                                newRow.find('#' + idPrefix + '__userId').attr('name', 'pos[' + rowCount + '].userId').attr('id', 'pos_' + rowCount + '__userId').val(location.val());
                                newRow.find('#' + idPrefix + '__orderAmount').attr('name', 'pos[' + rowCount + '].orderAmount').attr('id', 'pos_' + rowCount + '__orderAmount').val(orderAmount.val());
                                newRow.find('#' + idPrefix + '__exceptionReason').attr('name', 'pos[' + rowCount + '].exceptionReason').attr('id', 'pos_' + rowCount + '__exceptionReason').val(exception.val());

                                // find row with the same fuel type
                                similarRow.after(newRow);

                                dialog.dialog("close");
                            }
                        },
                        Cancel: function () {
                            dialog.dialog("close");
                        }
                    }
                });
            });           
        });
    </script>
</asp:Content>