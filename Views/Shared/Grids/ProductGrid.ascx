﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Data.Product>>" %>

    <% if (Model.ToList().Count > 0) { %>
        <table id="productTable" class="tablesorter">
            <thead>
            <tr>
                <th style="width: 40px;"></th>
                <th><a href="#" class="sortable" id="UserName">Product</a></th>
                <th><a href="#" class="sortable" id="MeasurementType">Measurement Type</a></th>
                <th><a href="#" class="sortable" id="DefaultCapacity">Default Capacity</a></th>
                <th><a href="#" class="sortable" id="IsDrum">Is Drum</a></th>
                <th><a href="#" class="sortable" id="IsReadingRequired">Is Reading Required</a></th>
                <th><a href="#" class="sortable" id="SortNumber">Sort Number</a></th>
                <th><a href="#" class="sortable" id="IsActive">Active</a></th>
                <th>Default Vendor</th>
            </tr>
            </thead>
            <tbody>
        <% foreach (var item in Model) { %>
                <tr id="row-<%: item.Product_ID %>">
                    <td>
                        <a href="<%: Url.RouteUrl("Product Edit", new { action = "Edit", id = item.Product_ID }) %>">
                            <img class="editButton" border="0" src="<%=ResolveUrl("~/Content/Images/Icons/pencil.png")%>" alt="Edit <%: item.Product_Name %>" name="<%: item.Product_Name %>" /></a>
                        <a href="#">
                            <img class="deleteButton" border="0" src="<%=ResolveUrl("~/Content/Images/Icons/remove.png")%>" alt="Remove <%: item.Product_Name %>" id="<%: item.Product_ID %>" name="<%: item.Product_Name %>" />
                        </a>
                    </td>
                    <td><%: item.Product_Name%></td>
                    <td><%: item.Measurement_Type%></td>
                    <td><%: item.Default_Capacity%></td>
                    <td><%: item.is_Drums%></td>
                    <td><%: item.is_Reading_Required%></td>
                    <td><%: item.Order_Num%></td>
                    <td><%: item.is_Active%></td>
                    <td><%: item.Vendor.Vendor_Name%></td>
                </tr>
        <% } %>
            <tbody>

        </table>
    <% } else { %>
        <div>There are no products.</div>
    <% } %>