﻿<input type="hidden" id="exceptions" name="exceptions" value="" />

<div id="end-book-issue" class="modal" title="Inventory Level Issue">
	<p>
        <span class="ui-icon ui-icon-alert"></span>
        The end book inventory amount is <span id="end-book-amount"></span>, you have entered an on hand quantity amount of <span id="end-book-on-hand-qty"></span>. Is this correct?
	</p>
    <p class="reason">
        Please provide reason:<br />
        <textarea rows="2" cols="40" id="bookEndReason"></textarea>
    </p>

</div>

<div id="delivery-issue" class="modal" title="Delivery Issue">
	<p>
        <span class="ui-icon ui-icon-alert"></span>
        You have a delivery amount of <span id="delivery-issue-amount"></span>, but <span id="delivery-issue-product"></span> entered into tank is <span id="delivery-issue-prod-amount"></span> amount. Is this correct?
	</p>
    <p class="reason">
        Please provide reason:<br />
        <textarea rows="2" cols="40" id="delieryIssueReason"></textarea>
    </p>

</div>

<div id="expected-delivery-issue" class="modal"  title="Expected Delivery Issue">
	<p>
        <span class="ui-icon ui-icon-alert"></span>
        Expecting a delivery amount of less than or equal to <span id="expected-delivery-issue-expected-amount"></span>, you entered a delivery amount of <span id="expected-delivery-issue-amount"></span>.  Is this correct?
	</p>
    <p class="reason">
        Please provide reason:<br />
        <textarea rows="2" cols="40" id="expectedDeliveryReason"></textarea>
    </p>
</div>